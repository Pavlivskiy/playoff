// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
// Parse.Cloud.define("hello", function(request, response) {
//     response.success("Hello world!");
// });
// var jobs = require('cloud/jobs.js')
var tempUserData, malePhotoString;

//jared is CAVOAfUxib, amandatestuser is SQox4UoR73
var targetUserIdForTests = "CAVOAfUxib"
var iap = require('in-app-purchase');

Parse.Cloud.define("checkInAppPurchaseStatus", function (req, res) {
    var userId = req.params.userId;
    var receipt = req.params.receipt;
    // console.log('====== checkInAppPurchaseStatus ')
    // console.log(userId)
    // console.log(receipt)
    //When getUser(id) is called a promise is returned. Notice the .then this means that once the promise is fulfilled it will continue. See getUser() function below.
    getUser(userId).then(
        //When the promise is fulfilled function(user) fires, and now we have our USER!
        function (myUser) {
            // console.log(myUser);
            checkStatus();
        },
        function (error) {
            console.error(error)
            res.error(error);
        });
    function checkStatus() {
        var secret = '056f6e96a2c94ecb9c25484e98cf30fc';

        iap.config({
            // If your purchase item is subscription type, you need this
            applePassword: secret,
            googleAccToken: '4/9xkEcjWxmdMHTo_5ZF5o7MRqViB2PLrRrnAnKT9w-dA',
            googleClientID: '853932392297-1dur3d0043cfhqufvu9pcpctncqitpof.apps.googleusercontent.com',
            googleClientSecret: 'EAtDc67fBFhUNRfEOcT2U6pf',
            googleRefToken: '1/7UZhrQyKVQO8baFZKm064I5O4owGQf27qbkYIF2-lcw'
        });
        iap.setup(function (error) {
            if (error) {
                // Don't forget to catch error here
                console.log('Error is ============== ', error)
                res.error(error);
            }
            // console.log('In setup success')
            iap.validate(receipt, function (error, response) {
                console.log("In validate error 000000000 ", error)
                // console.log("In validate response 000000000", response)
                if (error) {
                    // Failed to validate
                    console.log('Error in validate =========== ', error)
                    res.error(error);
                }

                if (iap.isValidated(response)) {
                    var options = {
                        ignoreExpired: true
                    };

                    var purchaseDataList = iap.getPurchaseData(response, options);
                    var activeItem = false;
                    for (var i = 0, len = purchaseDataList.length; i < len; i++) {
                        if (iap.isExpired(purchaseDataList[i])) {
                            // this item has been expired...
                            //console.log('Expired is', i)
                        }
                    }
                    res.success(purchaseDataList)
                } else {
                    res.error("response is not valid");
                }
            });
        });

    }

});

Parse.Cloud.define("getNext10InStackForUser", function (request, response) {


    /**
     * Query:
     * we need to find the next 10 users here for the current userId
     * User cannot have swiped on these users
     * Limit 10
     * Also prioritize those who have swiped
     * Has to be within stated distance of user
     * Make sure genders match the person's preference
     *
     * Also include swipe data for requested user (boolean if that person has also swiped)
     * So add second query to Swipe collection with the user ID and the recipient ID [within the 10 that we pull]
     */
    // request.log.info('Next10start')

    //get the user's data so we have the location, preferences, etc.
    var userId = request.params.userId;
    var options = request.params.options;
    var matchDistance = request.params.matchDistance;
    var returnArray = [];
    var userArrayForFutureQuery = []
    var user, currentGeoPoint, userAttributes, optionalSchoolMatch, optionalSportMatch;

    // request.log.info('Starting next 10 in stack. UserId: ' + userId)


    //When getUser(id) is called a promise is returned. Notice the .then this means that once the promise is fulfilled it will continue. See getUser() function below.
    getUser(userId).then(
        //When the promise is fulfilled function(user) fires, and now we have our USER!
        function (userResponse) {
            // request.log.info('Next 10 in stack: Got user')

            user = userResponse;
            userAttributes = user.attributes;
            if (!userAttributes.locationGeoPoint) {
                // request.log.info('NO locationGeoPoint PRESENT')
            }
            if (!userAttributes.locationGeoPoint._latitude) {
                // request.log.info('NO locationGeoPoint._latitude PRESENT')
            }
            if (!userAttributes.locationGeoPoint._longitude) {
                // request.log.info('NO locationGeoPoint._longitude PRESENT')
            }
            currentGeoPoint = new Parse.GeoPoint(userAttributes.locationGeoPoint._latitude, userAttributes.locationGeoPoint._longitude)
            var userQuery = new Parse.Query(Parse.User);

            // don't search deleted profiles
            userQuery.notEqualTo('deleteProfile', true)
            userQuery.equalTo('manuallyVerified', true)

            //premium options
            if (options == '10' || options == '11') {
                if (userAttributes.matchBySport == true) {
                    if (userAttributes.selectedSport.length) {
                        userQuery.containedIn('primarySport', userAttributes.selectedSport)
                    }
                }
            }
            if (options == '01' || options == '11') {
                if (userAttributes.matchBySchool == true) {
                    if (userAttributes.selectedSchool.length) {
                        userQuery.containedIn('school', userAttributes.selectedSchool)
                    }
                }
            }
            //match age
            if (!userAttributes.ageMax) {
                // request.log.info('NO ageMax PRESENT')
            }
            if (!userAttributes.ageMin) {
                // request.log.info('NO ageMin PRESENT')
            }
            userQuery.greaterThanOrEqualTo('age', userAttributes.ageMin)
            userQuery.lessThanOrEqualTo('age', userAttributes.ageMax)

            //match age
            if (!userAttributes.age) {
                // request.log.info('NO age PRESENT')
            }
            userQuery.greaterThanOrEqualTo('ageMax', userAttributes.age)
            userQuery.lessThanOrEqualTo('ageMin', userAttributes.age)

            //Make sure genders match the person's preference
            //temp remove for test 2017-08-13 19:23:31
            if (!userAttributes.interestedInString) {
                // request.log.info('NO interestedInString PRESENT')
            }
            switch (userAttributes.interestedInString) {
                case "men":
                    userQuery.equalTo('genderString', 'man')
                    break;
                case "women":
                    userQuery.equalTo('genderString', 'woman')
                    break;
                default:
                case "both":
                    userQuery.containedIn('genderString', ['man', 'woman'])
                    break;
            }

            //2017-10-09 16:40:36 adding sort optimizations
            optionalSportMatch = request.params.optionalSportMatch;
            if (request.params.optionalSportMatch) {
                //boolean, match by it
                userQuery.containedIn('primarySport', [userAttributes.primarySport])
            }
            //2017-10-09 16:40:36 adding sort optimizations
            optionalSchoolMatch = request.params.optionalSchoolMatch;
            if (optionalSchoolMatch) {
                //boolean, match by it
                userQuery.containedIn('school', [userAttributes.school])
            }

            //Make sure genders match the person's preference
            //temp remove for test 2017-08-13 19:23:31
            if (!userAttributes.genderString) {
                // request.log.info('NO GENDERSTRING PRESENT')
            }

            switch (userAttributes.genderString) {
                case "man":
                    userQuery.containedIn('interestedInString', ['men', 'both'])
                    break;
                case "woman":
                    userQuery.containedIn('interestedInString', ['women', 'both'])
                    break;
            }

            // Has to be within stated distance of user
            var distanceMilesMax = userAttributes.distanceMilesMax;
            var userGeoPoint = userAttributes.locationGeoPoint;

            //we arent' actually limiting the distance. We just try to find the nearest people first.
            userQuery.near('locationGeoPoint', userAttributes.locationGeoPoint)
            //get userlist who's distance is within user's distanceMilesMax or matchDistance provided by user.
            if (matchDistance) {
                userQuery.withinMiles('locationGeoPoint', userAttributes.locationGeoPoint, matchDistance)
            } else {
                userQuery.withinMiles('locationGeoPoint', userAttributes.locationGeoPoint, distanceMilesMax)
            }

            //limit to 10
            userQuery.limit(10)

            //make sure that the user hasn't swiped on any profiles that we pull up
            //notEqualTo sender
            var Swipes = Parse.Object.extend("Swipes");
            var swipedUsersQuery = new Parse.Query("Swipes");

            //need to find swipes where our user is the sender
            swipedUsersQuery.equalTo("sender", user);
            swipedUsersQuery.include('recipient')
            swipedUsersQuery.descending("createdAt");

            // request.log.info('Pre query next 10 in stack')
            //limit to 10
            swipedUsersQuery.limit(999)
            //run the userQuery
            swipedUsersQuery.find({
                success: function (previousSwipesArray) {

                    // request.log.info('Success query 1' + JSON.stringify(previousSwipesArray, null, 2))

                    // response.success(previousSwipesArray[0])
                    // return;
                    var swipeObject, recipient;
                    var previousUserIdsArray = []
                    if (previousSwipesArray) {
                        for (var index = 0; index < previousSwipesArray.length; index++) {
                            swipeObject = previousSwipesArray[index];
                            recipient = swipeObject.get('recipient');
                            if (recipient && recipient.id)
                                previousUserIdsArray.push(recipient.id)
                        }
                    }
                    // console.log(previousUserIdsArray);

                    userQuery.notContainedIn('objectId', previousUserIdsArray)
                    userQuery.notEqualTo('objectId', userId)

                    //run the userQuery
                    userQuery.find({
                        success: function (results) {
                            // console.log(results);

                            // console.log('results');
                            // console.log(results);
                            //checking which users have swiped the requested User to right and prioritize them
                            var Likers = Parse.Object.extend("Swipes");
                            var likersUsersQuery = new Parse.Query("Swipes");

                            //need to find swipes where our user is the sender
                            likersUsersQuery.equalTo("recipient", user);
                            likersUsersQuery.include('sender')

                            //run the userQuery
                            likersUsersQuery.find({
                                success: function (likersSwipesArray) {
                                    // console.log('likersSwipesArray');
                                    // console.log(likersSwipesArray);
                                    var likerObject, sender;
                                    var likerUserIdsArray = []
                                    if (likersSwipesArray) {
                                        for (var index = 0; index < likersSwipesArray.length; index++) {
                                            likerObject = likersSwipesArray[index];
                                            sender = likerObject.get('sender');
                                            if (sender && sender.id)
                                                likerUserIdsArray.push(sender.id)
                                        }
                                    }
                                    // console.log('likerUserIdsArray');
                                    // console.log(likerUserIdsArray);
                                    var rightSwipedArray = [];
                                    var others = [];

                                    for (var i = 0; i < results.length; i++) {
                                        if (likerUserIdsArray.indexOf(results[i].id) != -1) {
                                            rightSwipedArray.push(results[i])
                                        } else {
                                            others.push(results[i])
                                        }
                                    }

                                    // console.log(rightSwipedArray);
                                    // console.log(others);
                                    if (rightSwipedArray.length) {
                                        var input = Math.min((10 - rightSwipedArray.length), others.length);
                                        for (var j = 0; j < input; j++) {
                                            rightSwipedArray.push(others[j]);
                                        }
                                        // console.log(rightSwipedArray);
                                        userArrayForFutureQuery = rightSwipedArray;
                                    } else {
                                        userArrayForFutureQuery = others;
                                    }


                                    // Also include swipe data for requested user (boolean if that person has also swiped)
                                    // So add second query to Swipe collection with the user ID and the recipient ID [within the 10 that we pull]
                                    // request.log.info('Queried next 10 in stack')

                                    //set this to also check matches in next function

                                    // request.log.info('Success query 2')

                                    checkPotentialMatches()

                                },
                                error: function (error) {
                                    request.log.error(error)
                                    response.error(error)
                                },
                                useMasterKey: true
                            });



                        },
                        error: function (error) {
                            request.log.error(error)
                            response.error(error)
                        },
                        useMasterKey: true
                    });

                },
                error: function (error) {
                    // request.log.info('Error query 1')

                    request.log.error(error)
                    response.error(error)
                },
                useMasterKey: true
            });

        },
        function (error) {
            request.log.error(error)
            response.error(error);
        });


    function checkPotentialMatches() {
        //go through the retunarray and query if any of them have already swiped right on our user
        var potentialMatchQuery = new Parse.Query("Swipes");
        potentialMatchQuery.equalTo('recipient', user)
        potentialMatchQuery.containedIn('sender', userArrayForFutureQuery)
        potentialMatchQuery.include('sender')

        potentialMatchQuery.find({
            success: function (matchedSwipesArray) {

                // request.log.info('Success query 3')


                //also calculate the distanceFrom in miles property for each
                var profile, calcedDistance, personPoint, returnDist, swipe, isMatch, age
                for (var index2 = 0; index2 < userArrayForFutureQuery.length; index2++) {

                    profile = userArrayForFutureQuery[index2];
                    personPoint = profile.attributes.locationGeoPoint;
                    calcedDistance = currentGeoPoint.milesTo(personPoint)
                    returnDist = parseFloat(calcedDistance.toFixed(1));
                    if (returnDist < 1) {
                        returnDist = 1.0;
                    }

                    //also check if it's a match
                    isMatch = false;
                    if (matchedSwipesArray && matchedSwipesArray.length > 0) {
                        for (var index = 0; index < matchedSwipesArray.length; index++) {
                            swipe = matchedSwipesArray[index];
                            if (swipe.attributes.sender.id == profile.id && swipe.attributes.direction == 'right') {
                                //it's a match
                                isMatch = true;
                            }
                        }
                    }

                    age = getAge(profile.attributes.birthdayMMDDYYYY)

                    returnArray.push({
                        user: profile,
                        distanceFrom: returnDist,
                        isMatch: isMatch,
                        heightFeet: profile.attributes.heightFeet,
                        heightInches: profile.attributes.heightInches,
                        graduationYear: profile.attributes.graduationYear,
                        profileDescription: profile.attributes.profileDescription,
                        question1: profile.attributes.question1,
                        question2: profile.attributes.question2,
                        question3: profile.attributes.question3,
                        question4: profile.attributes.question4,
                        age: age,
                        optionalSchoolMatch: optionalSchoolMatch,
                        optionalSportMatch: optionalSportMatch
                    })
                }

                response.success(returnArray)
            },
            error: function (error) {
                request.log.error(error)
                response.error(error)
            },
            useMasterKey: true
        });
    }

});

Parse.Cloud.define("getUserProfileInfo", function (request, response) {


    //get the user's data so we have the location, preferences, etc.
    var userId = request.params.userId;
    var myUserId = request.params.myUserId;
    var myLocationGeoPoint;
    //When getUser(id) is called a promise is returned. Notice the .then this means that once the promise is fulfilled it will continue. See getUser() function below.
    getUser(myUserId).then(
        //When the promise is fulfilled function(user) fires, and now we have our USER!
        function (myUser) {
            myLocationGeoPoint = myUser.attributes.locationGeoPoint;
            grabOtherData()
        },
        function (error) {
            console.error(error)
            response.error(error);
        });

    function grabOtherData() {
        getUser(userId).then(
            //When the promise is fulfilled function(user) fires, and now we have our USER!
            function (userResponse) {
                var userAttributes = userResponse.attributes;

                var returnObject = {}
                returnObject.firstName = userAttributes.firstName;
                returnObject.publicPhotoLink0 = userAttributes.publicPhotoLink0;
                returnObject.publicPhotoLink1 = userAttributes.publicPhotoLink1;
                returnObject.publicPhotoLink2 = userAttributes.publicPhotoLink2;
                returnObject.publicPhotoLink3 = userAttributes.publicPhotoLink3;
                returnObject.publicPhotoLink4 = userAttributes.publicPhotoLink4;
                returnObject.primarySport = userAttributes.primarySport;
                returnObject.heightFeet = userAttributes.heightFeet;
                returnObject.heightInches = userAttributes.heightInches;
                returnObject.graduationYear = userAttributes.graduationYear;
                returnObject.profileDescription = userAttributes.profileDescription;

                returnObject.question1 = userAttributes.question1;
                returnObject.question2 = userAttributes.question2;
                returnObject.question3 = userAttributes.question3;
                returnObject.question4 = userAttributes.question4;

                returnObject.deleteProfile = userAttributes.deleteProfile;
                returnObject.deleteProfileDate = userAttributes.deleteProfileDate;
                returnObject.reActivateProfileDate = userAttributes.reActivateProfileDate;


                var age = 18;
                returnObject.age = getAge(userAttributes.birthdayMMDDYYYY)

                //calc distance from
                var personPoint = userAttributes.locationGeoPoint;
                var calcedDistance = myLocationGeoPoint.milesTo(personPoint)
                var returnDist = parseFloat(calcedDistance.toFixed(1));
                if (returnDist < 1) {
                    returnDist = 1.0;
                }
                returnObject.distanceFrom = returnDist;

                response.success(returnObject)
            },
            function (error) {
                console.error(error)
                response.error(error);
            });
    }
});


function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}


Parse.Cloud.define("get50MostRecentMatchesForUser", function (request, response) {
    var userId = request.params.userId;
    var user
    //When getUser(id) is called a promise is returned. Notice the .then this means that once the promise is fulfilled it will continue. See getUser() function below.
    getUser(userId).then(
        //When the promise is fulfilled function(user) fires, and now we have our USER!
        function (userResponse) {
            user = userResponse;

            //need to check both fields
            var checkQuery1 = new Parse.Query("Matches")
            checkQuery1.equalTo('user1', user)
            checkQuery1.include('user1')
            // checkQuery1.include(['user1.locationString'])


            //and flip it for check
            var checkQuery2 = new Parse.Query("Matches")
            checkQuery2.equalTo('user2', user)
            checkQuery2.include('user2')

            // checkQuery2.include(['user2.locationString'])

            if (user.attributes.deleteProfileDate && user.attributes.reActivateProfileDate) {
                var reActivateProfileDate = user.attributes.reActivateProfileDate;
                checkQuery1.greaterThan('matchDate', reActivateProfileDate);
                checkQuery2.greaterThan('matchDate', reActivateProfileDate);
            }

            var mainQuery = Parse.Query.or(checkQuery1, checkQuery2);
            mainQuery.limit(50)
            mainQuery.descending("lastMessageDateX");
            mainQuery.find({
                success: function (results) {
                    //modify data to only show public info
                    // var matchObject, returnArray, customMatchObject
                    // returnArray = []
                    // for (var index = 0; index < results.length; index++) {
                    //     customMatchObject = {}
                    //     matchObject = results[index]
                    //     var user1 = matchObject.get('user1');
                    //     customMatchObject.test = user1.get('firstName');
                    //     // customMatchObject.user1Data = {
                    //     //     id: matchObject.attributes.user1.id,
                    //     //     firstName: matchObject.attributes.user1.attributes.firstName,
                    //     //     locationString: matchObject.attributes.user1.attributes.locationString,
                    //     //     publicPhotoLink0: matchObject.attributes.user1.attributes.publicPhotoLink0,
                    //     //     primarySport: matchObject.attributes.user1.attributes.primarySport,
                    //     //     genderString: matchObject.attributes.user1.attributes.genderString,
                    //     //     school: matchObject.attributes.user1.attributes.school

                    //     // }
                    //     // customMatchObject.user2Data = {
                    //     //     id: matchObject.attributes.user2.id,
                    //     //     firstName: matchObject.attributes.user2.attributes.firstName,
                    //     //     locationString: matchObject.attributes.user2.attributes.locationString,
                    //     //     publicPhotoLink0: matchObject.attributes.user2.attributes.publicPhotoLink0,
                    //     //     primarySport: matchObject.attributes.user2.attributes.primarySport,
                    //     //     genderString: matchObject.attributes.user2.attributes.genderString,
                    //     //     school: matchObject.attributes.user2.attributes.school
                    //     // }
                    //     returnArray.push(customMatchObject)
                    // }
                    response.success(results)

                },
                error: function (error) {
                    // There was an error.
                }
                // useMasterKey: true
            });


            // var query = new Parse.Query("Matches");
            // query.equalTo('users', user)
            // query.include('users')
            // query.include(['users.locationString'])
            // query.limit(50)
            // query.descending("lastMessageDateX");
            // query.find({
            //     success: function(results) {
            //         var returnArray = [];
            //         //only give the user data that they need
            //         var matchObject;
            //         for (var index = 0; index < results.length; index++) {
            //             matchObject = results[index];
            //             var userRelation = matchObject.relation('users')
            //             // returnArray.push({
            //             //     user: matcho
            //             // })
            //         }
            //         response.success(results)
            //     },
            //     error: function(error) {
            //         // request.log.error(JSON.stringify(error, null, 2))
            //         // response.error(error)
            //         console.error(error)
            //         //return q.reject(error)
            //         response.error(error)
            //     }
            // })

        },
        function (error) {
            console.error(error)
            response.error(error);
        });



})

function getUser(userId) {

    var userQuery = new Parse.Query(Parse.User);
    userQuery.equalTo("objectId", userId);

    //Here you aren't directly returning a user, but you are returning a function that will sometime in the future return a user. This is considered a promise.
    return userQuery.first({
        success: function (userRetrieved) {
            //When the success method fires and you return userRetrieved you fulfill the above promise, and the userRetrieved continues up the chain.
            return userRetrieved;
        },
        error: function (error) {
            return error;
        },
        useMasterKey: true
    });
}


//jobs

Parse.Cloud.define("reset", function (request, response) {
    // console.log('reset start');
    removeAllSwipes();

    function removeAllSwipes() {
        var query = new Parse.Query("Swipes");
        query.find({
            success: function (results) {

                Parse.Object.destroyAll(results, {
                    useMasterKey: true
                })
                    .then(function (success) {

                        removeAllMatches()

                    }, function (error) {
                        request.log.error(JSON.stringify(error, null, 2))

                        console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                        response.error(error);

                    });

            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
                response.error(error)
            },
            useMasterKey: true
        });
    }

    function removeAllMatches() {
        var query = new Parse.Query("Matches");
        query.find({
            success: function (results) {

                Parse.Object.destroyAll(results, {
                    useMasterKey: true
                })
                    .then(function (success) {

                        removeAllMessages()

                    }, function (error) {
                        request.log.error(JSON.stringify(error, null, 2))

                        console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                        response.error(error);

                    });

            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
                response.error(error)
            },
            useMasterKey: true
        });
    }

    function removeAllMessages() {
        var query = new Parse.Query("Messages");
        query.find({
            success: function (results) {
                Parse.Object.destroyAll(results, {
                    useMasterKey: true
                })
                    .then(function (success) {
                        // add15TestUsersLocal()

                    }, function (error) {
                        request.log.error(JSON.stringify(error, null, 2))

                        console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                        response.error(error);

                    });

            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
                response.error(error)
            },
            useMasterKey: true
        });
    }

    function add15TestUsersLocal() {

        // the params passed through the start request
        var params = request.params;
        // Headers from the request that triggered the job
        var headers = request.headers;

        // get the parse-server logger
        var log = request.log;

        var userArray = []
        //create 50 users
        var user;
        response.message("Starting");


        var randNum1to3, newUserData, randNum1to2, randNumBirthyear, randNum1to2b
        for (var index = 0; index < 15; index++) {

            // var element = array[index];
            user = new Parse.User();
            // user.setACL(new Parse.ACL(user));
            newUserData = JSON.parse(JSON.stringify(tempUserData))
            newUserData.username = "Test Name" + Math.floor((Math.random() * 100000) + 1);
            newUserData.password = "testtest";
            newUserData.isTestUser = true;

            // rand num 1-3 for interested in 
            randNum1to3 = Math.floor((Math.random() * 3) + 1);
            if (randNum1to3 == 1) {
                newUserData.interestedInString = "men"
            }
            if (randNum1to3 == 2) {
                newUserData.interestedInString = "women"
            }
            if (randNum1to3 == 3) {
                newUserData.interestedInString = "both"
            }
            newUserData.profileDescription = "IM: " + newUserData.interestedInString;

            //rand 1-2 for gender
            randNum1to2 = Math.floor((Math.random() * 2) + 1);
            if (randNum1to2 == 1) {
                newUserData.genderString = "man"
                newUserData.firstName = "Test Male " + index
                //change pics to male
                newUserData.publicPhotoLink0 = malePhotoString;
                newUserData.publicPhotoLink1 = malePhotoString;
                newUserData.publicPhotoLink2 = malePhotoString;
                newUserData.publicPhotoLink3 = malePhotoString;
                newUserData.publicPhotoLink4 = malePhotoString;
            }
            if (randNum1to2 == 2) {
                newUserData.genderString = "woman"
                newUserData.firstName = "Test Female " + index
            }
            newUserData.profileDescription = "Gender: " + newUserData.genderString;

            // rand 1950 to 1999 for birth year
            randNumBirthyear = Math.floor((Math.random() * 49) + 1950);
            newUserData.birthdayMMDDYYYY = "10-22-" + randNumBirthyear;
            newUserData.profileDescription = "Bday: " + newUserData.birthdayMMDDYYYY;
            newUserData.age = getAge(newUserData.birthdayMMDDYYYY);


            response.message("Users created");
            // response.success("50 Test Users Added");

            //also randomly add swipes on our test user
            // randNum1to2b = Math.floor((Math.random() * 2) + 1);
            // if (randNum1to2b == 1) {
            addSwipeForTestUser(user, 'right')
            // }
            // if (randNum1to2b == 2) {
            //     addSwipeForTestUser(user, 'left')
            // }


            user.save(newUserData, {
                success: function (list) {
                    // All the objects were saved.
                    //purposely no handler here because of the timeout function

                },
                error: function (error) {
                    // An error occurred while saving one of the objects.

                    request.log.error(error)
                },
                useMasterKey: true
            });
        }

        function addSwipeForTestUser(user, direction) {

            var Swipes = Parse.Object.extend("Swipes");
            var swipe = new Swipes();
            swipe.set('sender', user)

            swipe.set('direction', direction)
            swipe.set('swipeDateX', Date.now())

            var mainTestUser = Parse.User.createWithoutData(targetUserIdForTests);
            swipe.set('recipient', mainTestUser)
            swipe.save(null, {
                useMasterKey: true
            }).then(
                function (gameScore) {
                    // Execute any logic that should take place after the object is saved.
                    // request.log.info('added successful right swipe')
                },
                function (gameScore, error) {
                    // Execute any logic that should take place if the save fails.
                    // error is a Parse.Error with an error code and message.
                    request.log.error(error)
                });
        }
    }
})

Parse.Cloud.job("add15TestUsers", function (request, response) {


    add15TestUsers(request, response)

    setTimeout(function () {
        response.success("15 Test Users Added");

    }, 10000);

});

Parse.Cloud.define("manuallyVerifyUser", function (request, response) {
    var id = request.params.userId;
    var manuallyVerify = request.params.manuallyVerify;
    // console.log("id = " + id);
    getUser(id).then(function (userResponse) {
        var user = userResponse;
        user.set('manuallyVerified', manuallyVerify)
        user.save(null, {
            success: function (list) {
                sendPush(id, {
                    'type': 'manualVerification',
                    'content-available': 1,
                    'message': "" + manuallyVerify + "",
                    'time': new Date(),
                    "priority": 10
                });
                var res = {
                    'message': 'Successfully verification submitted',
                    result: userResponse
                }
                response.success(res);
            },
            error: function (error) {
                // An error occurred while saving one of the objects.

                request.log.error(error)
            },
            useMasterKey: true
        });
    }, function (error) {
        response.error(error);
    });

});


//Fetch Admin options
Parse.Cloud.define("getAdminSettings", function (request, response) {
    var returnObject = {};
    //Get user count 
    var userQuery = new Parse.Query(Parse.User)
        .count({
            success: function (count) {
                returnObject.userCount = count;
                //   Match count
                var matchesQuery = new Parse.Query("Matches")
                    .count({
                        success: function (matchesCount) {
                            returnObject.matchesCount = matchesCount;

                            var query1 = new Parse.Query("Swipes")
                                .equalTo('direction', 'right')
                                .count({
                                    success: function (rightSwipesCount) {
                                        returnObject.rightSwipesCount = rightSwipesCount;

                                        var query2 = new Parse.Query("Swipes")
                                            .equalTo('direction', 'left')
                                            .count({
                                                success: function (leftSwipesCount) {
                                                    returnObject.leftSwipesCount = leftSwipesCount;
                                                    response.success(returnObject);
                                                    // The object was retrieved successfully.
                                                },
                                                error: function (object, error) {
                                                    console.log(error);
                                                    response.error(error);
                                                    // The object was not retrieved successfully.
                                                    // error is a Parse.Error with an error code and message.
                                                }
                                            });
                                    },
                                    error: function (object, error) {
                                        console.log(error);
                                        response.error(error);
                                    }
                                });
                        },
                        error: function (object, error) {
                            console.log(error);
                            response.error(error);
                        }
                    });
            },
            error: function (object, error) {
                console.log(error);
                response.error(error);
                // The object was not retrieved successfully.
                // error is a Parse.Error with an error code and message.
            }
        });

});



Parse.Cloud.define("add15TestUsersFunc", function (request, response) {
    var requesterId = request.params.userId;

    add15TestUsers(request, response, requesterId)

    setTimeout(function () {
        response.success("15 Test Users Added");

    }, 10000);

});

function add15TestUsers(request, response, recipientId) {

    // the params passed through the start request
    var params = request.params;
    // Headers from the request that triggered the job
    var headers = request.headers;

    // get the parse-server logger
    var log = request.log;

    var userArray = []
    //create 50 users
    var user;
    // response.message("Starting");

    if (recipientId) {
        targetUserIdForTests = recipientId;
    }


    var randNum1to3, newUserData, randNum1to2, randNumBirthyear, randNum1to2b
    for (var index = 0; index < 15; index++) {

        // var element = array[index];
        user = new Parse.User();
        // user.setACL(new Parse.ACL(user));
        newUserData = JSON.parse(JSON.stringify(tempUserData))
        newUserData.username = "Test Name" + Math.floor((Math.random() * 100000) + 1);
        newUserData.password = "testtest";
        newUserData.isTestUser = true;

        // rand num 1-3 for interested in 
        randNum1to3 = Math.floor((Math.random() * 3) + 1);
        if (randNum1to3 == 1) {
            newUserData.interestedInString = "men"
        }
        if (randNum1to3 == 2) {
            newUserData.interestedInString = "women"
        }
        if (randNum1to3 == 3) {
            newUserData.interestedInString = "both"
        }
        newUserData.profileDescription = "IM: " + newUserData.interestedInString;

        //rand 1-2 for gender
        randNum1to2 = Math.floor((Math.random() * 2) + 1);
        if (randNum1to2 == 1) {
            newUserData.genderString = "man"
            newUserData.firstName = "Test Male " + index
            //change pics to male
            newUserData.publicPhotoLink0 = malePhotoString;
            newUserData.publicPhotoLink1 = malePhotoString;
            newUserData.publicPhotoLink2 = malePhotoString;
            newUserData.publicPhotoLink3 = malePhotoString;
            newUserData.publicPhotoLink4 = malePhotoString;
        }
        if (randNum1to2 == 2) {
            newUserData.genderString = "woman"
            newUserData.firstName = "Test Female " + index
        }
        newUserData.profileDescription = "Gender: " + newUserData.genderString;

        // rand 1950 to 1999 for birth year
        randNumBirthyear = Math.floor((Math.random() * 49) + 1950);
        newUserData.birthdayMMDDYYYY = "10-22-" + randNumBirthyear;
        newUserData.profileDescription = "Bday: " + newUserData.birthdayMMDDYYYY;
        newUserData.age = getAge(newUserData.birthdayMMDDYYYY);


        // response.message("Users created");
        // response.success("50 Test Users Added");

        //also randomly add swipes on our test user
        // randNum1to2b = Math.floor((Math.random() * 2) + 1);
        // if (randNum1to2b == 1) {
        addSwipeForTestUser(user, 'right')
        // }
        // if (randNum1to2b == 2) {
        //     addSwipeForTestUser(user, 'left')
        // }


        user.save(newUserData, {
            success: function (list) {
                // All the objects were saved.
                //purposely no handler here because of the timeout function

            },
            error: function (error) {
                // An error occurred while saving one of the objects.

                request.log.error(error)
            },
            useMasterKey: true
        });
    }

    function addSwipeForTestUser(user, direction) {

        var Swipes = Parse.Object.extend("Swipes");
        var swipe = new Swipes();
        swipe.set('sender', user)

        swipe.set('direction', direction)
        swipe.set('swipeDateX', Date.now())

        var mainTestUser = Parse.User.createWithoutData(targetUserIdForTests);
        swipe.set('recipient', mainTestUser)
        swipe.save(null, {
            useMasterKey: true
        }).then(
            function (gameScore) {
                // Execute any logic that should take place after the object is saved.
                // request.log.info('added successful right swipe')
            },
            function (gameScore, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                request.log.error(error)
            });
    }
}

Parse.Cloud.job("add150TestUsers", function (request, response) {
    // the params passed through the start request
    var params = request.params;
    // Headers from the request that triggered the job
    var headers = request.headers;

    // get the parse-server logger
    var log = request.log;

    var userArray = []
    //create 50 users
    var user;
    // request.log.info('starting 150 user add')


    //
    var randNum1to3, newUserData, randNum1to2, randNumBirthyear, randNum1to2b;
    for (var index = 0; index < 150; index++) {

        // var element = array[index];
        user = new Parse.User();
        // user.setACL(new Parse.ACL(user));
        newUserData = JSON.parse(JSON.stringify(tempUserData))
        newUserData.username = "Test Name" + Math.floor((Math.random() * 100000) + 1);
        newUserData.password = "testtest";
        newUserData.isTestUser = true;

        // rand num 1-3 for interested in 
        randNum1to3 = Math.floor((Math.random() * 3) + 1);
        if (randNum1to3 == 1) {
            newUserData.interestedInString = "men"
        }
        if (randNum1to3 == 2) {
            newUserData.interestedInString = "women"
        }
        if (randNum1to3 == 3) {
            newUserData.interestedInString = "both"
        }
        newUserData.profileDescription = "IM: " + newUserData.interestedInString;

        //rand 1-2 for gender
        randNum1to2 = Math.floor((Math.random() * 2) + 1);
        if (randNum1to2 == 1) {
            newUserData.genderString = "man"
            newUserData.firstName = "Test Male " + index
            //change pics to male
            newUserData.publicPhotoLink0 = malePhotoString;
            newUserData.publicPhotoLink1 = malePhotoString;
            newUserData.publicPhotoLink2 = malePhotoString;
            newUserData.publicPhotoLink3 = malePhotoString;
            newUserData.publicPhotoLink4 = malePhotoString;
        }
        if (randNum1to2 == 2) {
            newUserData.genderString = "woman"
            newUserData.firstName = "Test Female " + index
        }
        newUserData.profileDescription = "Gender: " + newUserData.genderString;

        // rand 1950 to 1999 for birth year
        randNumBirthyear = Math.floor((Math.random() * 49) + 1950);
        newUserData.birthdayMMDDYYYY = "10-22-" + randNumBirthyear;
        newUserData.profileDescription = "Bday: " + newUserData.birthdayMMDDYYYY;
        newUserData.age = getAge(newUserData.birthdayMMDDYYYY);


        response.message("Users created");
        // response.success("50 Test Users Added");

        //also randomly add swipes on our test user
        randNum1to2b = Math.floor((Math.random() * 2) + 1);
        if (randNum1to2b == 1) {
            addSwipeForTestUser(user, 'right')
        }
        if (randNum1to2b == 2) {
            addSwipeForTestUser(user, 'left')
        }


        user.save(newUserData, {
            success: function (list) {
                // All the objects were saved.
                //purposely no handler here because of the timeout function

            },
            error: function (error) {
                // An error occurred while saving one of the objects.

                request.log.error(error)
            },
            useMasterKey: true
        });
    }

    function addSwipeForTestUser(userSender, direction) {

        var Swipes = Parse.Object.extend("Swipes");
        var swipe = new Swipes();
        swipe.set('sender', userSender)

        swipe.set('direction', direction)
        swipe.set('swipeDateX', Date.now())

        var query = new Parse.Query(Parse.User);
        query.equalTo('objectId', targetUserIdForTests)
        query.find({
            success: function (results) {
                // request.log.info('found test user ' + JSON.stringify(results[0], null, 2))

                var mainTestUser = results[0]
                swipe.set('recipient', mainTestUser)
                swipe.save(null, {
                    useMasterKey: true
                }).then(
                    function (gameScore) {
                        // Execute any logic that should take place after the object is saved.
                        // request.log.info('added successful right swipe')
                    },
                    function (gameScore, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        request.log.error(error)
                    });
            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
            },
            useMasterKey: true
        });
    }

    setTimeout(function () {
        response.success("150 Test Users Added");

    }, 10000);

});

Parse.Cloud.job("removeAllTestUsers", function (request, response) {

    // the params passed through the start request
    var params = request.params;
    // Headers from the request that triggered the job
    var headers = request.headers;

    // get the parse-server logger
    var log = request.log;

    response.message('Starting')
    // request.log.info('Starting removal of test users')

    //delete all users except jared account - CAVOAfUxib
    var query = new Parse.Query(Parse.User);
    query.notEqualTo('objectId', targetUserIdForTests)
    query.find({
        success: function (results) {

            //What do I do HERE to delete the posts?
            // users.forEach(function(user) {
            //     user.destroy({
            //         success: function() {
            //             // SUCCESS CODE HERE, IF YOU WANT
            // request.log.info('Pulled ' + results.length + ' users')

            // response.message(results.length)

            //         },
            //         error: function() {
            //             // ERROR CODE HERE, IF YOU WANT
            //         }
            //     });
            // })
            Parse.Object.destroyAll(results, {
                useMasterKey: true
            })
                .then(function (success) {
                    // All the objects were deleted
                    // request.log.info('Deleted test users')

                    //also need to delete relations for my test account
                    removeAllSwipes()

                }, function (error) {
                    request.log.error(JSON.stringify(error, null, 2))

                    console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                    response.error(error);

                });

        },
        error: function (error) {
            request.log.error(JSON.stringify(error, null, 2))
            response.error(error)
        },
        useMasterKey: true
    });

    function removeAllSwipes() {
        var query = new Parse.Query("Swipes");
        query.find({
            success: function (results) {

                //What do I do HERE to delete the posts?
                // users.forEach(function(user) {
                //     user.destroy({
                //         success: function() {
                //             // SUCCESS CODE HERE, IF YOU WANT
                // request.log.info('Pulled ' + results.length + ' swipes')

                // response.message(results.length)

                //         },
                //         error: function() {
                //             // ERROR CODE HERE, IF YOU WANT
                //         }
                //     });
                // })
                Parse.Object.destroyAll(results, {
                    useMasterKey: true
                })
                    .then(function (success) {
                        // console.log('all swipes deleted');
                        // All the objects were deleted
                        // request.log.info('Deleted swipes')

                        removeAllMatches()

                    }, function (error) {
                        request.log.error(JSON.stringify(error, null, 2))

                        console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                        response.error(error);

                    });

            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
                response.error(error)
            },
            useMasterKey: true
        });
    }

    function removeAllMatches() {
        var query = new Parse.Query("Matches");
        query.find({
            success: function (results) {

                //What do I do HERE to delete the posts?
                // users.forEach(function(user) {
                //     user.destroy({
                //         success: function() {
                //             // SUCCESS CODE HERE, IF YOU WANT
                // request.log.info('Pulled ' + results.length + ' matches')

                // response.message(results.length)

                //         },
                //         error: function() {
                //             // ERROR CODE HERE, IF YOU WANT
                //         }
                //     });
                // })
                Parse.Object.destroyAll(results, {
                    useMasterKey: true
                })
                    .then(function (success) {
                        // console.log('all matches deleted');
                        // All the objects were deleted
                        // request.log.info('Deleted matches')

                        removeAllMessages()

                    }, function (error) {
                        request.log.error(JSON.stringify(error, null, 2))

                        console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                        response.error(error);

                    });

            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
                response.error(error)
            },
            useMasterKey: true
        });
    }

    function removeAllMessages() {
        var query = new Parse.Query("Messages");
        query.find({
            success: function (results) {

                //What do I do HERE to delete the posts?
                // users.forEach(function(user) {
                //     user.destroy({
                //         success: function() {
                //             // SUCCESS CODE HERE, IF YOU WANT
                // request.log.info('Pulled ' + results.length + ' messages')

                // response.message(results.length)

                //         },
                //         error: function() {
                //             // ERROR CODE HERE, IF YOU WANT
                //         }
                //     });
                // })
                Parse.Object.destroyAll(results, {
                    useMasterKey: true
                })
                    .then(function (success) {
                        // console.log('all messages deleted');
                        // All the objects were deleted
                        // request.log.info('Deleted messages')

                        response.success('done')

                    }, function (error) {
                        request.log.error(JSON.stringify(error, null, 2))

                        console.error("Oops! Something went wrong: " + error.message + " (" + error.code + ")");
                        response.error(error);

                    });

            },
            error: function (error) {
                request.log.error(JSON.stringify(error, null, 2))
                response.error(error)
            },
            useMasterKey: true
        });
    }
    // function removeTestRelations() {
    //     getUser(targetUserIdForTests).then(
    //         //When the promise is fulfilled function(user) fires, and now we have our USER!
    //         function(user) {
    //             user.set('rightSwipes', null)
    //             user.set('leftSwipes', null)
    //             user.set('upSwipes', null)

    //             user.save(null, {
    //                 success: function(list) {
    //                     // All the objects were saved.

    //                     //remove all swiopes 
    //                     removeAllSwipes()


    //                 },
    //                 error: function(error) {
    //                     // An error occurred while saving one of the objects.
    //                     response.error(error);

    //                 }
    //             });

    //         },
    //         function(error) {
    //             response.error(error);
    //         }
    //     );
    // }


});




//after save to manage matches
Parse.Cloud.afterSave("Swipes", function (request) {
    //see if there was a matching swipe, and if so, create a Match object
    var query = new Parse.Query("Swipes");
    var senderUser = request.object.get("sender");
    var recipientUser = request.object.get("recipient");
    var direction = request.object.get("direction");
    // console.log("Parse.Cloud.afterSave(Swipes direction", direction)
    if (direction == 'right') {
        query.equalTo('recipient', senderUser)
        query.equalTo('sender', recipientUser)
        query.find({
            success: function (results) {
                // console.log("Parse.Cloud.afterSave(Swipes results[0].attributes.direction", results[0].attributes.direction)
                if (results && results.length > 0 && results[0].attributes.direction == 'right') {
                    //both swiped, but also need to make sure that we don't already have a match
                    var checkQuery1 = new Parse.Query("Matches")
                    checkQuery1.equalTo('user1', senderUser)
                    checkQuery1.equalTo('user2', recipientUser)

                    //and flip it for check
                    var checkQuery2 = new Parse.Query("Matches")
                    checkQuery2.equalTo('user1', recipientUser)
                    checkQuery2.equalTo('user2', senderUser)

                    var mainQuery = Parse.Query.or(checkQuery1, checkQuery2);
                    mainQuery.find({
                        success: function (results) {
                            // console.log("Parse.Cloud.afterSave(Swipes match found", results, results.length)
                            if (results && results.length == 0) {
                                console.log('match found');
                                createMatch()
                            }
                        },
                        error: function (error) {
                            // There was an error.
                        }
                    });

                }
            },
            error: function (error) {
                // request.log.error(JSON.stringify(error, null, 2))
                // response.error(error)

                // console.error(error)
                //return $q.reject(error)
            }
        })
    }

    function sendMatchPush(sender, receiver) {
        // console.log("Parse.Cloud.afterSave(Swipes sendMatchPush", sender, receiver)
        var payloadToSender = {
            'type': 'match',
            'userId': receiver.id,
            'userName': receiver.firstName,
            'userPhoto': receiver.publicPhotoLink0,
            "content-available": 1
        }
        var payloadToReceiver = {
            'type': 'match',
            'userId': sender.id,
            'userName': sender.firstName,
            'userPhoto': sender.publicPhotoLink0,
            "content-available": 1
        }

        sendPush(sender.id, payloadToSender);
        sendPush(receiver.id, payloadToReceiver);


    }


    function createMatch() {
        // console.log("Parse.Cloud.afterSave(Swipes createMatch")
        //good, create a message object
        var Matches = Parse.Object.extend('Matches')
        var match = new Matches()
        match.set('matchDateX', Date.now())
        match.set('lastMessageText', 'New Match')
        match.set('lastMessageSender', null)
        match.set('lastMessageDateX', Date.now()) //setting this to right away so that our UI sorts correctly
        match.set('hasUnread', true)
        match.set('matchDate', new Date())
        match.set('user1', senderUser)

        match.set('user2', recipientUser)
        // var relation = match.relation("users");
        // relation.add(senderUser);
        // relation.add(recipientUser);
        // now save the message object

        //add the public data
        getUser(senderUser.id)
            .then(
            function (senderUserData) {
                // console.log("Parse.Cloud.afterSave(Swipes createMatch senderUserData", senderUserData)
                match.set('user1Data', {
                    id: senderUserData.id,
                    firstName: senderUserData.get('firstName'),
                    publicPhotoLink0: senderUserData.get('publicPhotoLink0')
                })

                getUser(recipientUser.id)
                    .then(
                    function (recipientUserData) {
                        // console.log("Parse.Cloud.afterSave(Swipes createMatch recipientUserData", recipientUserData)
                        match.set('user2Data', {
                            id: recipientUserData.id,
                            firstName: recipientUserData.get('firstName'),
                            publicPhotoLink0: recipientUserData.get('publicPhotoLink0')
                        })
                        match.save();

                        //Send push to Sender
                        var sender = {
                            id: senderUserData.id,
                            firstName: senderUserData.get('firstName'),
                            publicPhotoLink0: senderUserData.get('publicPhotoLink0')
                        }

                        //Send push to Receipient
                        var receiver = {
                            id: recipientUserData.id,
                            firstName: recipientUserData.get('firstName'),
                            publicPhotoLink0: recipientUserData.get('publicPhotoLink0')
                        }
                        // console.log("Parse.Cloud.afterSave(Swipes createMatch sendMatchPush", sender, receiver);
                        sendMatchPush(sender, receiver);

                    })
                    .catch(function (error) {
                        console.error(error);
                    })
            })
            .catch(function (error) {
                console.error(error);
            })

    }
    // .then(function (post) {
    //     post.increment("comments");
    //     return post.save();
    // })
    // .catch(function (error) {
    //     console.error("Got an error " + error.code + " : " + error.message);
    // });

})

//save the user's age after the birthday is saved
Parse.Cloud.afterSave(Parse.User, function (request) {
    var userId = request.object.id

    getUser(userId).then(
        //When the promise is fulfilled function(user) fires, and now we have our USER!

        function (userResponse) {
            var user = userResponse;
            // if (!user.get("age")) {
                var age = 18;
                age = getAge(user.get('birthdayMMDDYYYY'))
                if(user.get('age') && age == user.get('age')){
                    // user age already set. Breaking the loop here
                    console.log('/////////// user age already set, not changing ///////////');
                }
                else{
                    user.set('age', age)
                    user.save(null, {
                        success: function (list) {
                            // All the objects were saved.
                            //purposely no handler here because of the timeout function
    
                        },
                        error: function (error) {
                            // An error occurred while saving one of the objects.
    
                            request.log.error(error)
                        },
                        useMasterKey: true
                    });
                }
                
            // }
        },
        function (error) {
            console.error(error)
        });

})

//update match info after a message is sent, namely the last sent msg and the time
Parse.Cloud.afterSave("Messages", function (request) {
    //need to update the match object

    var sender = request.object.get('sender');
    var recipient = request.object.get('recipient');

    // request.log.info('aftesave start for messages')

    //Send push to receiver
    var receiver = recipient.toJSON();
    var message = request.object.get('messageText');
    // console.log(receiver);
    // console.log('message');
    // console.log(message);
    sendPush(receiver.objectId, {
        'type': 'chat',
        'sender': sender,
        'content-available': 1,
        'message': message,
        'time': new Date(request.object.get('createdAt')).getTime(),
        "priority": 10
    });

    var query1 = new Parse.Query("Matches");
    query1.equalTo('user1', sender)
    query1.equalTo('user2', recipient)

    var query2 = new Parse.Query("Matches");
    query2.equalTo('user1', recipient)
    query2.equalTo('user2', sender)

    var mainQuery = Parse.Query.or(query1, query2);

    mainQuery.find({
        success: function (results) {
            if (results && results.length > 0) {
                var matchObject = results[0];
                matchObject.set('lastMessageDateX', request.object.get('messageDateX'))
                matchObject.set('lastMessageText', request.object.get('messageText'))
                matchObject.save(null, {
                    success: function (list) {
                        // All the objects were saved.
                        //purposely no handler here because of the timeout function

                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.

                        request.log.error(error)
                    },
                    useMasterKey: true
                });
            }
        },
        error: function (error) {
            request.log.error(JSON.stringify(error, null, 2))
            // response.error(error)

            console.error(error)
            //return $q.reject(error)
        }
    })

})

function sendPush(channel, payload) {
    var alertText = '';
    if (payload.type == "chat") {
        alertText = "New message"
    } else if (payload.type == "manualVerification") {
        alertText = "Manual verification report"
    }
    else {
        alertText = 'You have a new match!';
    }
    //Send to Sender
    Parse.Push.send({
        channels: [channel],
        data: {
            alert: alertText, // Set our alert message.
            badge: 'Increment', // Increment the target device's badge count.
            payload: payload,
            sound: "default",
            "content-available": 1
        }
    }, {
            useMasterKey: true
        })
        .then(function () {
            // console.log('push sent. args received: ' + JSON.stringify(arguments) + '\n');
            res.success({
                status: 'push sent',
                ts: Date.now()
            });
        }, function (error) {
            console.log('push failed. ' + JSON.stringify(error) + '\n');
            res.error(error);
        });
}

tempUserData = {
    "heightInches": 3,
    "birthdayMMDDYYYY": "10-22-1993",
    "publicPhotoLink0": "http://maxpixel.freegreatpicture.com/static/photo/1x/Girl-Black-Tight-Dress-Beautiful-Woman-1293985.jpg",
    "interestedInString": "women",
    "username": "OYiJOkvqslTwudBaSkDpoDb7k",
    "primarySport": "Football",
    "publicPhotoLink3": "http://static.thefrisky.com/uploads/2015/08/teenage-athlete-640x427.jpg",
    "profileDescription": "Hello",
    "genderString": "men",
    "updatedAt": "2017-08-13T22:11:05.773Z",
    "ageMax": 41,
    "locationGeoPoint": {
        "__type": "GeoPoint",
        "latitude": 39.7697589,
        "longitude": -105.0114287
    },
    "school": "University of Northern Colorado",
    "firstName": "Jared",
    "heightFeet": 6,
    "createdAt": "2017-07-31T20:21:09.299Z",
    "distanceMilesMax": 50,
    "locationString": "Northwest, Denver, CO, USA",
    "graduationYear": 2017,
    "publicPhotoLink2": "http://maxpixel.freegreatpicture.com/static/photo/1x/Girl-Black-Tight-Dress-Beautiful-Woman-1293985.jpg",
    "publicPhotoLink4": "https://cdn2.omidoo.com/sites/default/files/imagecache/full_width/images/bydate/aug_20_2011_-_447pm/shutterstock_62248267.jpg",
    "ageMin": 23,
    "publicPhotoLink1": "http://theathleticbuild.com/wp-content/uploads/2014/10/jess-ennis-307x286.jpg"
}

malePhotoString = "https://ae01.alicdn.com/kf/HTB14jLOJFXXXXcLXpXXq6xXFXXXq/2016-font-b-Athletics-b-font-Printed-T-Shirt-font-b-men-b-font-Fitness-and.jpg";