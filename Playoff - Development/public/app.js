/**
 * Created by abhishekrathore on 12/23/16.
 */
(function () {
    'use strict';

    angular
        .module('playoff', [
            'ui.router',
            'ngMap',
            'ng-sweet-alert'
        ]);

})();
