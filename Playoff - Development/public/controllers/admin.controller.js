angular.module('playoff')
    .controller('AdminLoginCtrl', AdminLoginCtrl)
function AdminLoginCtrl($timeout, $location, $rootScope, $state, $scope) {
    console.log('load')
    $rootScope.loggedIn = false;
    var login = this;
    login.user = {};
    login.loginError = false;
    $rootScope.loginPage = false;
    login.login = function () {
        Parse.User.logIn(login.user.username, login.user.password, {
            success: function (currentUser) {
                console.log('successfully login', Parse.User.current());
                if (currentUser.id == 'eEbX3ECNsY') {
                    $rootScope.loggedIn = true;
                    $state.go('home');
                } else {
                    $timeout(function () {
                        login.loginError = true;
                        Parse.User.logOut();
                    }, 50);
                }
            },
            error: function (err) {
                console.log("error in login", err);
                $timeout(function () {
                    login.loginError = true;
                }, 50);
            }
        });
    }
    login.change = function () {
        $timeout(function () {
            login.loginError = false;
        }, 20);
    }
    $scope.logOut = function () {
        console.log('logout');
        Parse.User.logOut();
    }
}