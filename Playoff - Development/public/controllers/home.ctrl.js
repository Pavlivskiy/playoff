(function () {
    'use strict';

    angular
        .module('playoff')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['DataService', 'SweetAlert', 'NgMap', '$state'];

    /* @ngInject */
    function HomeCtrl(DataService, SweetAlert, NgMap, $state) {
        const home = this;
        home.title = 'Playoff Users';
        home.chatSessions = 0;
        home.selectedTab = 1;
        home.menuItem = 'users'
        home.currentPage = 0;
        home.gap = 5;
        home.totalUsers = 0;
        home.totalPages = 5;
        home.showMap = false;
        console.log('Home Ctrl');

        if (Parse.User.current() && Parse.User.current().id == 'eEbX3ECNsY') {
            activate();
        } else {
            $state.go('login');
            if (Parse.User.current())
                Parse.User.logOut();
        }

        home.goToUsers = function () {
            console.log('go to users')
            home.menuItem = 'users';
            home.title = "Playoff Users"
        }

        home.goToLocation = function () {
            console.log('go to location')
            home.menuItem = 'location';
            home.title = "Playoff Users' Location"
        }

        home.selectTab = function (index) {
            home.selectedTab = index;
        }

        home.changeStatus = function (user) {

            if (user.verified) {
                SweetAlert.confirm('This user has previously been verified. Do you want to mark this user as Unverified?', { title: 'Mark as Unverified', type: "info" })
                    .then(function (p) {
                        if (p) {
                            console.log('true');
                            DataService.markUnverified(user.objectId).then(function (userData) {
                                console.log('user marked as not verified');
                                user.verified = false;
                            })
                        }
                        else {
                            console.log('false');
                        }
                    }
                    );
            }
            else if (!user.verified) {
                SweetAlert.confirm('This user has not been verified yet. Do you want to mark this user as Verified?', { title: 'Mark as Verified', type: "info" })
                    .then(function (p) {
                        if (p) {
                            console.log('true');
                            DataService.markVerified(user.objectId).then(function (userData) {
                                console.log('user marked as verified');
                                user.verified = true;
                            })
                        }
                        else {
                            console.log('false')
                        }
                    }
                    );
            }
        }

        function processUsers(userData) {

            home.allUsers = [];
            _.each(userData, function (user) {
                home.allUsers.push({
                    name: user.firstName,
                    sex: user.genderString,
                    age: user.age,
                    joiningDate: user.createdAt,
                    jerseyImage: user.jerseyImage,
                    profilePhoto: user.publicPhotoLink0,
                    photo1: user.publicPhotoLink1,
                    photo2: user.publicPhotoLink2,
                    photo3: user.publicPhotoLink3,
                    photo4: user.publicPhotoLink4,
                    verified: user.manuallyVerified,
                    objectId: user.objectId
                })
            })

            home.newUsers = home.allUsers.filter(function (user) {
                return user.name && user.jerseyImage && user.profilePhoto && user.verified == 'undefined'
            })
            console.log(home.newUsers);
        }

        function activate() {
            console.log("PLayoff users");
            DataService.getUsersCount().then(function (count) {
                home.totalUsers = count;
                home.totalPages = parseInt(count / 100) + 1;
                console.log(home.totalUsers);
            }, function (err) {
                console.log(err);
            });
            DataService.getUserlist().then(function (userData) {
                console.log('pagination on the way');
                home.dynMarkers = [];
                home.markerClusterer = [];
                NgMap.getMap().then(function (map) {
                    _.each(userData, function (user) {
                        if (user.locationGeoPoint) {
                            var latLng = new google.maps.LatLng(user.locationGeoPoint.latitude, user.locationGeoPoint.longitude);
                            home.dynMarkers.push(new google.maps.Marker({ position: latLng }));
                        }
                    });
                    home.markerClusterer = new MarkerClusterer(map, home.dynMarkers, {});
                    home.showMap = true;
                });
            }, function (err) {
                console.log(err);
            });
            DataService.getUsersPerPage(home.currentPage).then(function (userData) {
                console.log('pagination on the way');
                home.showMap = true;
                home.dynMarkers = [];
                home.markerClusterer = [];
                processUsers(userData);
                home.usersLoaded = true;
            }, function (err) {
                console.log(err);
            });
            DataService.getChatSessionCount().then(function (data) {
                console.log(data);
                home.chatSessions = data;
            }, function (err) {
                console.log(err);
            });

        }
        function getUsersData(pageNumber) {
            DataService.getUsersPerPage(pageNumber).then(function (userData) {
                console.log('pagination on the way');
                home.dynMarkers = [];
                home.markerClusterer = [];
                processUsers(userData);
                home.usersLoaded = true;
            }, function (err) {
                console.log(err);
            });
        }
        home.range = function (size, start, end) {
            var ret = [];
            //  console.log(size, start, end);

            if (size < end) {
                end = size;
                start = size - home.gap;
            }
            for (var i = start; i < end; i++) {
                ret.push(i);
            }
            //  console.log(ret);
            return ret;
        };
        home.prevPage = function () {
            if (home.currentPage > 0) {
                home.currentPage--;
                getUsersData(home.currentPage);
            }
        };

        home.nextPage = function () {
            if (home.currentPage < home.totalPages - 1) {
                home.currentPage++;
                getUsersData(home.currentPage);
            }
        };

        home.setPage = function (goTo) {
            home.currentPage = goTo;
            getUsersData(home.currentPage);
        };
        home.unverifyUser = function (user) {
            SweetAlert.confirm('Do you want to mark this user as Unverified?', { title: 'Mark as Unverified', type: "info" })
                .then(function (p) {
                    if (p) {
                        console.log('true');
                        DataService.markUnverified(user.objectId).then(function (userData) {
                            console.log('user marked as not verified');
                            user.verified = false;
                        })
                    }
                    else {
                        console.log('false');
                    }
                }
                );
        }
    }

})();

