(function () {
    'use strict';

    angular
        .module('playoff')
        .service('DataService', DataService);

    DataService.$inject = ['$http', '$q', 'usersList'];

    /* @ngInject */
    function DataService($http, $q, usersList) {
        this.getUserlist = getUserlist;
        this.getChatSessionCount = getChatSessionCount;
        this.markVerified = markVerified;
        this.markUnverified = markUnverified;
        let tempData = [];
        let queryCounter = 0;

        this.getUsersCount = getUsersCount;
        this.getUsersPerPage = getUsersPerPage;
        function getUserlist() {
            
            var defer = $q.defer();
            var userObject = Parse.Object.extend('User');
            var query = new Parse.Query(userObject);
            getUsers(queryCounter * 999);
            //query.notEqualTo('roleName', 'Admin');
            function processUserList(userList) {
                _.each(userList, (job) => {
                    tempData.push(job.toJSON());
                });
                queryCounter++;
                getUsers(queryCounter * 999);
            }
            function getUsers(skip) {
                query.skip(skip)
                query.limit(999);
                query.find({ useMasterKey: true }).then((success) => {
                    var data = success;
                    console.log('users retreived = ' + data.length)
                    if (data.length == 999) {
                        console.log('users retreived = 999')
                        processUserList(data);
                    }
                    else {
                        console.log('users retreived = ' + data.length);
                        _.each(data, (job) => {
                            tempData.push(job.toJSON());
                        });
                        defer.resolve(tempData);
                    }
                }, (err) => {
                    console.log(err);
                    defer.reject(err);
                });
            }

            return defer.promise;
        }
        function getUsersPerPage(pageNumber) {
            var defer = $q.defer();
            if (usersList.data[pageNumber] && usersList.data[pageNumber].length) {
                defer.resolve(usersList.data[pageNumber]);
            } else {
                var skip = pageNumber * 100;
                let tempData = [];
                var userObject = Parse.Object.extend('User');
                var query = new Parse.Query(userObject);
                query.descending('createdAt');
                query.skip(skip);
                query.limit(100);
                //query.notEqualTo('roleName', 'Admin');
                query.find({ useMasterKey: true }).then((success) => {
                    var data = success;
                    _.each(data, (job) => {
                        tempData.push(job.toJSON());
                    });
                    usersList.data[pageNumber] = tempData;
                    defer.resolve(usersList.data[pageNumber]);
                }, (err) => {
                    console.log(err);
                    defer.reject(err);
                });
            }
            return defer.promise;
        }
        function getUsersCount() {
            var defer = $q.defer();
            var userObject = Parse.Object.extend('User');
            var query = new Parse.Query(userObject);
            query.count({ useMasterKey: true }).then((success) => {
                defer.resolve(success);
            }, (err) => {
                console.log(err);
                defer.reject(err);
            });
            return defer.promise;
        }
        function markVerified(userId) {
            // verification code here
            var defer = $q.defer();
            var userObject = Parse.Object.extend('User');
            var newUserObj = new userObject();
            newUserObj.id = userId;
            newUserObj.set('manuallyVerified', true)
            newUserObj.save({}, { useMasterKey: true }).then(function (success) {
                defer.resolve(success);
            }, function (err) {
                defer.reject(err);
            })
            return defer.promise;
        }
        function markUnverified(userId) {
            // un-verification code here
            var defer = $q.defer();
            var userObject = Parse.Object.extend('User');
            var newUserObj = new userObject();
            newUserObj.id = userId;
            newUserObj.set('manuallyVerified', false)
            newUserObj.save({}, { useMasterKey: true }).then(function (success) {
                defer.resolve(success);
            }, function (err) {
                defer.reject(err);
            })
            return defer.promise;
        }
        function getChatSessionCount() {
            var defer = $q.defer();
            var matchesObject = Parse.Object.extend('Matches');
            var query = new Parse.Query(matchesObject);
            //query.limit(999);
            query.notEqualTo('lastMessageText', 'New Match');
            query.count({ useMasterKey: true }).then((success) => {
                console.log(success);
                defer.resolve(success);
            }, (err) => {
                console.log(err);
                defer.reject(err);
            });
            return defer.promise;
        }
    }

})();

