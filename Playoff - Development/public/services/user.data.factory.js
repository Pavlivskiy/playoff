(function () {
    'use strict';

    angular
        .module('playoff')
        .factory("usersList", function () {
            return {
                data: []
            };
        })
})();