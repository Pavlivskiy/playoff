
## Admin Panel Files 

Following is the file structure for admin panel files. All files reside in `Playoff - Development/public` folder

```
root
     |--- node_modules (auto installed)
     |--- Playoff - Development (cloud code files + web admin panel files)
     |      |--- cloud (Keeps all the cloud code files)
     |      |--- public (keeps all web admin panel files)
     |              |--- controllers (JS logic files i.e. controllers in MVC)
     |              |--- css (css files)
     |              |--- fonts (font files)
     |              |--- lib (front end libraries)
     |              |--- services (service/ model files for the app)
     |              |--- views (all html files for views)
     |              |--- www (main folder with all js files)
     |              |--- .bowerrc (contains directory path for bower components i.e. www/lib in our case)
     |              |--- app.config.js 
     |              |--- app.constant.js (app constants)
     |              |--- app.js (main js file to initialize the web admin panel app)
     |              |--- app.routes.js (routes for views)
     |              |--- app.run.js 
     |              |--- index.html (main file for admin panel)
     |              |--- materialize.min.js (for materialize css)
     |--- .... 

```

* Admin panel works like a regular angular app. New pages can be added in `views`, with corresponding controller in `controllers` folder, attaching a route in `app.routes.js`
* Any other css or js file can be added similar to a regular angular app
* To deploy admin panel changes, you just need to `deploy` the code similar to when you deploy a cloud code, using `b4a deploy`. All changes will take effect immediately.

> Caution : Back4app does not give an option to install node_modules on their back-end freely. So try to install all dependencies using `bower` or keep the required files in an enclosed folder which you can control 
If installing `node_modules` is essential, contact back4app support, and they will install the required thing on the back-end