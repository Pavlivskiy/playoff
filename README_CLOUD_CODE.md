## Installation

### MAC AND LINUX
In Mac OS X and Linux environments, you can get the parse command line tool by running this command:

```
curl https://raw.githubusercontent.com/back4app/parse-cli/back4app/installer.sh | sudo /bin/bash
```

This installs a tool named “b4a” to =/usr/local/bin/b4a. There’s no other junk, so to uninstall, just delete that file. This will also update your command line tool if you already have it installed.

### WINDOWS
Back4App command line interface for Windows is available here (https://github.com/back4app/parse-cli/releases).

You have to download b4a.exe

![b4a_exe](https://docs.back4app.com/wp-content/uploads/2017/01/b4a_exe-1.png)

Now, we are going to homologate the downloaded program

* Open the folder where b4a.exe file is
* Copy the file
* Go to `C:\windows\system32`
* Paste the file
* 
Note that this is not an installer, it is just a plain Windows executable.


## Account Keys
You can configure the command line interface to use account keys to perform various actions like creating a new app and fetching master key for an app when required (for instance, when performing deploy, fetching logs, etc.).

As noted in this section, we’ll not store master keys in config files anymore. Instead we’ll fetch them as needed.

### WHAT DOES THIS MEAN FOR A REGULAR DEPLOY FLOW?
Apps API requires user authentication in the form of (`email`, `password`) with which you registered for the Back4App account, or a valid account key (created for that account).

If you do not configure account keys, you’ll be prompted to enter an email & password. This disruption in the flow (for all commands requiring master key), can easily be averted by configuring an account key using the `b4a configure accountkey` command.

```
$ b4a configure accountkey
Input your account key or press ENTER to generate a new one.
NOTE: on pressing ENTER we’ll try to open the url:
“https://www.parse.com/account/keys”
in default browser.
Account key:
```

But,where is my account key?

#### 1.Enter at your Back4App account
![login](https://docs.back4app.com/wp-content/uploads/2017/01/Captura-de-Tela-2017-01-19-%C3%A0s-13.19.29.png)

* Click on the blue box ‘sign in’ at the high right corner of the site

* Fill the blanks with your e-mail and password, after this, click on the blue box ‘login’

#### 2.After doing this, click on your name at the high right corner of the site
Click on the option ‘accountkey’

![accountKey](https://docs.back4app.com/wp-content/uploads/2017/01/Captura-de-Tela-2017-01-19-%C3%A0s-13.26.56.png)


#### 3.If there is an accountkey saved, copy and paste at the terminal
![accountKey](https://docs.back4app.com/wp-content/uploads/2017/01/Captura-de-Tela-2017-01-19-%C3%A0s-13.28.04.png)

![key](https://docs.back4app.com/wp-content/uploads/2017/01/Captura-de-Tela-2017-01-19-%C3%A0s-13.30.14.png)

#### 4.If there is not an accountkey
* name a new key

![new_key](https://docs.back4app.com/wp-content/uploads/2017/01/Captura-de-Tela-2017-01-19-%C3%A0s-13.31.43.png)

* Click on the gray box with a + signal and click on the green box ‘save’

* Copy and paste the key at the terminal

 
```
Account Key: your_accountkey

Successfully stored account key for: “account_email”
 ```

Account keys you configure are stored in `${HOME}/.back4app/netrc` (Mac and Linux) and `C:\Users\yourName\.back4app\netrc` (Windows)



## Creating a Back4App app
You can create a new Back4App app using `b4a new`. It asks you a series of questions and at the end of it, you will have a new Back4app app (with the given name). Additionally, you will also set up a Cloud Code project for the app. You can test that everything works by performing `b4a deploy` and executing the curl command printed at the end of output.

Let’s do it step by step!
#### 1.We will create a Back4App app or add Cloud Code to an existing app
Open your terminal and type the next command

```
$ b4a new
Would you like to create a new app, or add Cloud Code to an existing app?
Type "(n)ew" or "(e)xisting": n
Please choose a name for your Parse app.
Note that this name will appear on the Back4App website,
but it does not have to be the same as your mobile app's public name.
Name: CLI-Back4App
```
#### 2.Creating the directory Name
If you want to give other name to the directory type it,if not,hit ENTER.
```
Awesome! Now it's time to set up some Cloud Code for the app: "CLI-Back4App",
Next we will create a directory to hold your Cloud Code.
Please enter the name to use for this directory,
or hit ENTER to use "CLI-Back4App" as the directory name.
```
Now,you have the options to set up a blank project or create a simple Cloud Code project
```
You can either set up a blank project or create a sample Cloud Code project.
Please type "(b)lank" if you wish to setup a blank project, otherwise press ENTER:<enter>
```
Successfully configured email for current project to: `${ACCOUNT_EMAIL}`


Your Cloud Code has been created at `${CUR_DIR}/app_name`

This includes a “Hello world” cloud function, so once you deploy, you can test that it works, with the printed curl command.

#### 3.Deploy the app
> Note:Be careful! You have to be on the folder of the app that we want to deploy!

Next, you might want to deploy this code with:

```
$ b4a deploy
Uploading source files
Uploading recent changes to scripts...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/cloud/main.js
Uploading recent changes to hosting...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/public/index.html
Finished uploading files
New release is named v1 (using Parse JavaScript SDK v2.2.25)
```
Once deployed you can test that it works by running:
```
curl -X POST \

-H "X-Parse-Application-Id: ${APPLICATION_ID}" \

-H "X-Parse-REST-API-Key: ${REST_API_KEY}" \

-H "Content-Type: application/json" \ -d '{}' \

https://parseapi.back4app.com/functions/hello
```
You can see the result os our function
```
{"result":"Hello world!"}
```
#### 5.Adding other app to the same folder
We can add an app to the same folder of another created!

To do this,open the terminal and do the `b4a add` command
```
$ b4a add
1:back4app
2:myb4app
3:myback4app
Select an app to add to configure:
Written config for "AppChosen"
```
It was added successfully!



## Setting up Cloud Code
If you already created a Back4App app (for example, through the website or apps API) and want to set up Cloud Code for the app, `b4a new` can still help you.

Open your terminal and type the next command
```
$ b4a new
Would you like to create a new app, or add Cloud Code to an existing app?
Type "(n)ew" or "(e)xisting": n
Please choose a name for your Parse app.
Note that this name will appear on the Back4App website,
but it does not have to be the same as your mobile app's public name.
Name: CLI-Back4App
```
```
Awesome! Now it's time to set up some Cloud Code for the app: "CLI-Back4App",
Next we will create a directory to hold your Cloud Code.
Please enter the name to use for this directory,
or hit ENTER to use "CLI-Back4App" as the directory name.
```
You can either set up a blank project or create a sample Cloud Code project.
Please type “(b)lank” if you wish to setup a blank project, otherwise press ENTER:

Successfully configured email for current project to: `${ACCOUNT_EMAIL}`

Your Cloud Code has been created at `${CUR_DIR}/appName`

This includes a “Hello world” cloud function, so once you deploy, you can test that it works, with the printed curl command.

* Next, you might want to deploy this code with:

```
$ b4a deploy
Uploading source files
Uploading recent changes to scripts...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/cloud/main.js
Uploading recent changes to hosting...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/public/index.html
Finished uploading files
New release is named v1 (using Parse JavaScript SDK v2.2.25)
```
Once deployed you can test that it works by running:
```
curl -X POST \

-H "X-Parse-Application-Id: ${APPLICATION_ID}" \

-H "X-Parse-REST-API-Key: ${REST_API_KEY}" \

-H "Content-Type: application/json" \ -d '{}' \

https://parseapi.back4app.com/functions/hello
```
You can see the result as our function
```
{"result":"Hello world!"}
```
Note: This flow assumes that you already set up account keys. See also multiple account keys, if you are developing on apps belonging to different Parse accounts.

The `appCreatedDirectory` directory looks like:

 

![directory](https://docs.back4app.com/wp-content/uploads/2017/01/Captura-de-Tela-2017-01-11-%C3%A0s-18.30.58.png)

`.parse.local` and `.parse.project` are config files that store application info, and project level info (for instance, Javascript SDK version), respectively. The `cloud` directory stores your Cloud Code, and the `public` directory stores any static content that you want to host on Parse. In the `cloud` directory, you’ll typically just be editing `main.js`, which stores all of your Cloud Code functions.

For now, just check that these files were created successfully. If you’re using source control, you can check all of these files in.

We recommend using source control to check in all of these files. If you’re not already set up with source control, try this tutorial from GitHub.

The same code can be deployed to multiple different applications. This is useful so that you can have separate `development` and `production` applications. Then you test the code on a `development` application before launching it in `production`.

The first application that is added (by the `new` command) will be the default application for all command line operations. All commands except `new` take an optional app name that the command will be performed on.

## Developing Cloud Code
You can also run the Back4App command line interface in development mode using the develop command. This will make the tool watch the source directory for any updates and deploy them to Back4app. It also provides a live stream of Cloud Code logs.

> Note:Be careful! You have to be at the folder of the app!

Type `b4a list` to view all the apps that are linked to this one, after this, type `b4a develop appName`

```
$ b4a list
The following apps are associated with Cloud Code in the current directory:
*Back4App
These are the apps you currently have access to:
1:Back4App
2:MyBack4App
3:Myapp
$b4a develop Back4App
Your changes are now live.
```
Notice that terminal will inform you every time that the app is modified

Unlike the other commands, for `develop` you must specify the Parse App to push updates to. This is to avoid accidentally running develop on your production app causing you to run untested code in your production app.


## Adding a New Target
You can add a new Back4App app as a target by running the `b4a add` command.

```
$ b4a add
1:Back4App
2:myb4ap
3:MyBack4App
Select an App to add to config:
```
The `b4a add` command takes an optional argument which is an alias to assign to the application that can be used instead of the app name.


## Setting the Default App
`b4a deploy`, `b4a logs`, `b4a rollback`, and `b4a releases` use the `default` app to be run against the commands. `b4a default` allows you to change this default app.

type at the terminal:

```
$b4a default appName
Default set to appName
$b4a default
Current default app is appName
```
