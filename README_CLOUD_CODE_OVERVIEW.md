## Cloud code

Following are the `aftersave` functions running in cloudCode (`aftersave` functions run automatically when some data is modified or added in the database)

* Swipes - Watches the `Swipes` table of the database. Each `left` and `right` swipe goes in the `Swipes` table. This piece of code takes care of the logic that should run after a `left` or `right` swipe by the user
* Parse.User - Watches the `_User` table of the database. This serves a simple purpose of setting user `age` as per the `DOB` entered by the user, or fetched form Facebook data. 
> Caution : There is an `if-else` logic written inside this function. Without this logic, this `aftersave` function can go in infinite loop
* Messages - Watches the `Messages` table of the database. Once a message is sent by a User, this function updates the `last_sent` message and `last_sent_time` for the same

### Cloud functions called as REST APIs

* checkInAppPurchaseStatus - To fetch information regarding the user's payment to determine the renewal status, receipt etc.
* getNext10InStackForUser - Finds the next 10 users for the current userId
Three queries run in parallel here 
    * Query 1 - 
        * Checks if user has a geolocation
        * User's profile should not be a `deleted` one
        * User's profile should be manually verified
        * Premium options - Identified by binary notation
            - 00 - Don't match by sport, don't match by sschool
            - 01 - Don't match by sport, match by sschool
            - 11 - Match by sport, match by sschool
            - 10 - Match by sport, match by sschool
        * User's age should be within `ageMax` and `ageMin`
        * The querying user's age should also be withing `ageMax` and `ageMin` of the user being queried
        * Gender preferences should match from both sides
        * Sorting priority
            * Primary sport matching users
            * School matching users
        * User has to be withing preferred `distance` of querying user, and vice versa
        * Return 10 such results at a time
    * Query 2 - Checks if the querying user has swiped the querying users, or not 
    * Query 3 - Checks if the queried user have likes the user. If yes, then put that user in priority


* getUserProfileInfo - Get the user's data so we have the location, preferences, etc.
* get50MostRecentMatchesForUser - To get recent matches of user to show in `settings` page of the app
* reset - When a user removes/deletes the profile, all the related data is removed
    * Removes all swipes
    * Removes all matches
    * Removes all chat messages
    * 

* manuallyVerifyUser - For admin, where admin can mark a user `verified` or `not verified` from admin panel
    * Set `manuallyVerified` field to `true` or `false`
    * Send a push to the user

* getAdminSettings - To get admin settings like app version codes and `versiondate` etc to show/hide the update alert

* add15TestUsersFunc - Used during development, to create new users for testing

### Parse cloud jobs 
Can be run from the Parse dashboard (not critical for live app)
* add150TestUsers - To add test users for development purpose
* removeAllTestUsers - To remove all test users from Database

### Internal functions
* sendPush - Send push to user with given payload
* getAge - Returns user age as per the DOB provided
* getUser - Returns user object based on userId provided