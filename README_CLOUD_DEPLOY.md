## Deploying

### (only this code is relevant to deploying changed code once you setup the CLI, apps etc properly)

Let’s deploy a new release
To do this, run `b4a deploy` from the command line:

> Note:Be careful! You have to be at the folder of the app that you have created!
```
$ b4a deploy
Uploading source files
Uploading recent changes to scripts...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/cloud/main.js
Uploading recent changes to hosting...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/public/index.html
Finished uploading files
New release is named v1 (using Parse JavaScript SDK v2.2.25)
 ```

This pushes the new code (in `cloud` and `public`) to the Parse Cloud and deploys this code for the default target which is the first app that was added or the one you set using  `b4a default`.You can choose to deploy to a different target by adding the target as an argument to `deploy`:
```
$ b4a deploy app_name
Uploading source files
Uploading recent changes to script…
The following files will be uploaded:
/users/user_name/app_name/cloud/main.js
Uploading recent changes to hosting…
The following files will be uploaded:
/users/user_name/app_name/public/asd.py
users/user_name/app_name/public/index.html
Finished uploading files
New release is named v1 (using Parse_JavaScript SDK v2.2.25)
```
You can add release notes to the deploy with the `-d` or `--description` option

If the contents of your Back4App project remain unchanged then we skip deploy. You will see an output like:

```
$ b4a deploy app_name
Uploading source files
Finished uploading files
Not creating a release because no files have changed
```

You can override this behavior with the `-f` or `--force` flag. Providing this flag forces a deploy despite no changes to your project.

When embedding `parse deploy` within other scripts (such as in an automated testing/deploy environment) you can rely on the exit code from the Parse command line tool to indicate whether the command succeded. It will have an exit code of 0 on success and a non-zero exit code when the deploy failed.