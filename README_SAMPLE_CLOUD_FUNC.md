## Making function
Following ancient tradition, let’s see how to run the simplest possible function in the cloud. If you take a look at `cloud/main.js`, you’ll see an example function that just returns a string:
```
Parse.Cloud.define(“hello”, function(request, response){
response.success(“Hello world!”);
});
```
To deploy the code from your machine to the Parse Cloud, run: `b4a deploy`

 
```
$ b4a deploy
Uploading source files
Uploading recent changes to scripts...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/cloud/main.js
Uploading recent changes to hosting...
The following files will be uploaded:
/Users/alysson/CLI-Back4app/public/index.html
Finished uploading files
New release is named v1 (using Parse JavaScript SDK v2.2.25)
```
Once deployed you can test that it works by running:
```
curl -X POST \

-H "X-Parse-Application-Id: ${APPLICATION_ID}" \

-H "X-Parse-REST-API-Key: ${REST_API_KEY}" \

-H "Content-Type: application/json" \ -d '{}' \

https://parseapi.back4app.com/functions/hello
```
You can see the result as our function
```
{"result":"Hello world!"}
```
Congratulations! You’ve successfully deployed and run Cloud Code.

This is a good time to play around with the deployment cycle. Try changing `Hello world!` to a different string, then deploy and run the function again to get a different result. The whole JavaScript SDK is available in Cloud Code, so there’s a lot you can do. The Cloud Code guide goes over various examples in more detail.