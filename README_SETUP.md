## Playoff Dating app

This is the documentation for Playoff Dating app installation and walkthrough. This is not a complete developer documentation (Mar 02, 2018)

#### Documentation Breakdown

 * Installation and build steps
 * Technologies used, versions and related info
 * File structure and usage



### 1. Installation and build steps

You must have cordova installed prior to this.

```bash
	npm install -g cordova ionic
```


NOTE: This will install Ionic 3 environment, but the source is in Ionic 1. No need to worry, because Ionic 3 environment can run Ionic 1 perfectly alright (.. alright alright!)


#### Installation of  project

* Clone the repo or extract the zip file you download and move to root folder

* Install npm dependecies

```
	npm install
```
This will create folder `node_modules` which keeps all dependencies other than front-end dependencies

* Install bower dependecies

```
	bower install
```
This will create folder `lib` inside `www` which keeps all front-end dependencies

* Install Resources (Ionic 3 environment)

```
	ionic cordova resources
```

This will install both iOS and Android resources. If you want to build only one, build using
```
	ionic cordova resources ios
    // OR
	ionic cordova resources android
```

Make sure you delete any `.md5` files created in `resources` folder in the root directory. `.md5` files stop new resources from getting created (caching mechanism)

* Add Platform (whichever required)

```bash
	ionic platform add android
	ionic platform add ios
```
in few cases, you might need to install the latest platform
```bash
	ionic platform add android@latest
	ionic platform add ios@latest
```

For successful Android platform addition, you need to have latest and updated Android Studio and/or Android SDK installed on your system. 
Plus you need to have `ANDROID_HOME` and `TOOLS` in your environment variables. 

For windows, use command prompt with the commands
```
set ANDROID_HOME=C:\ installation location \android-sdk

set PATH=%PATH%;%ANDROID_HOME%\tools;%ANDROID_HOME%\platform-tools
```

For Mac OSX
```
export ANDROID_HOME=/<installation location>/android-sdk-macosx
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```

#### In case you want to make a new git repo

* Initialize the new git (requires you to have git environment installed on the system). Install git from here (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
```
	git init
```

* Setup the new git remotes accordingly
```
	git remote add origin <new remote>
```


#### Plugins List
This is just for info. These plugins are already written in `config.xml` file

    * cordova-plugin-device
    * cordova-plugin-console
    * cordova-plugin-whitelist
    * cordova-plugin-splashscreen
    * cordova-plugin-inappbrowser
    * cordova-plugin-x-toast
    * ionic-plugin-keyboard
    * cordova-plugin-android-permissions
    * cordova-plugin-compat
    * cordova-plugin-dialogs@1.3.1
    * cordova-plugin-file
    * cordova-plugin-file-transfer
    * cordova-plugin-request-location-accuracy
    * cordova-plugin-inapppurchase
    * cordova-plugin-network-information
    * cordova-plugin-statusbar
    * cordova-plugin-facebook4@1.7.4
    * cordova-plugin-inappbrowser
    * cordova-plugin-geolocation
    * cordova-sms-plugin
    * cordova-plugin-contacts
    * parse-push-plugin
    * cordova-plugin-camera
    * cordova-plugin-image-picker
    * cordova.plugins.diagnostic@3.2.1
    
Versions for the plugins can change anytime, as they are all open source and independently maintained.

* Run app on browser (dev mode)

```bash
	ionic serve
```

* Run app on device

```bash
	ionic cordova run android
    ionic cordova run ios --device
```

#### - Release on Google Play

* Create signing key for android to release on Google Play

(This step is only required once, and is already done. The `.keystore` file is provided along with the source code. If you haven't found it yet, ask the developer, along with the `password` and `alias`)


```bash
	keytool -genkey -v -keystore <keystore folder address> -alias <app alias> -keyalg RSA -keysize 2048 -validity 10000
```
-- Make sure to increase the version number (or build number) of the build from the one already existing on Play store. This can be changed in `config.xml` file

* Create release build for Android Play Store

```bash
	ionic cordova build android --release
```

* Sign the 'unsigned' APK for upload on Play store

```bash
	jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore <.keystore file full path> <unsigned apk full path> <app alias>
```


* Zipalign to optimize size for play store upload

```bash
	./zipalign -v 4 <signed apk full path> <path for final APK> 
```

Upload the APK on the existing Play store account in the same app. 

#### - Release on App Store

You need to have an iOS developer account to create `certificates` and `provisioning profiles`. These are required to build the app on your own device, even in `debug` mode. You can run the app in XCode simulator without these.

-- Make sure to increase the version number (or build number) of the build from the one already existing on App store. This can be changed in `config.xml` file

* Create release build for Xcode

```bash
	ionic cordova build ios
```
This will create XCode project in `/platforms/ios` in root folder

* Open the `.xcworkspace` file in Xcode. This opens the project in Xcode. You can build the app to your device, or release one for `adHoc` deployment, or app store deployment from XCode. 
* Few settings to take care of
1. All keys starting with `Privacy` in `info.plist` file should have a relevant string in its value field
2. In Xcode file tree `resources/XCassets` folder, the app should have all icons, splash screens as well as `iTunes store icon`
3. In `build settings -> Family`, the value should be `1,2` for both iPhone and iPad


### 2. Technologies used, versions and related info



1. Front-end - Ionic (1.3.x) based on Angular (1.5.x) (Ionic 1 Framework)
2. Back-end - NodeJS (6.9.5) with MongoDB (Parse Framework V2.3.7) On Back4app.com
3. Chat server - Parse (Parse Live Query Service)
4. Push notification server - Parse push server

Other requirements
1. XCode 9 or above
2. Android SDK, API 25+

### 3. File structure and usage

#### Folder Structure

```
root
     |--- node_modules (auto installed)
     |--- Playoff - Development (cloud code files + web admin panel files)
     |--- platforms (required for build on device)
     |--- plugins (required for build on device)
     |--- resources (icons and splash for device build)
     |--- scss (required if using sass)
     |--- www (source code)
     |      |--- css (styling files)
     |      |--- fonts
     |      |--- img
     |      |--- js (photo editor files)
     |      |--- lib (front-end libraries)
     |      |--- views (all html/view files)
     |      |--- index.html (starting view file)
	 |		|--- app.js
     |		|--- app.routes.js (routing info of views)
     |		|--- app.directives.js (directives)
     |		
     |--- .bowerrc (bower component location file)
     |--- bower.json (bower component details)
     |--- package.json (npm component details)
     |--- config.xml (creates device specific manifest for iOS and Android)
     |--- ionic.config.json (ionic configuration)   

```
     
NOTE: Most of the file names are self-explanatory

#### Some of the important files
* `app.js` - Runs in the very start of the app. Contains the initialization logic of almost all features
* `app.routes.js` - This files defines which page can be accessed using what routing. This also controls the redirection of app on first start, push notification tap start, start from background etc.
* `config.xml` - It controls the configuration of iOS / android app on the device. E.g. what splash/icon to show, what orientation is allowed etc (Manifest.xml in Android / info.plist in iOS).
Should only be changed if version, icon or splash is changed, or if a new plugin is added.
* `bower.json` - contains front-end libraries and their version details.
* `package.json` - contains overall environment libraries and version details. Should not be modified unless you know what you are doing


