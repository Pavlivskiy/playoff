module.exports = function(grunt) {
    
      grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
          options: {
            separator: ';'
          },
          dist: {
            src: [
                'www/app.js',
                'www/app.routes.js',
                'www/app.directives.js',
                'www/views/data/location.factory.js',
                'www/views/data/match.factory.js',
                'www/views/data/parse.service.js',
                'www/views/signup/signup-1/signup-1.controller.js',
                'www/views/signup/eula/eula.controller.js',
                'www/views/signup/signup-2/signup-2.controller.js',
                'www/views/signup/signup-3/signup-3.controller.js',
                'www/views/signup/signup-4/signup-4.controller.js',
                'www/views/admin/admin_settings.controller.js',
                'www/views/setup-profile/setup-1/setup-profile-1.controller.js',
                'www/views/setup-profile/setup-2/setup-profile-2.controller.js',
                'www/views/setup-profile/setup-3/setup-profile-3.controller.js',
                'www/views/setup-profile/setup-35/setup-profile-35.controller.js',
                'www/views/setup-profile/setup-4/setup-profile-4.controller.js',
                'www/views/view-profile/view-profile.controller.js',
                'www/views/settings/settings.controller.js',
                'www/views/playoff/playoff.controller.js',
                'www/views/profile/profile.controller.js',
                'www/views/messages/messages.controller.js',
                'www/views/chat/chat.controller.js',
                
                'www/views/data/in.app.purchase.service.js',
                'www/views/data/auth.factory.js',
                'www/views/data/messages.factory.js',
                'www/views/data/parse.push.factory.js',
                'www/views/data/camera.service.js',
                'www/views/data/services.js',
                'www/views/util/ion-tinder-cards.js',
                'www/views/util/ui.cropper.js',
                'www/views/util/collegeList.js',
                'www/views/directives/ion-multiselect.js',
                'www/views/directives/auto-focus.js',
                'www/views/directives/search-school-directive.js'
            ],
            dest: 'www/dist/<%= pkg.name %>.js'
          }
        },
        uglify: {
          options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
          },
          dist: {
            files: {
              'www/dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
            }
          }
        }
      });
    
      grunt.loadNpmTasks('grunt-contrib-uglify');
      grunt.loadNpmTasks('grunt-contrib-concat');
    
      grunt.registerTask('default', ['concat', 'uglify']);
    
    };