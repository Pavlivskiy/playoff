(function() {
    'use strict';

    angular
        .module('app.directives', [])
        .directive('uiMultiRange', MultiRangeDirective)
        .directive('input', input)

    // All this does is allow the message
    // to be sent when you tap return
    input.$inject = (['$timeout']);
    function input($timeout) {
        return {
            restrict: 'E',
            scope: {
                'returnClose': '=',
                'onReturn': '&',
                'onFocus': '&',
                'onBlur': '&'
            },
            link: function(scope, element, attr) {
                element.bind('focus', function(e) {
                    if (scope.onFocus) {
                        $timeout(function() {
                            scope.onFocus();
                        });
                    }
                });
                element.bind('blur', function(e) {
                    if (scope.onBlur) {
                        $timeout(function() {
                            scope.onBlur();
                        });
                    }
                });
                element.bind('keydown', function(e) {
                    if (e.which == 13) {
                        if (scope.returnClose) element[0].blur();
                        if (scope.onReturn) {
                            $timeout(function() {
                                scope.onReturn();
                            });
                        }
                    }
                });
            }
        };
    }

    MultiRangeDirective.$inject = (['$compile']);
    function MultiRangeDirective($compile) {
        var directive = {
            restrict: 'E',
            scope: {
                ngModelMin: '=',
                ngModelMax: '=',
                ngMin: '=',
                ngMax: '=',
                ngStep: '=',
                ngChangeMin: '&',
                ngChangeMax: '&'
            },
            link: link
        };

        return directive;

        ////////////////////

        function link($scope, $element, $attrs) {
            var min, max, step, $inputMin = angular.element('<input type="range">'),
                $inputMax;
            $scope.ngChangeMin = $scope.ngChangeMin || angular.noop;
            $scope.ngChangeMax = $scope.ngChangeMax || angular.noop;

            if (typeof $scope.ngMin == 'undefined') {
                min = 0;
            } else {
                min = $scope.ngMin;
                $inputMin.attr('min', min);
            }
            if (typeof $scope.ngMax == 'undefined') {
                max = 0;
            } else {
                max = $scope.ngMax;
                $inputMin.attr('max', max);
            }
            if (typeof $scope.ngStep == 'undefined') {
                step = 0;
            } else {
                step = $scope.ngStep;
                $inputMin.attr('step', step);
            }
            $inputMax = $inputMin.clone();
            $inputMin.attr('ng-model', 'ngModelMin');
            $inputMax.attr('ng-model', 'ngModelMax');
            $scope.minColor = '';
            // var $scope.minColor = "{'background-image':'linear-gradient(to right,#ccc 0%, #ccc 7.771929824561404%,#f00 8.771929824561404%,#f00 48.12280701754386%,#ccc 49.12280701754386%,#ccc 100%)'}";
            processColor();
            $inputMin.attr('ng-style', 'minColor');
            $compile($inputMin)($scope);
            $compile($inputMax)($scope);
            $element.append($inputMin).append($inputMax);
            $scope.ngModelMin = $scope.ngModelMin || min;
            $scope.ngModelMax = $scope.ngModelMax || max;

            $scope.$watch('ngModelMin', function(newVal, oldVal) {
                if (newVal > $scope.ngModelMax) {
                    $scope.ngModelMin = oldVal;
                    processColor();
                } else {
                    $scope.ngChangeMin();
                    processColor();
                }
            });

            $scope.$watch('ngModelMax', function(newVal, oldVal) {
                if (newVal < $scope.ngModelMin) {
                    $scope.ngModelMax = oldVal;
                    processColor();
                } else {
                    $scope.ngChangeMax();
                    processColor();
                }
            });

            function processColor() {
                if ($scope.ngModelMin && $scope.ngModelMax) {
                    // console.log($scope.ngModelMin);
                    // console.log($scope.ngModelMax);
                    // $scope.minColor = {'background-image':'linear-gradient(to right,#ccc 0%, #ccc 7.771929824561404%,#f00 8.771929824561404%,#f00 48.12280701754386%,#ccc 49.12280701754386%,#ccc 100%)'};
                    var colorLimit1 = ((100 * ($scope.ngModelMin - 18)) / 57) - 1;
                    var colorLimit2 = ((100 * ($scope.ngModelMin - 18)) / 57);
                    var colorLimit3 = ((100 * ($scope.ngModelMax - 18)) / 57) - 1;
                    var colorLimit4 = ((100 * ($scope.ngModelMax - 18)) / 57);
                    $scope.minColor = {
                        'background-image': 'linear-gradient(to right,#ccc 0%, #ccc ' + colorLimit1 + '%,#74F2A2 ' + colorLimit2 + '%,#74F2A2 ' + colorLimit3 + '%,#ccc ' + colorLimit4 + '%,#ccc 100%)'
                    };
                    // console.log($scope.minColor)
                }
            }
        }
    }
})();