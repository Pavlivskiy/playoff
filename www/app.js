angular.module('app', [
    'ionic',
    'app.directives',
    'app.signup',
    'app.setup',
    'app.routes',
    'app.settings',
    'app.playoff',
    'app.messages',
  
    'app.admin',
    'app.profile',
    'ionTinderCards',
    'ionic-datepicker',
    'angularMoment',
    'ngParse',
    'ngMap',
    'ngCordova',
    'uiCropper',
    'gridster',
    'ion-multiselect-app',
    'angular.filter',
    'LocalForageModule',
    'ionic-cache-src'
])
    //OUR ENV****  use either 'production' or 'development'
    .constant('ENV', 'production')
    // .constant('ENV', 'test')
    

    .config(["$ionicConfigProvider", "$sceDelegateProvider", "ParseProvider", "ENV", "$cordovaFacebookProvider", "ionicDatePickerProvider",function ($ionicConfigProvider, $sceDelegateProvider, ParseProvider, ENV, $cordovaFacebookProvider, ionicDatePickerProvider) {

        $sceDelegateProvider.resourceUrlWhitelist(['self', '*://www.youtube.com/**', '*://player.vimeo.com/video/**']);

        var datePickerObj = {
            setLabel: 'Set',
            closeLabel: 'Close',
            templateType: 'popup',

            showTodayButton: false,

            closeOnSelect: false
        };
        ionicDatePickerProvider.configDatePicker(datePickerObj);

        // var fbTestAppId = "467174560306595"
        var fbTestAppId = "219112612200979"
        
        // var fbLiveAppId = "237170033459835"
        // setTimeout(function() {
        //     var appId;
        //     if (ENV == 'production') {
        //         appId = parseInt(fbLiveAppId, 10);
        //     } else {
        //         appId = parseInt(fbTestAppId, 10);
        //     }
        //     var version = "v2.10"; // or leave blank and default is v2.0
        //     $cordovaFacebookProvider.browserInit(appId, version);
        // }, 2000);
    }])

    .run(["ParsePush", "$ionicPlatform", "ENV", "Parse", "$rootScope", "$state", "ParseAsService", "MessagesFactory",function (ParsePush, $ionicPlatform, ENV, Parse, $rootScope, $state, ParseAsService, MessagesFactory) {
        $ionicPlatform.ready(function () {
            console.log('ionic platform ready')
            console.log('pre attempting keyboard set')

            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                console.log('attempting keyboard set')
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                console.log('done attempting keyboard set2')

                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }


            // If notification permission is provided, register parse push
            document.addEventListener('deviceready', function () {
                // ParsePush.init();
                cordova.plugins.diagnostic.isRemoteNotificationsEnabled(function (enabled) {
                    console.log("Remote notifications are " + (enabled ? "enabled" : "disabled"));
                    if (enabled) {
                        registerPush();
                    }
                    else{
                        setListeners();
                    }
                }, function (error) {
                    console.error("The following error occurred: " + error);
                });
            })


            function registerPush() {
                setListeners();

                // if (window.ParsePushPlugin) {
                //     ParsePushPlugin.register(function (response) {
                //         console.log(response);
                //         console.log('success push register');
                //         setListeners();
                //     }, function (error) {
                //         console.log('fail push register');
                //     })
                // }
            }

            function setListeners() {

            //     if (window.ParsePushPlugin) {
            //         ParsePush.resetBadge();
            //         console.log('parse push init');
            //         ParsePushPlugin.on('receivePN', function (pn) {
            //             console.log('receivePN: ' + JSON.stringify(pn));

            //             //Clear any badge received in open app
            //             ParsePush.resetBadge();

            //             if (pn.payload.type == 'match') {
            //                 $rootScope.$broadcast('push-match', JSON.stringify(pn));
            //             } else if (pn.payload.type == 'chat') {
            //                 // Update chat list in previous page

            //                 var data = {
            //                     'recipientId': pn.payload.sender.objectId,
            //                     'lastMessageText': pn.payload.message,
            //                     'messageDateX': pn.payload.time,
            //                     'unseen': true
            //                 };
            //                 // Update chat list in previous page
            //                 MessagesFactory.messageReceived(data);

            //                 // if ($state.current.name != "chat") {
            //                 //     $state.go('chat', {
            //                 //         matchData: {
            //                 //             id: pn.payload.sender.objectId,
            //                 //             firstName: '',
            //                 //             publicPhotoLink0: ''
            //                 //         }
            //                 //     })
            //                 // }
            //             }
            //         });

            //         ParsePushPlugin.on('openPN', function (pn) {
            //             console.log('openPN:' + JSON.stringify(pn));

            //             //Clear any badge received in open app
            //             ParsePush.resetBadge();

            //             if (pn.payload.type == 'match') {
            //                 $rootScope.$broadcast('push-match', JSON.stringify(pn));
            //             } else if (pn.payload.type == 'chat') {
            //                 // Update chat list in previous page

            //                 var data = {
            //                     'recipientId': pn.payload.sender.objectId,
            //                     'lastMessageText': pn.payload.message,
            //                     'messageDateX': pn.payload.time,
            //                     'unseen': true
            //                 };
            //                 // Update chat list in previous page
            //                 MessagesFactory.messageReceived(data);

            //                 // if ($state.current.name != "chat") {
            //                 //     $state.go('chat', {
            //                 //         matchData: {
            //                 //             id: pn.payload.sender.objectId,
            //                 //             firstName: '',
            //                 //             publicPhotoLink0: ''
            //                 //         }
            //                 //     })
            //                 // }
            //             }
            //         });
            //     } else {
            //         if (!window.cordova) {
            //             console.log('parse push not init');
            //         } else {
            //             alert('parse push not init');
            //         }
            //     }
            // }

            // Check for device type for plugin configuration
            if (window.cordova) {
                if (device.platform == "Android") {
                    $rootScope.android = true;
                };
                if (device.platform == "iOS") {
                    window.FirebasePlugin.grantPermission();
                    $rootScope.ios = true;
                }
            }


            // //temp
            // var user = new Parse.User();
            // user.set("username", "testuseramanda");
            // user.set("password", "testtest1");

            // other fields can be set just like with Parse.Object

            // user.signUp(null, {
            //     success: function(user) {
            //         // Hooray! Let them use the app now.
            //         console.log('success singyup')
            //     },
            //     error: function(user, error) {
            //         // Show the error message somewhere and let the user try again.
            //         // alert("Error: " + error.code + " " + error.message);
            //     }
            // });


            function loadParse() {

                //init parse
                if (ENV == 'production') {
                    Parse.FacebookUtils.init({
                        appId: fbLiveAppId,
                        version: 'v2.10', //Currently available versions https://developers.facebook.com/docs/apps/changelog
                        xfbml: false
                    });
                    // console.log('Parse Facebookutils inited')

                } else {

                    Parse.FacebookUtils.init({
                        appId: fbTestAppId,
                        version: 'v2.10', //Currently available versions https://developers.facebook.com/docs/apps/changelog
                        xfbml: false
                    });
                    // console.log('Parse Facebookutils inited')
                }

            }

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)


            // var user = new Parse.User();
            // user.set("username", "my name");
            // user.set("password", "my pass");
            // user.set("email", "email@example.com");

            // // other fields can be set just like with Parse.Object
            // user.set("phone", "415-392-0202");


            // user.signUp(null, {
            //     success: function(user) {
            //         // Hooray! Let them use the app now.
            //     },
            //     error: function(user, error) {
            //         // Show the error message somewhere and let the user try again.
            //         alert("Error: " + error.code + " " + error.message);
            //     }
            // });

            // //manage push notifications
            // try {
            //     var push = PushNotification.init({
            //         android: {},
            //         // browser: {
            //         //     pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            //         // },
            //         ios: {
            //             alert: "true",
            //             badge: true,
            //             sound: 'false'
            //         },
            //         windows: {}
            //     });
            // } catch (error) {
            //     console.error('Error: unable to load push notifs:', error)
            // }

            // if (window.ParsePushPlugin) {


            //     ParsePushPlugin.on('receivePN', function(pn) {
            //         alert('yo i got this push notification:' + JSON.stringify(pn));
            //     });

            //     //
            //     //you can also listen to your own custom events
            //     // Note: to push custom event, include 'event' key in your push payload,
            //     // e.g. {alert: "sup", event:'chat'}
            //     ParsePushPlugin.on('receivePN:chat', function() {
            //         console.log('received chat event')
            //     });

            //     //
            //     // When the app is off or in background, push notifications get added
            //     // to the notification tray. When a user open a notification, you
            //     // can catch it via openPN
            //     ParsePushPlugin.on('openPN', function(pn) {
            //         alert('a notification was opened:' + JSON.stringify(pn));
            //     });
            }
        });
    }])
    // apple id; 1287953352
    .constant('StoreProductId', 'playoffpremium799') //For Android playoffpremium799 ..., For iOS : playoffPremium799
    .constant('StoreProductId2', 'playoffpremiumnew099') //For Android playoffpremium099 ..., For iOS : playoffPremium099
    .constant('appVersionNumber', 101017)
    .constant('messagesLimit', 10)
    .filter('reverse', function () {
        return function (items) {
            return items.slice().reverse();
        };
    })
    .directive('focusOn', function () {
        return function (scope, elem, attr) {
            scope.$on('focusOn', function (e, name) {
                if (name === attr.focusOn) {
                    elem[0].focus();
                }
            });
        };
    })

    .factory('focus', ["$rootScope", "$timeout", function ($rootScope, $timeout) {
        return function (name) {
            $timeout(function () {
                $rootScope.$broadcast('focusOn', name);
            });
        }
    }]);