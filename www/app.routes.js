(function () {
    'use strict';

    angular
        .module('app.routes', [])
        .config(routes);

    routes.$inject = (['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider']);

    function routes($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

        $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-left').previousTitleText(false);
        $ionicConfigProvider.views.swipeBackEnabled(false);
        // $ionicConfigProvider.views.maxCache(0);

        $stateProvider

            /*-----------------------------SIGN UP ROUTES------------------------------------*/



            .state('signup-1', {
                url: '/signup-1',
                templateUrl: 'views/signup/signup-1/signup-1.html',
                controller: 'Signup1Ctrl as vm'
            })

            .state('eula', {
                url: '/eula',
                templateUrl: 'views/signup/eula/eula.html',
                controller: 'EulaCtrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return false;
                    }
                }
            })
            .state('eula-from-edit', {
                url: '/eula',
                templateUrl: 'views/signup/eula/eula.html',
                controller: 'EulaCtrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return true;
                    }
                }
            })

            .state('signup-2', {
                url: '/signup-2',
                templateUrl: 'views/signup/signup-2/signup-2.html',
                controller: 'Signup2Ctrl as vm'
            })

            .state('signup-3', {
                url: '/signup-3',
                templateUrl: 'views/signup/signup-3/signup-3.html',
                controller: 'Signup3Ctrl as vm'
            })


            .state('signup-4', {
                url: '/signup-4',
                templateUrl: 'views/signup/signup-4/signup-4.html',
                controller: 'Signup4Ctrl as vm'
            })

            /*-------------------------------------------------------------------------------*/


            /*-----------------------------SET UP PROFILE ROUTES------------------------------------*/

            .state('setup-1', {
                url: '/setup-1',
                templateUrl: 'views/setup-profile/setup-1/setup-profile-1.html',
                controller: 'SetupProfile1Ctrl as vm',
                resolve: {
                    parseUser: function () {
                        return {}
                    },
                    parseUserAttributes: function (AuthFactory) {
                        return {}
                    },
                    isFromEditScreen: function () {
                        return false;
                    }
                }
            })

            .state('setup-2', {
                url: '/setup-2',
                templateUrl: 'views/setup-profile/setup-2/setup-profile-2.html',
                controller: 'SetupProfile2Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return false;
                    }
                }
            })
            .state('setup-2-from-edit', {
                url: '/setup-2',
                templateUrl: 'views/setup-profile/setup-2/setup-profile-2.html',
                controller: 'SetupProfile2Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return true;
                    }
                }
            })

            .state('setup-3', {
                url: '/setup-3',
                templateUrl: 'views/setup-profile/setup-3/setup-profile-3.html',
                controller: 'SetupProfile3Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return false;
                    },
                    profileDescription: function (AuthFactory) {
                        return null;
                    }
                }
            })
            .state('setup-35', {
                url: '/setup-35',
                templateUrl: 'views/setup-profile/setup-35/setup-profile-35.html',
                controller: 'SetupProfile35Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return false;
                    },
                    profileDescription: function (AuthFactory) {
                        return null;
                    }
                }
            })
            .state('setup-35-from-edit', {
                url: '/setup-35',
                templateUrl: 'views/setup-profile/setup-35/setup-profile-35.html',
                controller: 'SetupProfile35Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return true;
                    },
                    profileDescription: function (AuthFactory) {
                        return null;
                    }
                }
            })

            .state('setup-4', {
                url: '/setup-4',
                templateUrl: 'views/setup-profile/setup-4/setup-profile-4.html',
                controller: 'SetupProfile4Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return false;
                    },
                    profileDescription: function () {
                        return null;
                    }
                }
            })


            /*-------------------------------------------------------------------------------*/

            .state('playoff', {
                url: '/playoff/:action',
                cache: false,
                templateUrl: 'views/playoff/playoff.html',
                controller: 'PlayoffCtrl as vm'
            })

            .state('settings', {
                url: '/settings',
                templateUrl: 'views/settings/settings.html',
                controller: 'SettingsCtrl as vm'
            })
            .state('admin_settings', {
                url: '/admin_settings',
                templateUrl: 'views/admin/admin_settings.html',
                controller: 'adminSettingsCtrl as vm',
                cache: false
            })

            //same as profile, except we note that the user is currently logged in
            .state('profile-edit', {
                url: '/profile-edit',
                templateUrl: 'views/setup-profile/setup-1/setup-profile-1.html',
                controller: 'SetupProfile1Ctrl as vm',
                resolve: {
                    parseUser: function () {
                        return Parse.User.current()
                    },
                    parseUserAttributes: function (AuthFactory) {
                        return AuthFactory.getAllUserAttributes()
                    },
                    isFromEditScreen: function () {
                        return true;
                    }
                }
            })
            //for dev
            //because resolve functions weren't working otherwise
            .state('profile-edit-dev', {
                url: '/profile-edit-dev',
                templateUrl: 'views/setup-profile/setup-1/setup-profile-1.html',
                controller: 'SetupProfile1Ctrl as vm',
                resolve: {
                    parseUser: function () {
                        return {}
                    },
                    parseUserAttributes: function (AuthFactory) {
                        return {}
                    },
                    isFromEditScreen: function () {
                        return true;
                    }
                }
            })

            .state('description-edit', {
                url: '/description-edit',
                templateUrl: 'views/setup-profile/setup-3/setup-profile-3.html',
                controller: 'SetupProfile3Ctrl as vm',
                resolve: {
                    isFromEditScreen: function () {
                        return true;
                    },
                    profileDescription: function (AuthFactory) {
                        return AuthFactory.getProfileDescription()
                    }
                }
            })


            // .state('view-profile', {
            //     url: '/view-profile',
            //     templateUrl: 'views/view-profile/view-profile.html',
            //     controller: 'ViewProfileCtrl as vm',
            //     params: {
            //         user: null
            //     }
            // })

            .state('messages', {
                url: '/messages',
                templateUrl: 'views/messages/messages.html',
                controller: 'MessagesCtrl as vm'
            })


            .state('chat', {
                url: '/chat',
                templateUrl: 'views/chat/chat.html',
                controller: 'ChatCtrl as vm',
                params: {
                    matchData: null
                }
            })

        $urlRouterProvider.otherwise(function ($injector, $location) {


            var rootScope = $injector.get('$rootScope');
            var ParseAsService = $injector.get('ParseAsService');
            var state = $injector.get('$state');
            var timeout = $injector.get('$timeout');
            var ionicPopup = $injector.get('$ionicPopup');
            var ionicPlatform = $injector.get('$ionicPlatform');
            //var InAppPurchaseService = $injector.get('InAppPurchaseService');

            rootScope.$on('parse-init', onParseInit);
            document.addEventListener("deviceready", onDeviceReady, false);

            function onDeviceReady() {
                //InAppPurchaseService.getProducts();

            }
            function checkPremiumAndroid() {
                var currentUser = Parse.User.current().toJSON();
                console.log(currentUser);
                if (currentUser.premium) {
                    if (currentUser.premiumOn && ((new Date() - new Date(currentUser.premiumOn.iso)) > (30 * 24 * 60 * 60 * 1000))) {
                        console.log("check premium")
                        var query = new Parse.Query("Payments");
                        query.equalTo('user', Parse.User.current());
                        query.descending('createdAt')
                        query.first({
                            success: function (paymentList) {
                                console.log("paymentList", paymentList);
                                if (paymentList) {
                                    var paymentObject = paymentList.toJSON();
                                    var receipt = {
                                        data: paymentObject.receipt,
                                        signature: paymentObject.signature,
                                    }
                                    Parse.Cloud.run('checkInAppPurchaseStatus', {
                                        userId: Parse.User.current().id,
                                        receipt: receipt
                                    }).then(function (data) {
                                        console.log("purchased item data");
                                        console.log(data)
                                        if (!data.length) {
                                            Parse.User.current().set('premium', false)
                                                .save().then(function (result) {
                                                    console.log(result);
                                                }, function (err) {
                                                    console.log(err);
                                                })
                                            var alertPopup = ionicPopup.alert({
                                                title: 'Premium Subscription Expired',
                                                template: "Your Playoff premium subscription has expired. Please renew to enjoy premium access"
                                            });
                                            alertPopup.then(function (res) {
                                            });
                                        } else {
                                            Parse.User.current().set('premium', true)
                                            Parse.User.current().set('premiumOn', new Date(data[0].purchaseTime))
                                                .save().then(function (result) {
                                                    console.log(result);
                                                }, function (err) {
                                                    console.log(err);
                                                })
                                        }
                                    }, function (error) {
                                        console.log(error);
                                    });
                                } else {
                                    Parse.User.current().set('premium', false)
                                        .save().then(function (result) {
                                            console.log(result);
                                        }, function (err) {
                                            console.log(err);
                                        })
                                }

                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                    }
                }

            }
            function checkPremium() {
                var currentUser = Parse.User.current().toJSON();
                console.log(currentUser);
                if (currentUser.premium) {
                    if (currentUser.premiumOn && ((new Date() - new Date(currentUser.premiumOn.iso)) > (30 * 24 * 60 * 60 * 1000))) {
                        console.log("check premium")
                        var query = new Parse.Query("Payments");
                        query.equalTo('user', Parse.User.current());
                        query.descending('createdAt')
                        query.first({
                            success: function (paymentList) {
                                console.log("paymentList", paymentList);
                                if (paymentList) {
                                    var paymentObject = paymentList.toJSON();
                                    Parse.Cloud.run('checkInAppPurchaseStatus', {
                                        userId: Parse.User.current().id,
                                        receipt: paymentObject.receipt
                                    }).then(function (data) {
                                        console.log("purchased item data");
                                        console.log(data)
                                        if (!data.length) {
                                            Parse.User.current().set('premium', false)
                                                .save().then(function (result) {
                                                    console.log(result);
                                                }, function (err) {
                                                    console.log(err);
                                                })
                                            var alertPopup = ionicPopup.alert({
                                                title: 'Premium Subscription Expired',
                                                template: "Your Playoff premium subscription has expired. Please renew to enjoy premium access"
                                            });
                                            alertPopup.then(function (res) {
                                            });
                                        } else {
                                            Parse.User.current().set('premium', true)
                                            Parse.User.current().set('premiumOn', new Date(data[0].purchaseDateMs))
                                                .save().then(function (result) {
                                                    console.log(result);
                                                }, function (err) {
                                                    console.log(err);
                                                })
                                        }
                                    }, function (error) {
                                        console.log(error);
                                    });
                                } else {
                                    Parse.User.current().set('premium', false)
                                        .save().then(function (result) {
                                            console.log(result);
                                        }, function (err) {
                                            console.log(err);
                                        })
                                }

                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                    } else if (!currentUser.premiumOn) {
                        console.log("not premiumon date ==========")
                        inAppPurchase
                            .getReceipt()
                            .then(function (receipt) {
                                console.log("Receipt of purchase is:")
                                if (receipt) {
                                    var Payments = Parse.Object.extend("Payments");
                                    var payment = new Payments();
                                    payment
                                        .set('user', Parse.User.current())
                                        .set('receipt', receipt)
                                        .save().then(function () {
                                            Parse.Cloud.run('checkInAppPurchaseStatus', {
                                                userId: Parse.User.current().id,
                                                receipt: receipt
                                            }).then(function (data) {
                                                console.log("purchased item data");
                                                console.log(data)
                                                if (!data.length) {
                                                    Parse.User.current().set('premium', false)
                                                        .save().then(function (result) {
                                                            console.log(result);
                                                        }, function (err) {
                                                            console.log(err);
                                                        })
                                                    var alertPopup = ionicPopup.alert({
                                                        title: 'Premium Subscription Expired',
                                                        template: "Your Playoff premium subscription has expired. Please renew to enjoy premium access"
                                                    });
                                                    alertPopup.then(function (res) {
                                                    });
                                                } else {
                                                    Parse.User.current().set('premium', true)
                                                    Parse.User.current().set('premiumOn', new Date(data[0].purchaseDateMs))
                                                        .save().then(function (result) {
                                                            console.log(result);
                                                        }, function (err) {
                                                            console.log(err);
                                                        })
                                                }
                                            }, function (error) {
                                                console.log(error);
                                            });
                                        }, function (err) {
                                            console.log(err);
                                        })
                                }
                            })
                            .catch(function (err) {
                                console.log('error in get receipt');
                                console.log(err);
                            });

                    }
                }

            }

            function onParseInit() {
                var isOnline;
                console.log('called parse init in route');
                document.addEventListener("deviceready", onDeviceReadyFun, false);
                function onDeviceReadyFun() {
                    //InAppPurchaseService.getProducts();
                    console.log('called onDeviceReadyFun');
                    var network = $injector.get('$cordovaNetwork');
                    var type = network.getNetwork();
                    console.log('network info');
                    isOnline = network.isOnline();
                    console.log(isOnline);
                    if (isOnline) {
                        // console.log('ionic ready');
                        var state = $injector.get('$state');
                        var ionicLoading = $injector.get('$ionicLoading');
                        if (Parse.User.current()) {

                            var currentUser = Parse.User.current().toJSON();
                            if (ionicPlatform.is('ios')) {
                                checkPremium();
                            } else if(ionicPlatform.is('android')) {
                                checkPremiumAndroid();
                            }
                            var profileImageCount = 0;
                            var setupOneFlag = angular.isDefined(currentUser.birthdayMMDDYYYY) && angular.isDefined(currentUser.firstName) && angular.isDefined(currentUser.graduationYear) && angular.isDefined(currentUser.heightFeet) && angular.isDefined(currentUser.heightInches) && angular.isDefined(currentUser.primarySport) && angular.isDefined(currentUser.school);
                            var setupThreeFlag = angular.isDefined(currentUser.locationGeoPoint) && angular.isDefined(currentUser.locationGeoPoint.latitude) && angular.isDefined(currentUser.locationGeoPoint.longitude);
                            var setupBioFlag = angular.isDefined(currentUser.bioLink) && angular.isDefined(currentUser.jerseyImage);
                            angular.forEach(currentUser, function (value, key) {
                                if (key.indexOf('publicPhotoLink') > -1 && value) {
                                    profileImageCount++;
                                }
                            });
                            console.log("profileImageCount", profileImageCount)
                            // var setupTwoFlag = 
                            if (profileImageCount >= 3 && setupOneFlag && setupThreeFlag && setupBioFlag) {
                                state.go('playoff');
                                console.log("currentUser", currentUser)
                            } else if (profileImageCount >= 3 && setupOneFlag && setupBioFlag) {
                                state.go('setup-4');
                                console.log("currentUser", currentUser)
                            } else if (setupOneFlag && setupBioFlag) {
                                state.go('setup-2');
                            } else if (setupBioFlag) {
                                state.go('setup-1');
                            } else {
                                state.go('signup-4');
                            }
                        } else {
                            state.go('signup-1');
                        }

                        return $location.path();
                        // })
                    } else {
                        var alertPopup = ionicPopup.alert({
                            title: 'Playoff',
                            template: 'Internet connection is not available.'
                        });

                        alertPopup.then(function (res) {
                            console.log('network connection failed');
                        });
                    }
                }
                if (!window.cordova) {
                    var state = $injector.get('$state');
                    var ionicLoading = $injector.get('$ionicLoading');
                    if (Parse.User.current()) {
                        var currentUser = Parse.User.current().toJSON();
                        var profileImageCount = 0;
                        if (ionicPlatform.is('ios')) {
                            checkPremium();
                        } else if(ionicPlatform.is('android')) {
                            checkPremiumAndroid();
                        }
                        var setupOneFlag = angular.isDefined(currentUser.birthdayMMDDYYYY) && angular.isDefined(currentUser.firstName) && angular.isDefined(currentUser.graduationYear) && angular.isDefined(currentUser.heightFeet) && angular.isDefined(currentUser.heightInches) && angular.isDefined(currentUser.primarySport) && angular.isDefined(currentUser.school);
                        var setupThreeFlag = angular.isDefined(currentUser.locationGeoPoint) && angular.isDefined(currentUser.locationGeoPoint.latitude) && angular.isDefined(currentUser.locationGeoPoint.longitude);
                        var setupBioFlag = angular.isDefined(currentUser.bioLink) && angular.isDefined(currentUser.jerseyImage);
                        angular.forEach(currentUser, function (value, key) {
                            if (key.indexOf('publicPhotoLink') > -1 && value) {
                                profileImageCount++;
                            }
                        });
                        console.log("profileImageCount", profileImageCount)
                        // var setupTwoFlag = 
                        if (profileImageCount >= 3 && setupOneFlag && setupThreeFlag && setupBioFlag) {
                            state.go('playoff');
                            console.log("currentUser", currentUser)
                        } else if (profileImageCount >= 3 && setupOneFlag && setupBioFlag) {
                            state.go('setup-4');
                            console.log("currentUser", currentUser)
                        } else if (setupOneFlag && setupBioFlag) {
                            state.go('setup-2');
                        } else if (setupBioFlag) {
                            state.go('setup-1');
                        } else {
                            state.go('signup-4');
                        }
                    } else {
                        state.go('signup-1');
                    }

                    return $location.path();
                }

            }
        });

    }

})();