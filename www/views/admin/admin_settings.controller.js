(function() {
    'use strict';

    angular
        .module('app.admin', [])
        .controller('adminSettingsCtrl', adminSettingsCtrl);

    adminSettingsCtrl.$inject = ['$ionicLoading'];

    function adminSettingsCtrl($ionicLoading) {

        var vm = this;
        vm.closeSettings = closeSettings;
        vm.refreshCounts = refreshCounts;
        vm.settingsData = {};

        activate()

        function activate() {
            $ionicLoading.show();
            Parse.Cloud.run('getAdminSettings', {
                userId: Parse.User.current().id
            }).then(function(data){
                console.log(data);
                vm.settingsData = data;
                $ionicLoading.hide();
            }, function(error){
                console.log(error);
                $ionicLoading.hide();
            });
        
        }

        function closeSettings() {

        }

        function refreshCounts() {
            activate();
        }

    }
})();