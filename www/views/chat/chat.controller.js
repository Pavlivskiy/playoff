(function () {
    'use strict';

    angular
        .module('app.messages')
        .controller('ChatCtrl', ChatCtrl);

    ChatCtrl.$inject = ['$localForage', '$cordovaToast', 'ENV', '$ionicLoading', 'chatList', 'cameFromSearchPage' ,'$scope', '$timeout', '$ionicScrollDelegate', 'MessagesFactory', '$stateParams', '$state', '$ionicModal', 'MatchFactory', '$ionicPopup', '$rootScope', 'messagesLimit', 'AuthFactory'];

    function ChatCtrl($localForage, $cordovaToast, ENV, $ionicLoading, chatList, cameFromSearchPage, $scope, $timeout, $ionicScrollDelegate, MessagesFactory, $stateParams, $state, $ionicModal, MatchFactory, $ionicPopup, $rootScope, messagesLimit, AuthFactory) {
        var vm = this;
        var anyMessageAppear = false;
        vm.openProfile = openProfile;
        vm.showActions = showActions;
        vm.showChatTime = showChatTime;vm.messages
        vm.matchData = $state.params.matchData;
        var chatMessagesLength;
        var markAsLatest = false;
        $scope.hideTime = null;
        vm.showChatInput = true;
        vm.slClass = false;
        $scope.warn = function (message) {
            alert(message);
          };
        // var user = Parse.User.current()

        $scope.data = {};
        $scope.myId = '12345';
        vm.messages = [];
        vm.skip = 0;

        var alternate,
            isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();
        // Resume refresh
        $rootScope.$on('onResumeCordova', function (event) {
            init();
        });

        // System events
        document.addEventListener("resume", resume, false);

        function resume() {
            var div = document.getElementsByTagName('body')[0];
            var scope = angular.element(div).scope();
            var rootScope = scope.$root;
            rootScope.$apply(function () {
                rootScope.$broadcast('onResumeCordova');
            });
        }
        AuthFactory.subscribe(function (data) {
            console.log('New message has beed received');
            if (vm.messages.length == 0 || vm.messages[0].sender == data.sender || vm.messages[0].recipient == data.sender) {
                var newMessage = {}
                newMessage.sender = data.sender;
                newMessage.recipient = data.recipient;
                newMessage.time = new Date(data.createdAt).getTime();
                newMessage.text = data.messageText;
                newMessage.date = new Date(newMessage.time).getDate() + ' ' + new Date(newMessage.time).getMonth() + ' ' + new Date(newMessage.time).getFullYear();
                vm.messages.push(newMessage);
                anyMessageAppear = true;
                $timeout(function () {
                    $ionicScrollDelegate.scrollBottom(true);
                }, 100);
            }
            else{
                cordova.plugins.notification.local.schedule({
                    title: data.title,
                    text: data.messageText,
                    icon: 'https://firebasestorage.googleapis.com/v0/b/playoff-dating-app.appspot.com/o/additional_files%2Fpush_icon.png?alt=media&token=e29a54fa-e4d5-45c3-8065-0b2b86c7f8ef',
                });
            }
        });
        //globally defining query and live query to be able to close it in 'beforeleave' event
        var client, query1, query2, query3, subscription;

        function showActions(index) {
            vm.slClass = index;
        }

        function showChatTime(index) {
            if (!vm.chatTime && vm.chatTime != 0) {
                vm.chatTime = index;
            }
            else {
                vm.chatTime = null;
            }
        }

        function init() {
            window.FirebasePlugin.setBadgeNumber(0);
            cameFromSearchPage.isTrue = false;
            AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                var user = {};
                user.id = currentUser.get("localId");
                vm.messages = [];
                $ionicLoading.show({
                    template: '<ion-spinner icon="ios"></ion-spinner>'
                });

                console.log(vm.matchData);

                MessagesFactory.getMessagesBetweenUsers(vm.matchData.id)
                    .then(
                        function (results) {
                            chatMessagesLength = results.length;
                            $timeout(function () {
                                console.log(results);
                                _.each(results, function (item) {
                                    // console.log(item);
                                    item.date = new Date(item.time).getDate() + ' ' + new Date(item.time).getMonth() + ' ' + new Date(item.time).getFullYear();
                                    if (!checkIfMessageExistAlready(item,vm.messages)) {
                                        vm.messages.push(item);
                                    }
                                    else{
                                        console.log('skipp message as far as exist already: ' + item.text)
                                    }
                                    $timeout(function () {
                                        $ionicScrollDelegate.scrollBottom(true);
                                    }, 100);
                                })
                                $ionicLoading.hide();
                                // console.log(results);
                            }, 0);
                        })
                    .catch(function (error) {
                        console.error(error);
                        $ionicLoading.hide();
                    })


                vm.myId = user.id;

                // client.on('open', () => {
                //     console.log('connection opened');
                // subscription.on('create', (message) => {
                //     console.log(message);
                //     var newMessage = {}
                //     newMessage.sender = message.get('sender').id;
                //     newMessage.recipient = message.get('recipient').id;
                //     newMessage.time = new Date(message.get('createdAt')).getTime();
                //     newMessage.text = message.get('messageText');
                //     newMessage.date = new Date(newMessage.time).getDate() + ' ' + new Date(newMessage.time).getMonth() + ' ' + new Date(newMessage.time).getFullYear();

                //     // If the message is from the other user of chat
                //     // if (newMessage.sender == vm.matchData.id) {
                //     vm.messages.unshift(newMessage);

                //     var data = {
                //         'recipientId': newMessage.sender,
                //         'lastMessageText': newMessage.text,
                //         'messageDateX': newMessage.time
                //     };
                //     // Update chat list in previous page
                //     MessagesFactory.messageReceived(data);
                //     // }
                //     $scope.safeApply();
                //     $ionicScrollDelegate.scrollBottom(true);
                // });
             



                MatchFactory.getUserProfileInfo(vm.matchData.id, user.id)
                    .then(
                        function (result) {
                            $scope.modalCard = result;
                            console.log(result);
                            if (result.deleteProfile || vm.matchData.matchDate < result.reActivateProfileDate) {
                                //$timeout(function(){
                                vm.showChatInput = false;
                                //},10);
                            }
                            vm.matchData.deleteProfile = result.deleteProfile;
                            vm.matchDate =
                                $scope.modalCard.name = result.firstName;
                            $scope.modalCard.description = result.school;
                            $scope.modalCard.image = result.publicPhotoLink0;
                            $scope.modalCard.primarySport = result.primarySport;
                            $scope.modalCard.distanceFrom = result.distanceFrom;
                            $scope.modalCard.age = result.age;
                            $scope.modalCard.reportUser = reportUser
                            $scope.modalCard.unmatchUser = unmatchUser
                            $scope.modalCard.userId = vm.matchData.id;
                            // $scope.modalCard.allPhotos = [result.publicPhotoLink0,result.publicPhotoLink1,result.publicPhotoLink2,result.publicPhotoLink3,result.publicPhotoLink4]
                            $scope.modalCard.allPhotos = [];
                            $scope.modalCard.allPhotos[0] = $scope.modalCard.image;

                            // var user = $scope.modalCard.user.toJSON();

                            if (result.publicPhotoLink1) {
                                $scope.modalCard.allPhotos.push(result.publicPhotoLink1)
                            }
                            if (result.publicPhotoLink2) {
                                $scope.modalCard.allPhotos.push(result.publicPhotoLink2)
                            }
                            if (result.publicPhotoLink3) {
                                $scope.modalCard.allPhotos.push(result.publicPhotoLink3)
                            }
                            if (result.publicPhotoLink4) {
                                $scope.modalCard.allPhotos.push(result.publicPhotoLink4)
                            }
                            $scope.modalCard.settingsClick = false;

                            $scope.modalCard.messages = vm.messages;
                        })
                    .catch(function (error) {
                        console.error(error);
                    })


            })
        }

        $scope.safeApply = function (fn) {

            this.$apply(fn);
        };

        $scope.sendMessage = function () {
            $rootScope.sendButtonClicked = true;
            alternate = !alternate;
            markAsLatest = true;

            if (!vm.currentMessage || vm.currentMessage == "") {
                return;
            }

            // Add new message to chat messages in the view
            AuthFactory.getCurrentUserDbEntry().then(function (user) {
                var newMessage = {};
                newMessage.sender = user.get("localId");
                
                newMessage.recipient = vm.matchData.id;
                newMessage.time = new Date().getTime();
                console.log(newMessage.time);
                newMessage.text = vm.currentMessage;

                var date = new Date(newMessage.time);
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();

                newMessage.date = day + ' ' + monthIndex + ' ' + year;
                // vm.messages.unshift(newMessage);
                vm.messages.push(newMessage);

                $ionicScrollDelegate.scrollBottom(true);
                MessagesFactory.sendMessage(vm.matchData.id, vm.currentMessage, vm.matchData.key).then((data)=>{
                    console.log(data);
                    var recipientId = newMessage.recipient;
                    var matchObject = _.findWhere(chatList.data, {
                        id: recipientId
                    });
                    if (matchObject) {
                        matchObject.lastMessageText = newMessage.text;
                        matchObject.lastMessageDateX = data.messageDateX;
                        matchObject.unseen = false;
                    }
                    $localForage.setItem('chatList', chatList.data).then(function () {
                        console.log('chatlist updated in local')
                    })
                });
                anyMessageAppear = true;

                // $ionicScrollDelegate.scrollBottom(true);
                // This is to keep the keyboard up after click Send button and bring chat up
                $timeout(function () {
                    $rootScope.sendButtonClicked = false;
                }, 30);

                delete vm.currentMessage;
            });
        };


        $scope.inputUp = function () {
            // if (isIOS) $scope.data.keyboardHeight = 216;
            if (isIOS) {
                console.log("iOS version: " +  ionic.Platform.version());
                var viewScroll = $ionicScrollDelegate.$getByHandle('scrollClass');
                var scroller = document.body.querySelector('#chatWrapper');
    
                window.addEventListener('native.keyboardshow', keyboardShowHandler);
                function keyboardShowHandler(e) {
                    scroller.style.bottom = e.keyboardHeight + 'px';
                    viewScroll.scrollBottom();
                }
            }
            else{
                // $timeout(function () {
                //     $ionicScrollDelegate.scrollBottom(true);
                // }, 300);
                var viewScroll = $ionicScrollDelegate.$getByHandle('scrollClass');
                var scroller = document.body.querySelector('#chatWrapper');
    
                window.addEventListener('native.keyboardshow', keyboardShowHandler);
                function keyboardShowHandler(e) {
                    scroller.style.bottom = e.keyboardHeight + 'px';
                    $timeout(function () {
                        viewScroll.scrollBottom();
                    }, 300);
                    
                }
            }
        };

        $scope.inputDown = function () {
            // if (isIOS) $scope.data.keyboardHeight = 0;
            $rootScope.sendButtonClicked = false;
            $ionicScrollDelegate.resize();
        };

        $scope.closeKeyboard = function () {
            // cordova.plugins.Keyboard.close();
        };


        function openProfile() {
            $scope.sliderOptions = {
                loop: false,
                effect: 'slide',
                speed: 500
            }

            //to make sure we have messages in case of a report
            //conditional if here bc we use ChatCtrl elsewhere and may not have vm.messages
            if (vm.messages) {
                if (!$scope.modalCard) {
                    $scope.modalCard = {}
                }
                $scope.modalCard.messages = vm.messages;
            }

            $scope.showUnmatchButton = true;
            vm.userImage = 4 * window.innerWidth / 3;
            $ionicModal.fromTemplateUrl('./views/view-profile/view-profile.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
            // $scope.openModal = function() {
            // };
            $scope.closeModal = function () {
                $scope.modal.hide();
            };
            // Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function () {
                $scope.modal.remove();
            });
            // Execute action on hide modal
            $scope.$on('modal.hidden', function () {
                // Execute action
            });
            // Execute action on remove modal
            $scope.$on('modal.removed', function () {
                // Execute action
            });



            $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                // data.slider is the instance of Swiper
                $scope.slider = data.slider;
            });

            $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                // console.log('Slide change is beginning');
            });

            $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                // note: the indexes are 0-based
                $scope.activeIndex = data.slider.activeIndex;
                $scope.previousIndex = data.slider.previousIndex;
            });

            // return $state.go('view-profile', {
            //     user: {
            //         id: card.user.id,
            //         firstName: card.name,
            //         publicPhotoLink0: card.image
            //     }
            // })
        }

        /**
         * 
         * @param {string} userIdToUnmatch
         */
        function unmatchUser(userIdToUnmatch) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Unmatch',
                template: 'Are you sure you want to unmatch this user?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    return MatchFactory.unmatchUser(userIdToUnmatch)
                        .then(
                        function (result) {
                            $rootScope.$emit('refreshMessagesList')

                            return $state.go('messages')
                        })
                        .catch(function (error) {
                            console.error(error);
                        })
                }
            });
        }

        /**
         * 
         * @param {string} userIdToReport
         * @param {array} messages comes from vm.messages actually indirectly through the scope
         * NOTE: This also unmatches the users
         */
        function reportUser(userIdToReport, messages) {

            var confirmPopup = $ionicPopup.confirm({
                title: 'Report',
                template: 'Are you sure you want to report this user? This will unmatch you.'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    return MatchFactory.reportAndUnmatchUser(userIdToReport, messages)
                        .then(
                        function (result) {
                            $rootScope.$emit('refreshMessagesList')
                            return $state.go('messages')
                        })
                        .catch(function (error) {
                            console.error(error);
                        })
                }
            });
        }

        $scope.$on('$ionicView.beforeEnter', function () {
            if (window.cordova) {
                console.log( cordova.plugins.Keyboard);
                //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            }
        });

        $scope.$on('$ionicView.afterEnter', function () {
            openClient();
        });

        function openClient() {
            // // Defining livequery in Parse
            // var LiveQueryClient = Parse.LiveQueryClient;
            // //Creating socket from Advanced live query
            // if (ENV == 'production') {
            //     client = new LiveQueryClient({
            //         applicationId: 'eSzbKSy1sf14JD8gkeuTmZyrRl4UROfgNz0O4L9U',//production
            //         serverURL: 'ws://Playoff.back4app.io',
            //         javascriptKey: 'DVBjdRNHg4LI8SxK0ybg7KVqsqNa10eRRIhSuxCi'
            //     });
            // }
            // else if (ENV == 'development') {
            //     client = new LiveQueryClient({
            //         applicationId: 'ent5YIa1kuvkJlQKI3scJyNjfgEW0VFtKGP2TrTr', //development
            //         serverURL: 'ws://playoffdev.back4app.io',
            //         javascriptKey: 'yjdAU5UXEUjyiJN1wpDxCugTq08PiFLEHOt8AjSa'
            //     });
            // }

            // // Open socket
            // client.open();

            // query1 = new Parse.Query('Messages')
            // query1.equalTo('senderId', user.id)
            // query1.equalTo('recipientId', vm.matchData.id)

            // query2 = new Parse.Query('Messages')
            // query2.equalTo('senderId', vm.matchData.id)
            // query2.equalTo('recipientId', user.id)

            // query3 = Parse.Query.or(query1, query2);

            // subscription = client.subscribe(query3);

            init();
        }

        $scope.$on('$ionicView.beforeLeave', function () {
            console.log('before leaving');
            if (anyMessageAppear) {
                // update chatlist array in previous page 
                var thisChatIndex = _.findIndex(chatList.data, function (chat) {
                    return chat.id == vm.matchData.id;
                })
                if (thisChatIndex != -1) {
                    // change the time only when a message is sent or received
                    // if (chatMessagesLength < vm.messages.length) {
                    if (vm.messages) {
                        chatList.data[thisChatIndex].lastMessageDateX = vm.messages[vm.messages.length - 1].time;
                        chatList.data[thisChatIndex].lastMessageText = vm.messages[vm.messages.length - 1].text;
                    }
                    // }
                    // change the unseen status anyway
                    chatList.data[thisChatIndex].unseen = false;

                    $localForage.setItem('chatList', chatList.data).then(function () {
                        console.log('chatlist saved in chat page')
                    })
                }
            }
            else {
                // update chatlist array in previous page 
                var thisChatIndex = _.findIndex(chatList.data, function (chat) {
                    return chat.id == vm.matchData.id;
                })
                if (thisChatIndex != -1) {
                    if (vm.messages) {
                        var index = getTheMostRecentMessageIndex(vm.messages);
                        if (index != null) {
                            chatList.data[thisChatIndex].lastMessageDateX = vm.messages[index].time;
                            chatList.data[thisChatIndex].lastMessageText = vm.messages[index].text;
                        }
                    }
                    chatList.data[thisChatIndex].unseen = false;
                }
            }

            //close the live query subscription and socket
            // subscription.unsubscribe();
            // client.close();

            // re-enable the keyboard 'done' button
            if (window.cordova) {
                //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            }
        });

        $scope.doRefresh = function () {
            vm.skip = vm.skip + messagesLimit;
            console.log('doRefresh getMessagesBetweenUsers');
            MessagesFactory.getMessagesBetweenUsers(vm.matchData.id, vm.skip)
                .then(
                function (results) {
                    chatMessagesLength = results.length;
                    $timeout(function () {
                        // console.log(results);
                        _.each(results, function (item) {
                            // console.log(item);
                            item.date = new Date(item.time).getDate() + ' ' + new Date(item.time).getMonth() + ' ' + new Date(item.time).getFullYear();
                            if (!checkIfMessageExistAlready(item,vm.messages)) {
                                vm.messages.push(item);
                            }else{
                                console.log('skipp message as far as exist already: ' + item.text)
                            }
                        })
                        $scope.$broadcast('scroll.refreshComplete');
                        // console.log(results);
                    }, 0);
                })
                .catch(function (error) {
                    console.error(error);
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };



        document.addEventListener("resume", function () {
            if ($state.current.name === 'chat') {
                if (window.cordova) {
                    $cordovaToast.showShortBottom('Chat reconnected');
                }
                openClient();
            }
        });

    }
    function checkIfMessageExistAlready(obj, list) {
        var i;
        var res = false;
        list.forEach(function (item, i, arr) {
            if (item.text == obj.text) {
                if (item.recipient == obj.recipient) {
                    if (item.time == obj.time) {
                        if (item.sender == obj.sender) {
                            res = true;
                        }
                    }
                }
            }
        });
        return res;
    }
    function getTheMostRecentMessageIndex(messages){
        var result = null;
        var maxTime = 0;
        _.each(messages, function (current, index) {
            if(current.time >= maxTime){
                maxTime = current.time;
                result = index;
            }
        });
        return result;
    }
})();