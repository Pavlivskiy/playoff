(function () {
    'use strict';

    angular
    .module('app.settings', ['firebase'])
    .factory('AuthFactory', AuthFactory)
    AuthFactory.$inject = ['$q', '$timeout', 'Parse', 'moment', 'ENV', '$cordovaFacebook', '$ionicLoading', '$state', '$firebase', '$firebaseArray', '$localForage'];

    function AuthFactory($q, $timeout, Parse, moment, ENV, $cordovaFacebook, $ionicLoading, $state, $firebase, $firebaseArray, $localForage) {
        var vm = this;
        var authObject = {};
        var callbacks = [];
        var user;
        var service = {
            deletePicture: deletePicture,
            loginUserFacebook: loginUserFacebook,
            getFacebookFirstName: getFacebookFirstName,
            getBirthdayMoment: getBirthdayMoment,
            getFacebookMostRecentCompanyAndJobTitle: getFacebookMostRecentCompanyAndJobTitle,
            getFacebookMostRecentEducation: getFacebookMostRecentEducation,
            getFacebookProfilePicture: getFacebookProfilePicture,
            getFacebookLastFourProfilePictures: getFacebookLastFourProfilePictures,
            saveProfileDescription: saveProfileDescription,
            getProfileDescription: getProfileDescription,
            logOut: logOut,
            logOutAndReturnToWelcomeScreen: logOutAndReturnToWelcomeScreen,
            saveLocationString: saveLocationString,

            //setup1
            saveProfilePage1: saveProfilePage1,
            saveProfilePage2Images: saveProfilePage2Images,
            saveProfilePage4: saveProfilePage4,
            saveProfileQuestions: saveProfileQuestions,

            //fb access token
            getFBAccessToken: getFBAccessToken,

            //parse
            getCurrentProfilePictureArray: getCurrentProfilePictureArray,
            getMainProfilePictureLink: getMainProfilePictureLink,
            getAllUserAttributes: getAllUserAttributes,
            getFacebookFriends: getFacebookFriends,
            deleteProfile: deleteProfile,
            reActivateProfile: reActivateProfile,

            //firebase
            getCurrentUserDbEntry: getCurrentUserDbEntry ,
            subscribe:subscribe
        };

        // var fbPermissionsString = "email,public_profile,user_birthday,user_work_history,user_location,user_friends,user_education_history,user_photos";
        var fbPermissionsString = "email";
        
        init()

        return service;

        ////////////////

        function init() {
            var config = {
                apiKey: "AIzaSyCsLE0n3ZVQUAogGYXqyXEO33FQbiFr4Qk",
                authDomain: "playoff-dating-app.firebaseapp.com",
                databaseURL: "https://playoff-dating-app.firebaseio.com",
                storageBucket: "playoff-dating-app.appspot.com",
                projectId: "playoff-dating-app"
            };
            firebase.initializeApp(config);
        }
        function subscribe(callback) {
            callbacks.push(callback);
            window.FirebasePlugin.onNotificationOpen(function (data) {
                console.log(JSON.stringify(data))
                callbacks.forEach(function(el){
                    el(data);
                });
                // callback(data);
            }, function (error) {
                console.error(error);
            });
            window.FirebasePlugin.onTokenRefresh(function (token) {
                // save this server-side and use it to push notifications to this device
                console.log("Refreshed token: " + token);
            }, function (error) {
                console.error(error);
            });
        }

        /**
         * @return {Promise<boolean>} boolean is true if account created, false if no account created
         */
        function loginUserFacebook(loginUsingSavedCredentials) {
            var deferred = $q.defer();
           
            if (window.cordova) {
                if (loginUsingSavedCredentials) {
                    $localForage.getItem('fbAuthData').then(function (fbAuthData) {
                        var currentTokenExpireDate = new Date(fbAuthData.expiration_date).getTime();
                        var today = new Date().getTime();
                        if (fbAuthData || today <= currentTokenExpireDate) {
                            proceedWithAuthData(fbAuthData);
                        } else {
                            // $cordovaFacebook.login(["public_profile", "email", "user_birthday", "user_work_history", "user_location", "user_friends", "user_education_history", "user_photos"]).then(function (success) {
                            $cordovaFacebook.login(["email"]).then(function (success) {

                                console.log(success);

                                //Need to convert expiresIn format from FB to date
                                var expiration_date = new Date();
                                expiration_date.setSeconds(expiration_date.getSeconds() + success.authResponse.expiresIn);
                                expiration_date = expiration_date.toISOString();

                                var facebookAuthData = {
                                    "id": success.authResponse.userID,
                                    "access_token": success.authResponse.accessToken,
                                    "expiration_date": expiration_date
                                };
                                // proceedWithAuthData(facebookAuthData);

                                $localForage.setItem('fbAuthData', facebookAuthData).then(function (res) {
                                    proceedWithAuthData(facebookAuthData);
                                });

                            }, function (error) {
                                console.error(error)
                                deferred.reject(error)
                            })
                        }
                    });
                } else {
                    // $cordovaFacebook.login(["public_profile", "email", "user_birthday", "user_work_history", "user_location", "user_friends", "user_education_history", "user_photos"]).then(function (success) {
                    $cordovaFacebook.login(["email"]).then(function (success) {

                        console.log(success);

                        //Need to convert expiresIn format from FB to date
                        var expiration_date = new Date();
                        expiration_date.setSeconds(expiration_date.getSeconds() + success.authResponse.expiresIn);
                        expiration_date = expiration_date.toISOString();

                        var facebookAuthData = {
                            "id": success.authResponse.userID,
                            "access_token": success.authResponse.accessToken,
                            "expiration_date": expiration_date
                        };
                        $localForage.setItem('fbAuthData', facebookAuthData).then(function (res) {
                            proceedWithAuthData(facebookAuthData);
                        });
                    }, function (error) {
                        console.error(error)
                        deferred.reject(error)
                    })
                }
            } else {
                proceedWithAuthData();
            }

            function proceedWithAuthData(input) {
                if (input) {
                    var credential = firebase.auth.FacebookAuthProvider.credential(input.access_token)
                    firebase.auth().signInAndRetrieveDataWithCredential(credential).then(function (result) {
                        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                        var token = result.credential.accessToken;
                        // The signed-in user info.
                        var user = result.user;
                        window.FirebasePlugin.getToken(function(token) {
                                console.log("==========TOKEN:======   "+token);
                            result.fcm_token = token;
                            // save this server-side and use it to push notifications to this device
                            updateUserDBProperies(result).then((value) => {
                                getCurrentUserDbEntry().then((user) => {
                                        var completedProfile = user.get("completedProfile");
                                        deferred.resolve(completedProfile);
                                });
                            });
                        }, function(error) {
                            console.error(error);
                        });
                    }
                    ).catch(function (error) {
                        debugger;
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                        console.error(error);
                        deferred.reject(error);
                    });
                } else {
                    LoginFirebaseFacebook().then((value) => {
                        // var ref = firebase.auth().currentUser;
                        // var acessToken = value.credential.accessToken;
                        // var json = {
                        //     username: ref.uid,
                        //     firstName: ref.displayName.split(' ')[0],
                        //     completedProfile: false,
                        //     authData: {
                        //         facebook: {
                        //             id: value.additionalUserInfo.profile.id,
                        //             access_token: acessToken,
                        //             refresh_token: ref.refreshToken
                        //         }
                        //     }
                        // }
                        
                        updateUserDBProperies(value).then((value) => {
                            getCurrentUserDbEntry().then((user) => {
                                var completedProfile = user.get("completedProfile");
                                deferred.resolve(completedProfile);
                            });
                        });
                    });
                }
                
            }
            return deferred.promise;

        }

        function updateUserDBProperies(value) {
            var user = firebase.auth().currentUser;
            var acessToken = value.credential.accessToken;
            var json = {
                username: user.uid,
                firstName: user.displayName.split(' ')[0],
                authData: {
                    facebook: {
                        id: value.additionalUserInfo.profile.id,
                        access_token: acessToken,
                        refresh_token: user.refreshToken
                    }
                }
                
            }
            if (window.cordova) {
                json.fcm_token = value.fcm_token;
            }
            var ref = firebase.database().ref("_users");
            var deferred = $q.defer();
            ref.orderByChild("authData/facebook/id").equalTo(json.authData.facebook.id).once('value', function (snapshot) {
                var user = snapshot.val();
                if(!user){
                    json.localId = guid();
                    var dbResult = ref.push(json).then((snapshot) => {
                        deferred.resolve();
                    }).catch((e) => {
                        deferred.reject(e);
                    });
                } else {
                    var entryId = Object.keys(snapshot.val())[0];
                    firebase.database().ref("_users/" + entryId).update(json).then((snapshot) => {
                        deferred.resolve();
                    }).catch((e) => {
                        deferred.reject(e);
                    });
                    // deferred.resolve(O);
                }
            }).catch((e) => {
                deferred.reject(e);
            });
            return deferred.promise;  
        }
        function LoginFirebaseFacebook(facebookAuthData) {
            var provider = new firebase.auth.FacebookAuthProvider();
            var deferred = $q.defer();

            provider.setCustomParameters({
                'display': 'popup'
            });
            //TODO: add all providers from fbPermissionsString;
            // provider.addScope('user_birthday');
            var facebookLoginPromise = firebase.auth().signInWithPopup(provider)
            facebookLoginPromise.then(function (result) {
                // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                deferred.resolve(true)
                // ...
            }
            ).catch(function (error) {
                console.log(error);
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                // ...
                deferred.reject(error)
                // deferred.reject(err)
            });
            return facebookLoginPromise;
        }

        function getFacebookFirstName() {
            var deferred = $q.defer();

            getFBAccessToken()
                .then(

                    function (accessToken) {
                        if (!window.cordova) {
                            FB.api('/me', {
                                fields: 'first_name,birthday,education,work,email',
                                access_token: accessToken
                            }, function (response) {
                                console.log('response email data', response);
                                respond(response);
                                deferred.resolve()
                            });
                        } else {
                            facebookConnectPlugin.api('/me?fields=first_name,birthday,education,work,email&access_token=' + accessToken, null,
                                function (response) {
                                    respond(response);
                                },
                                function (err) {
                                    console.log(err);
                                    deferred.reject(err)
                                });
                        }

                        function respond(response) {
                            var returnObject = {
                                company: "",
                                jobTitle: "",
                                school: "",
                                yearGraduated: "",
                                first_name: "",
                                email: ""
                            }
                            //Get First_Name
                            returnObject.first_name = response.first_name

                            //get email 
                            returnObject.email = response.email

                            //Get work
                            var arrayOfWorkObjects = response.work
                            //need to find the most recent one using the start and end date
                            var mostRecentMoment;

                            _.each(arrayOfWorkObjects, function (element, index, list) {
                                var startDate = element.start_date;

                                if (startDate) {
                                    var startDateMoment = moment(startDate)
                                    //if this start date is later than our most recent one, use it
                                    if (startDateMoment.isAfter(mostRecentMoment) || !mostRecentMoment) {
                                        mostRecentMoment = startDateMoment;
                                        returnObject.company = element.employer.name;
                                        returnObject.jobTitle = element.position.name;
                                    }
                                }

                            });

                            //Get Education
                            var arrayOfWorkObjects = response.education
                            //need to find the most recent one using the start and end date
                            var mostRecentMoment;
                            _.each(arrayOfWorkObjects, function (element, index, list) {
                                if (element.year) {
                                    var startDate = element.year.name;

                                    if (startDate) {
                                        var startDateMoment = moment(startDate)
                                        //if this start date is later than our most recent one, use it
                                        if (startDateMoment.isAfter(mostRecentMoment) || !mostRecentMoment) {
                                            mostRecentMoment = startDateMoment;
                                            returnObject.school = element.school.name;
                                            returnObject.yearGraduated = element.year.name;
                                        }
                                    }
                                }

                            });

                            //Get Birthday
                            returnObject.birthday = moment(response.birthday);

                            //return result
                            return deferred.resolve(returnObject)
                        }
                    })
                .catch(function (error) {
                    console.error(error);
                    return deferred.reject(error);
                })

            return deferred.promise;
        }

        /**
         * @return {friends} Friends list who is using this app.
         */
        function getFacebookFriends() {
            var deferred = $q.defer();

            getFBAccessToken()
                .then(
                    function (accessToken) {
                        if (!window.cordova) {
                            FB.api('/me', {
                                fields: 'friends',
                                access_token: accessToken
                            }, function (response) {
                                respond(response);
                            });
                        } else {
                            facebookConnectPlugin.api('/me?fields=friends&access_token=' + accessToken, null,
                                function (response) {
                                    console.log(response);
                                    respond(response);
                                },
                                function (err) {
                                    console.log(err);
                                });
                        }

                        function respond(response) {
                            if (!response || response.error) {
                                return deferred.reject('Error occured');
                            } else {
                                console.log(response);
                                return deferred.resolve(response.friends)
                            }
                        }
                    })
                .catch(function (error) {
                    console.error(error);
                    return $q.reject(error);
                })

            return deferred.promise;
        }

        /**
         * @return {Promise<object>} work
         * @return {string} work.company
         * @return {string} work.jobTitle
         */
        function getFacebookMostRecentCompanyAndJobTitle() {
            var deferred = $q.defer();

            getFBAccessToken()
                .then(
                    function (accessToken) {
                        if (!window.cordova) {
                            FB.api('/me', {
                                fields: 'work',
                                access_token: accessToken
                            }, function (response) {
                                respond(response);
                            });
                        } else {
                            facebookConnectPlugin.api('/me?fields=work&access_token=' + accessToken, null,
                                function (response) {
                                    respond(response);
                                },
                                function (err) {
                                    console.log(err);
                                });
                        }

                        function respond(response) {
                            if (!response || response.error) {
                                return deferred.reject('Error occured');
                            } else {

                                var arrayOfWorkObjects = response.work
                                //need to find the most recent one using the start and end date
                                var mostRecentMoment;
                                var returnObject = {
                                    company: "",
                                    jobTitle: ""
                                }
                                _.each(arrayOfWorkObjects, function (element, index, list) {
                                    var startDate = element.start_date;

                                    if (startDate) {
                                        var startDateMoment = moment(startDate)
                                        //if this start date is later than our most recent one, use it
                                        if (startDateMoment.isAfter(mostRecentMoment) || !mostRecentMoment) {
                                            mostRecentMoment = startDateMoment;
                                            returnObject.company = element.employer.name;
                                            returnObject.jobTitle = element.position.name;
                                        }
                                    }

                                });

                                return deferred.resolve(returnObject)
                            }
                        }
                    })
                .catch(function (error) {
                    console.error(error);
                    return $q.reject(error);
                })

            return deferred.promise;
        }

        function logOut() {
            var deferred = $q.defer();
            if (window.cordova) {
                $cordovaFacebook.logout();
                $localForage.setItem('fbAuthData', null);
                firebase.auth().signOut()
                    .then(function () {
                        // Sign-out successful.
                        deferred.resolve()
                    })
                    .catch(function (error) {
                        // An error happened
                        console.error(error);
                        deferred.reject(error);
                    });
            } else {
                firebase.auth().signOut()
                    .then(function () {
                        // Sign-out successful.
                        deferred.resolve()
                    })
                    .catch(function (error) {
                        // An error happened
                        console.error(error);
                        deferred.reject(error);
                    });
            }
            return deferred.promise;
        }

        function logOutAndReturnToWelcomeScreen() {
            return logOut()
                .then(
                    function (result) {
                        return $state.go('signup-1')
                    })
                .catch(function (error) {
                    console.error(error);
                    return $q.reject(error);
                })
        }

        /**
         * @return {Promise<object>} work
         * @return {string} work.school
         * @return {number} work.yearGraduated
         */
        function getFacebookMostRecentEducation() {
            var deferred = $q.defer();

            getFBAccessToken()
                .then(
                    function (accessToken) {
                        if (!window.cordova) {
                            FB.api('/me', {
                                fields: 'education',
                                access_token: accessToken
                            }, function (response) {
                                respond(response);
                            });
                        } else {
                            facebookConnectPlugin.api('/me?fields=education&access_token=' + accessToken, null,
                                function (response) {
                                    respond(response);
                                    console.log(response);
                                },
                                function (err) {
                                    console.log(err);
                                });
                        }

                        function respond(response) {
                            if (!response || response.error) {
                                return deferred.reject('Error occured');
                            } else {

                                var arrayOfWorkObjects = response.education
                                //need to find the most recent one using the start and end date
                                var mostRecentMoment;
                                var returnObject = {
                                    school: "",
                                    yearGraduated: ""
                                }
                                _.each(arrayOfWorkObjects, function (element, index, list) {
                                    if (element.year) {
                                        var startDate = element.year.name;

                                        if (startDate) {
                                            var startDateMoment = moment(startDate)
                                            //if this start date is later than our most recent one, use it
                                            if (startDateMoment.isAfter(mostRecentMoment) || !mostRecentMoment) {
                                                mostRecentMoment = startDateMoment;
                                                returnObject.school = element.school.name;
                                                returnObject.yearGraduated = element.year.name;
                                            }
                                        }
                                    }

                                });

                                return deferred.resolve(returnObject)
                            }
                        }
                    })
                .catch(function (error) {
                    console.error(error);
                    return $q.reject(error);
                })

            return deferred.promise;
        }

        /**
         * @return {moment} Date in moment format
         */
        function getBirthdayMoment() {
            var deferred = $q.defer();

            getFBAccessToken()
                .then(
                    function (accessToken) {
                        if (!window.cordova) {
                            FB.api('/me', {
                                fields: 'birthday',
                                access_token: accessToken
                            }, function (response) {
                                respond(response);
                            });
                        } else {
                            facebookConnectPlugin.api('/me?fields=birthday&access_token=' + accessToken, null,
                                function (response) {
                                    respond(response);
                                },
                                function (err) {
                                    console.log(err);
                                });
                        }

                        function respond(response) {
                            if (!response || response.error) {
                                return deferred.reject('Error occured');
                            } else {
                                var birthdayMoment = moment(response.birthday);
                                return deferred.resolve(birthdayMoment)
                            }
                        }
                    })
                .catch(function (error) {
                    console.error(error);
                    return $q.reject(error);
                })

            return deferred.promise;
        }

        /**
         * @return {string} image url?
         */
        function getFacebookProfilePicture() {
            var deferred = $q.defer();
            getCurrentUserDbEntry().then(function (user) {
                if (user.publicPhotoLink0) {
                    deferred.resolve(user.get('publicPhotoLink0'))
                }
                else {
                    getFBAccessToken()
                        .then(
                            function (accessToken) {
                                if (!window.cordova) {
                                    FB.api('/me?fields=picture.width(800).height(800)', {
                                        // fields: '',
                                        access_token: accessToken
                                    }, function (response) {
                                        respond(response);
                                    });
                                } else {
                                    facebookConnectPlugin.api('/me?fields=picture.width(800).height(800)&redirect=false&access_token=' + accessToken, null,
                                        function (response) {
                                            respond(response);
                                        },
                                        function (err) {
                                            console.log(err);
                                        });
                                }

                                function respond(response) {
                                    if (!response || response.error) {
                                        return deferred.reject('Error occured');
                                    } else {
                                        if (response.data && response.data.url) {
                                            return deferred.resolve(response.data.url)
                                        } else if (response.picture && response.picture.data.url) {
                                            return deferred.resolve(response.picture.data.url)
                                        } else {
                                            return deferred.resolve("")
                                        }
                                    }
                                }
                            })
                        .catch(function (error) {
                            console.error(error);
                            return $q.reject(error);
                        })
                }
            }).catch((e) => {
                deferred.reject(e);
            })
            return deferred.promise;
        }

        function getCurrentUserDbEntry() {
            if(!user){
                user = firebase.auth().currentUser;
            }
            var ref = firebase.database().ref("_users");
            var deferred = $q.defer();
            ref.orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                var entryId = Object.keys(snapshot.val())[0];                
                var user = snapshot.val();
                if (user) {
                    user.waitForASave = {};
                    user.get = function(propertyName) {
                        console.log(user[entryId][propertyName]);
                        return user[entryId][propertyName];
                    }
                    user.getAttributes = function() {
                        return user[entryId];
                    }
                    user.set = function (propertyName, propertyValue) {
                        if (propertyValue) {
                            user.waitForASave[propertyName] = propertyValue;
                        }
                    }
                    user.save = function(firstArgument, result){
                        //create json from an array
                        firebase.database().ref("_users/" + entryId).update(user.waitForASave).then((snapshot) => {
                            user.waitForASave = {};
                            result.success(user);
                        }
                        ).catch((e) => {
                            user.waitForASave = {};
                            result.error(e);
                        });
                    }
                    deferred.resolve(user)
                }
                else {
                    deferred.reject();
                }
            });
            return deferred.promise;
        }

        function deletePicture(index) {
            var user = Parse.User.current();
            switch (index) {
                case 0:
                    user.set('publicPhotoLink1', "");
                    break;
                case 1:
                    user.set('publicPhotoLink2', "");
                    break;
                case 2:
                    user.set('publicPhotoLink3', "");
                    break;
                case 3:
                    user.set('publicPhotoLink4', "");
                    break;
            }

            return user.save(null, {
                success: function (user) {
                    console.log('successfully deleted', user)
                    return $q.resolve(true)

                },
                error: function (error) {
                    // An error occurred while saving one of the objects.
                    return $q.reject(error)
                }
            });
        }

        /**
         * @return {string} image url?
         */
        function getFacebookLastFourProfilePictures() {
            var deferred = $q.defer();
            // var user = Parse.User.current();
            getCurrentUserDbEntry().then(function (user) {


                // When user already has at least one photo, that means they are NOT signing up for first time
                if (user.get('publicPhotoLink1') || user.get('publicPhotoLink2') || user.get('publicPhotoLink3') || user.get('publicPhotoLink4')) {
                    var publicImagesArray = [
                        user.get('publicPhotoLink1') || 'img/camera2.png',
                        user.get('publicPhotoLink2') || 'img/camera2.png',
                        user.get('publicPhotoLink3') || 'img/camera2.png',
                        user.get('publicPhotoLink4') || 'img/camera2.png'
                    ]
                    deferred.resolve(publicImagesArray);
                }
                // When user doesn't have any photo, that means they are signing up for first time
                else {
                    var publicImagesArray = [
                        user.get('publicPhotoLink1'),
                        user.get('publicPhotoLink2'),
                        user.get('publicPhotoLink3'),
                        user.get('publicPhotoLink4')
                    ]

                    if ((publicImagesArray[0] && publicImagesArray[1]) || (publicImagesArray[0] && publicImagesArray[2]) || (publicImagesArray[0] && publicImagesArray[3]) || (publicImagesArray[1] && publicImagesArray[2]) || (publicImagesArray[1] && publicImagesArray[3]) || (publicImagesArray[2] && publicImagesArray[3])) {
                        deferred.resolve(publicImagesArray);
                    }
                    else {
                        var returnArray = [];
                        var promiseArray = [];
                        var photoArray = [];
                        var accessTokenLocal;
                        getFBAccessToken()
                            .then(
                                function (accessToken) {
                                    accessTokenLocal = accessToken;
                                    if (!window.cordova) {
                                        FB.api('/me/albums?type=profile', {
                                            // fields: '',
                                            access_token: accessToken
                                        }, function (response) {
                                            respond(response);
                                        });
                                    } else {
                                        facebookConnectPlugin.api('/me/albums?type=profile&redirect=false&access_token=' + accessToken, null,
                                            function (response) {
                                                respond(response);
                                            },
                                            function (err) {
                                                console.log(err);
                                            });
                                    }

                                    function respond(response) {
                                        if (!response || response.error) {
                                            return deferred.reject('Error occured');
                                        } else {
                                            //go through until we find one with the "profile pictures" name
                                            var profilePictureAlbumObj;
                                            _.each(response.data, function (element, index, list) {
                                                if (element.name == "Profile Pictures") {
                                                    //found it
                                                    profilePictureAlbumObj = element;
                                                }
                                            });
                                            if (profilePictureAlbumObj) {
                                                var albumId = profilePictureAlbumObj.id;
                                                pullProfilePics(albumId, accessTokenLocal)
                                            } else {
                                                return $q.reject('Profile Pictures album not found')
                                            }
                                        }
                                    }
                                })
                            .catch(function (error) {
                                console.error(error);
                                return $q.reject(error);
                            })
                    }

                    function pullProfilePics(albumId, accessToken) {
                        if (!window.cordova) {
                            FB.api('/' + albumId + '/photos', {
                                // fields: '',
                                access_token: accessToken
                            }, function (response) {
                                respond(response);
                            });
                        } else {
                            facebookConnectPlugin.api('/' + albumId + '/photos?limit=100&type=album&redirect=false&access_token=' + accessToken, null,
                                function (response) {
                                    respond(response);
                                },
                                function (err) {
                                    console.log(err);
                                });
                        }

                        function respond(response) {

                            if (!response || response.error) {
                                return deferred.reject('Error occured pulling profile pictures');
                            } else {

                                //this should give us an array of the user's photos
                                //get this first four IDs to pull
                                photoArray = response.data;
                                console.log("photoArray", photoArray)
                                //start at 1 so we don't pull the currentprofile picture
                                creatPromise(1)

                            }

                        };
                    }

                    function creatPromise(index) {
                        if (index < 5 && photoArray[index]) {
                            var photoId = photoArray[index].id;
                            promiseArray.push(pullFacebookPhoto(photoId, accessTokenLocal, index));
                        }
                        if (photoArray.length === index || (index === 5)) {
                            returnPhotoArray();
                        }
                    }

                    function returnPhotoArray() {
                        $q.all(promiseArray)
                            .then(
                                function (result) {
                                    $timeout(function () {
                                        // $ionicLoading.hide();
                                        deferred.resolve(returnArray);
                                    }, 10)
                                })
                            .catch(function (error) {
                                console.error(error);
                                return $q.reject(error);
                            })
                    }

                    function pullFacebookPhoto(photoId, accessToken, index) {
                        var deferred = $q.defer();

                        if (!window.cordova) {
                            FB.api('/' + photoId + '?fields=images', {
                                // fields: '',
                                access_token: accessToken
                            }, function (response) {
                                respond(response);
                            });
                        } else {
                            facebookConnectPlugin.api('/' + photoId + '?fields=images&redirect=false&access_token=' + accessToken, null,
                                function (response) {
                                    respond(response);
                                },
                                function (err) {
                                    console.log(err);
                                });
                        }

                        function respond(response) {

                            if (!response || response.error) {
                                console.error('Error occured pulling single profile picture');
                            } else {
                                var imageArray = response.images;
                                _.sortBy(imageArray, 'height')
                                var link = imageArray[imageArray.length - 1].source;
                                returnArray.push(link);
                                creatPromise(index + 1);
                                deferred.resolve(link);
                            }
                        }
                        return deferred.promise;

                    }
                }

            }).catch((e) => {
                deferred.reject(e);
            })
            return deferred.promise;
        }

        function getFBAccessToken() {
            var deferred = $q.defer();
            if (!window.cordova) {
                FB.getLoginStatus(function (response) {
                    respond(response);
                });
            } else {
                facebookConnectPlugin.getLoginStatus(function (response) {
                    respond(response);
                }, function (err) {
                    console.log(err);
                });
            }

            function respond(response) {
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                    console.log('authorized');
                    deferred.resolve(accessToken)
                } else if (response.status === 'not_authorized') {
                    // the user is logged in to Facebook, 
                    // but has not authenticated your app
                    console.log('not authorized');
                    deferred.reject('App is not authorized')
                } else {
                    // the user isn't logged in to Facebook.
                    console.log('not logged in');
                    deferred.reject('Not logged in')

                }
            }

            return deferred.promise;

        }

        /**
         * 
         * @param {string} firstName 
         * @param {moment} birthdayMoment 
         * @param {string} primarySport 
         * @param {string} school 
         * @param {number} graduationYear 
         * @param {number} heightFeet
         * @param {number} heightInches
         * @param {string} company
         * @param {string} jobTitle
         */
        function saveProfilePage1(firstName, lastName, birthdayMoment, primarySport, school, graduationYear, heightFeet, heightInches, company, jobTitle, userEmail) {

            // var user = firebase.auth().currentUser;
            // var ref = firebase.database().ref("_users");
            // var deferred = $q.defer();
            // ref.orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
            //     var user = snapshot.val();
            //     if (!user) {
            //         console.log("Can't find user wuth such id: " + user.uid, user)
            //         return deferred.reject(error)
            //     } else {
            //         var entryId = Object.keys(snapshot.val())[0];
            //         var json = {
            //             firstName: firstName,
            //             birthdayMMDDYYYY: birthdayMoment.format('MM/DD/YYYY'),
            //             age: getAge(birthdayMoment.format('MM/DD/YYYY')),
            //             primarySport: primarySport,
            //             graduationYear: graduationYear,
            //             heightFeet: heightFeet,
            //             heightInches: heightInches,
            //             school: school,
            //             company: company,
            //             jobTitle: jobTitle,
            //             userEmail: userEmail
            //         }
            //         firebase.database().ref("_users/" + entryId).update(json).then((snapshot) => {
            //             console.log('success user after save first name and others', user)
            //             return deferred.resolve()
            //         }).catch((error) => {
            //             return deferred.reject(error)
            //         });
            //     }
            // }).catch((error) => {
            //     //         return $q.reject(error)
            //     return deferred.reject(error)
            // });
            var deferred = $q.defer();

            getCurrentUserDbEntry().then(function (user) {
                console.log("user email on save setup profile=", userEmail);
                user.set('firstName', firstName)
                user.set('lastName', lastName)
                // user.set('manuallyVerified', true)
                user.set('birthdayMMDDYYYY', birthdayMoment.format('MM/DD/YYYY'))
                user.set('age', getAge(birthdayMoment.format('MM/DD/YYYY')))
                if (primarySport)
                    user.set('primarySport', primarySport)
                if (graduationYear)
                    user.set('graduationYear', graduationYear)
                if (heightFeet)
                    user.set('heightFeet', heightFeet)
                if (heightInches)
                    user.set('heightInches', heightInches)
                if (school)
                    user.set('school', school)
                if (company)
                    user.set('company', company)
                if (jobTitle)
                    user.set('jobTitle', jobTitle)
                if (userEmail)
                    user.set('userEmail', userEmail)
                return user.save(null, {
                    success: function (user) {
                        console.log('success user after save first name and others', user)
                         return deferred.resolve();

                        // Get the user from a non-authenticated method
                        // var query = new Parse.Query(Parse.User);
                        // query.get(user.objectId, {
                        //     success: function(userAgain) {
                        //         userAgain.set("username", "another_username");
                        //         userAgain.save(null, {
                        //             error: function(userAgain, error) {
                        //                 // This will error, since the Parse.User is not authenticated
                        //             }
                        //         });
                        //     }
                        // });
                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.
                        return deferred.reject(error)
                    }
                });
            });
            return deferred.promise;
        }

        function getAge(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }

        /**
         * 
         * @param {string} publicPhotoLink0
         * @param {string} publicPhotoLink1
         * @param {string} publicPhotoLink2
         * @param {string} publicPhotoLink3
         * @param {string} publicPhotoLink4
         */
        function saveProfilePage2Images(publicPhotoLink0, publicPhotoLink1, publicPhotoLink2, publicPhotoLink3, publicPhotoLink4) {
            var deferred = $q.defer();
            getCurrentUserDbEntry().then(function (user) {
                user.set('publicPhotoLink0', publicPhotoLink0)
                if (publicPhotoLink1) {
                    user.set('publicPhotoLink1', publicPhotoLink1)
                }
                if (publicPhotoLink2) {
                    user.set('publicPhotoLink2', publicPhotoLink2)
                }
                if (publicPhotoLink3) {
                    user.set('publicPhotoLink3', publicPhotoLink3)
                }
                if (publicPhotoLink4) {
                    user.set('publicPhotoLink4', publicPhotoLink4)
                }
    
                user.save(null, {
                    success: function (user) {
                        deferred.resolve()
                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.
                        deferred.reject(error)
                    }
                });
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {string} publicPhotoLink0
         */
        function saveProfileDescription(profileDescription) {
            var deferred = $q.defer();
            getCurrentUserDbEntry().then((user) => {
                user.set('profileDescription', profileDescription)
                user.save(null, {
                    success: function (user) {
                        deferred.resolve();
                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.
                        // deferred.error();
                    }
                });
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {object} questions
         * @param {string} questions.question1
         * @param {string} questions.question2
         * @param {string} questions.question3
         * @param {string} questions.question4
         */
        function saveProfileQuestions(questions) {
            var deferred = $q.defer();
            getCurrentUserDbEntry().then((user) => {
                user.set('question1', questions.question1 || "")
                user.set('question2', questions.question2 || "")
                user.set('question3', questions.question3 || "")
                user.set('question4', questions.question4 || "")

                user.save(null, {
                    success: function (user) {
                        deferred.resolve()
                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.
                        deferred.reject(error)
                    }
                });
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {string} publicPhotoLink0
         */
        function getProfileDescription(profileDescription) {
            var deferred = $q.defer();
            getCurrentUserDbEntry().then((user) => {
                if (user) {
                    if(profileDescription == null){
                        deferred.resolve(user.get('profileDescription'));
                    }
                    else{
                        deferred.resolve(user.get(profileDescription));
                    }
                }
                else {
                    deferred.reject(error)
                }
            });
            return deferred.promise;
            // var user = Parse.User.current()
            // var deferred = $q.defer();

            // var query = new Parse.Query(Parse.User);
            // query.get(user.id, {
            //     success: function (userAgain) {
            //         deferred.resolve(userAgain.attributes.profileDescription)
            //     },
            //     error: function (error) {

            //         deferred.reject(error)
            //     }
            // });
            // return deferred.promise;
        }

        /**
         * 
         * @param {number} ageMin 
         * @param {number} ageMax 
         * @param {number} distanceMilesMax 
         * @param {string} genderString "man", "woman"
         * @param {string} interestedInString "men", "women", or "both"
         */
        function saveProfilePage4(ageMin, ageMax, distanceMilesMax, genderString, interestedInString) {
            var deferred = $q.defer();

            getCurrentUserDbEntry().then((user) => {
                user.set('ageMin', parseInt(ageMin, 10))
                user.set('ageMax', parseInt(ageMax, 10))
                user.set('distanceMilesMax', parseInt(distanceMilesMax, 10))
                // user.set('locationString', locationString)

                // deprecating for geopoint 2017-08-13 16:09:00
                // user.set('locationLat', locationLat)
                // user.set('locationLng', locationLng)
                // var point = new Parse.GeoPoint({
                //     latitude: locationLat,
                //     longitude: locationLng
                // });
                // user.set('locationGeoPoint', point)

                user.set('interestedInString', interestedInString)
                user.set('genderString', genderString)

                //also note that the user has completed setup so they don't go through this flow again if they logout and login
                user.set('completedProfile', true)

                user.save(null, {
                    success: function (user) {
                        deferred.resolve()
                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.
                        deferred.reject(error)
                    }
                });
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {string} location 
         */
        function saveLocationString(location) {
            var deferred = $q.defer();
            var user = firebase.auth().currentUser;
            var ref = firebase.database().ref("_users");
            
            ref.orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                var entryId = Object.keys(snapshot.val())[0];
                //snapshot.ref is not working, so I declaring a full entry url using entryId
                var dbResult = firebase.database().ref("_users/" + entryId).update({ "locationString": location }).then((snapshot) => {
                    return deferred.resolve();
                }).catch((e) => {
                    return deferred.reject(error)
                });

            }).catch((e) => {
                return deferred.reject(error)
            });
            return deferred.promise;
        }

        /**
         * @return {Promise<Array>}
         */
        function getCurrentProfilePictureArray() {
            var returnArray = [];

            var deferred = $q.defer();
            getCurrentUserDbEntry((userAgain) => {
                if (user) {
                    for (var index = 0; index < 6; index++) {
                        returnArray.push(userAgain.get('publicPhotoLink' + index))
                    }
                    deferred.resolve(returnArray)
                }
                else {
                    deferred.reject(error)
                }
            });
            return deferred.promise;
            // var user = Parse.User.current()
            // var deferred = $q.defer();

            // var query = new Parse.Query(Parse.User);
            // query.get(user.id, {
            //     success: function (userAgain) {
            //         for (var index = 0; index < 6; index++) {
            //             returnArray.push(userAgain.get('publicPhotoLink' + index))
            //         }
            //         deferred.resolve(returnArray)
            //     },
            //     error: function (error) {

            //         deferred.reject(error)
            //     }
            // });
            return deferred.promise;

        }
        
        /**
         * @return {Promise<String>}
         */
        function getMainProfilePictureLink() {
            var deferred = $q.defer();
            getCurrentUserDbEntry((user) => {
                if (user) {
                    var temp = userAgain.get("publicPhotoLink0");
                    deferred.resolve(temp)
                }
                else {
                    deferred.reject(error)
                }
            });
            return deferred.promise;

            // var deferred = $q.defer();

            // var user = Parse.User.current()
            // var query = new Parse.Query(Parse.User);
            // query.get(user.id, {
            //     success: function (userAgain) {
            //         var temp = userAgain.get("publicPhotoLink0");
            //         deferred.resolve(temp)
            //     },
            //     error: function (error) {
            //         deferred.reject(error)
            //     }
            // });

            // return deferred.promise;

        }

        /**
         * @return {Promise<String>}
         */
        function getAllUserAttributes() {
            var deferred = $q.defer();
            getCurrentUserDbEntry().then((user) => {
                if (user) {
                    var temp = user.getAttributes();
                     deferred.resolve(temp)
                }
                else {
                    deferred.reject(error)
                }
            });
            return deferred.promise;
            // var deferred = $q.defer();

            // var user = Parse.User.current()
            // var query = new Parse.Query(Parse.User);
            // query.get(user.id, {
            //     success: function (userAgain) {
            //         var temp = userAgain.attributes;
            //         deferred.resolve(temp)
            //     },
            //     error: function (error) {
            //         deferred.reject(error)
            //     }
            // });

            // return deferred.promise;

        }

        /**
         * @return {Promise<String>}
         */
        function deleteProfile() {
            var deferred = $q.defer();
            getCurrentUserDbEntry((user) => {
                if (user) {
                    user.set('deleteProfile', true);
                    user.set('deleteProfileDate', new Date());
                    user.set('completedProfile', false);
                    user.save(null, {
                        success: function (userResult) {
                            //TODO: rewrite on Firebase
                            // var query = new Parse.Query("Swipes");
                            // query.equalTo("sender", user);

                            // query.find().then(function (results) {
                            //     // Collect one promise for each delete into an array.
                            //     var promises = [];
                            //     _.each(results, function (result) {
                            //         // Start this delete immediately and add its promise to the list.
                            //         promises.push(result.destroy());
                            //     });
                            //     // Return a new promise that is resolved when all of the deletes are finished.
                            //     return Parse.Promise.when(promises);

                            // }).then(function () {
                            //     // Every comment was deleted.
                            //     deferred.resolve(userResult)
                            // }, function (err) {
                            //     console.log(err);
                            // });

                        },
                        error: function (error) {
                            deferred.reject(error)
                        }
                    });
                }
                else {
                    deferred.reject(error)
                }
            });
            return deferred.promise;

            // var deferred = $q.defer();
            // var user = Parse.User.current();
            // user.set('deleteProfile', true);
            // user.set('deleteProfileDate', new Date());
            // user.set('completedProfile', false);
            // user.save(null, {
            //     success: function (userResult) {
            //         var query = new Parse.Query("Swipes");
            //         query.equalTo("sender", user);

            //         query.find().then(function (results) {
            //             // Collect one promise for each delete into an array.
            //             var promises = [];
            //             _.each(results, function (result) {
            //                 // Start this delete immediately and add its promise to the list.
            //                 promises.push(result.destroy());
            //             });
            //             // Return a new promise that is resolved when all of the deletes are finished.
            //             return Parse.Promise.when(promises);

            //         }).then(function () {
            //             // Every comment was deleted.
            //             deferred.resolve(userResult)
            //         }, function (err) {
            //             console.log(err);
            //         });

            //     },
            //     error: function (error) {
            //         deferred.reject(error)
            //     }
            // });

            return deferred.promise;
        }

        /**
         * @return {Promise<String>}
         */
        function reActivateProfile(user) {
            user.set('deleteProfile', false);
            user.set('reActivateProfileDate', new Date());
            user.save(null, {
                success: function (result) {
                    console.log("successfully re-activate");
                },
                error: function (err) {
                    console.log("error in re-activate");
                }
            });
        }
    }
    function guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      }


})();