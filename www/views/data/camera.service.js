(function () {
    'use strict';

    angular
        .module('app')
        .service('CameraService', CameraService)

    CameraService.$inject = ['$rootScope', '$q', '$cordovaCamera', '$timeout', '$ionicLoading', '$cordovaToast'];

    function CameraService($rootScope, $q, $cordovaCamera, $timeout, $ionicLoading, $cordovaToast) {

        function requestCameraAuthorization() {
            var defer = $q.defer();
            cordova.plugins.diagnostic.requestCameraAuthorization(function (status) {
                // console.log("Authorization request for camera use was " +
                // (status == cordova.plugins.diagnostic.permissionStatus.GRANTED ? "granted" : "denied"));
                if (status === cordova.plugins.diagnostic.permissionStatus.GRANTED) {
                    // console.log("Camera use is authorized");
                    defer.resolve(true);
                }
                else {
                    // console.log("Camera use is unauthorized");

                    openPhoneSettingPage().then(function (data) {
                        if (data) {
                            defer.resolve(true);
                        }
                        else {
                            defer.resolve(false);
                        }
                    });

                }
            }, function (error) {
                console.error(error);
            });

            return defer.promise;
        }

        this.checkCameraPermission = function () {
            // console.log('camera permissions');

            var defer = $q.defer();

            if (!$rootScope.android) {

                cordova.plugins.diagnostic.getCameraAuthorizationStatus(function (status) {
                    // console.log(status);
                    if (status === cordova.plugins.diagnostic.permissionStatus.GRANTED) {
                        // console.log("Camera use is authorized");
                        defer.resolve(true);
                    }
                    else {
                        // console.log("Camera use is unauthorized");
                        requestCameraAuthorization().then(function (data) {
                            if (data) {
                                defer.resolve(true);
                            }
                            else {
                                defer.resolve(false);
                            }
                        });
                    }
                }, function (error) {
                    console.error("The following error occurred: " + error);
                });
            }
            else {
                defer.resolve(true);
            }

            return defer.promise;

        }

        function openPhoneSettingPage() {
            var defer = $q.defer();

            function onConfirm(buttonIndex) {
                // console.log(buttonIndex);
                if (buttonIndex == 1) {

                    cordova.plugins.diagnostic.switchToSettings(function () {
                        // console.log("Successfully switched to Settings app");
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });

                    defer.resolve(false);

                }
            }

            navigator.notification.alert(
                'We need to use phone\'s camera in order to upload pictures',  // message
                onConfirm,              // callback to invoke with index of button pressed
                'Camera required',            // title
                'Open Settings'          // buttonLabels
            );
            return defer.promise;

        }

        function getCurrentUserDbEntry() {
            var user = firebase.auth().currentUser;
            var ref = firebase.database().ref("_users");
            var deferred = $q.defer();
            ref.orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                var user = snapshot.val();
                if (user) {
                    user.waitForASave = {};
                    user.get = function(propertyName) {
                        console.log(user.propertyName);
                        return user[propertyName];
                    }
                    user.set = function(propertyName, propertyValue){
                        user.waitForASave[propertyName] = propertyValue;
                    }
                    user.save = function(firstArgument, result){
                        //create json from an array
                        var entryId = Object.keys(snapshot.val())[0];
                        firebase.database().ref("_users/" + entryId).update(user.waitForASave).then((snapshot) => {
                            user.waitForASave = {};
                            result.success(user);
                        }
                        ).catch((e) => {
                            user.waitForASave = {};
                            result.error(e);
                        });
                    }
                    deferred.resolve(user)
                }
                else {
                    deferred.reject();
                }
            });
            return deferred.promise;
        }

        this.takePicture = function () {
            var defer = $q.defer();

            var options = {
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                quality: 60,
                correctOrientation: true,
                cameraDirection: 1
            };


            $cordovaCamera.getPicture(options).then(function (image) {
                //return base64 image to controller for cropping
                defer.resolve(image);
            }, function (err) {
                console.log(error)
            })
            return defer.promise;
        }


        this.pickPicture = function () {
            var defer = $q.defer();

            var options = {
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                quality: 60,
                correctOrientation: true,
                cameraDirection: 1
            };


            $cordovaCamera.getPicture(options).then(function (image) {
                defer.resolve(image);
            }, function (error) {
                // The file either could not be read, or could not be saved to Parse.
                console.log(error);
            });
            return defer.promise;
        }

        this.processCroppedPicture = function (imageBase64, photoIndex) {
            var defer = $q.defer();
            $ionicLoading.show({
                template: '<div class="dark-loader"><ion-spinner icon="ripple"></ion-spinner>'+
                          '<p>Uploading image .. </p>'+
                          '</div>'
              });
            // console.log(image);
            var user = firebase.auth().currentUser;
            //prepare string to firebase format
            var message = imageBase64.replace('data:image/png;base64,', '').replace(/\s/g, '');
            var fullPictureFirebasePath = '/playoff_photos/' + createUUID() + '.png';
            firebase.storage().ref(fullPictureFirebasePath).putString(message, 'base64').then(function (snapshot) {
                firebase.storage().ref(fullPictureFirebasePath).getDownloadURL().then(function (url) {
                    console.log(url);
                    console.log('Uploaded a base64 string!');
                    //update a field with url in the db
                    firebase.database().ref("_users").orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                        var entryId = Object.keys(snapshot.val())[0];
                        getCurrentUserDbEntry().then(function (result) {
                            switch (photoIndex) {
                                case 0:
                                    result.set('publicPhotoLink1', url);
                                    break;
                                case 1:
                                    result.set('publicPhotoLink2', url);
                                    break;
                                case 2:
                                    result.set('publicPhotoLink3', url);
                                    break;
                                case 3:
                                    result.set('publicPhotoLink4', url);
                                    break;
                                default:
                                    result.set('publicPhotoLink0', url);
                                    break;
                            }
                            result.save(null, {
                                success: function (result) {
                                    if (window.cordova) {
                                        $cordovaToast.showShortBottom('Image saved successfully');
                                    }
                                    $ionicLoading.hide();
                                    defer.resolve(url);
                                },
                                error: function (error) {
                                    defer.reject(error);
                                    console.log(error);
                                }
                            });
                            
                        });


                    }).catch((e) => {
                        console.log(e);
                    });
                });
            }).catch((e) => {
                console.log(e);
                $ionicLoading.hide();
            });

            // var user = Parse.User.current();
            // var username = user.get("username");
            // var parseFile = new Parse.File(username, { base64: imageBase64 }, 'image/jpeg');

            // parseFile.save().then(function (data) {
            //     // The file has been saved to Parse.
            //     console.log(data);

            //     var newImage = new Parse.Query('User');
            //     newImage.get(user.id)
            //         .then(function (result) {
            //             //decide which image to set in the database
            //             switch (photoIndex) {
            //                 case 0:
            //                     result.set('publicPhotoLink1', data._url);
            //                     break;
            //                 case 1:
            //                     result.set('publicPhotoLink2', data._url);
            //                     break;
            //                 case 2:
            //                     result.set('publicPhotoLink3', data._url);
            //                     break;
            //                 case 3:
            //                     result.set('publicPhotoLink4', data._url);
            //                     break;
            //                 default:
            //                     result.set('publicPhotoLink0', data._url);
            //                     break;
            //             }
            //             result.save();
            //             if (window.cordova) {
            //                 $cordovaToast.showShortBottom('Image saved successfully');
            //             }
            //             $ionicLoading.hide();
            //             defer.resolve(data._url);
            //         }, function (error) {
            //             defer.error(error);
            //             console.log(error);
            //         })

            // }, function (error) {
            //     // 
            //     console.log(error);
            // })
            return defer.promise;
        }
    }
    function createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789ABCDEF";
        for (var i = 0; i < 32; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[12] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
        s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
     
        var uuid = s.join("");
        return uuid;
    }
})();