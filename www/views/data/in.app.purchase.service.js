(function () {
    'use strict';

    angular
        .module('app')

        .factory("InAppPurchaseProducts", function () {
            return {
                products: []
            };
        })

        .service("InAppPurchaseService", ["InAppPurchaseProducts", "StoreProductId", "$q", "PaymentFactory", function (InAppPurchaseProducts, StoreProductId, $q, PaymentFactory) {

            this.getProducts = function () {
                var defer = $q.defer();
                console.log('get product called');
                console.log(StoreProductId);
                // if (window.cordova) {
                    //console.log(inAppPurchase.getProducts([StoreProductId]));
                    inAppPurchase
                        .getProducts([StoreProductId])
                        .then(function (products) {
                            console.log('products');
                            console.log(products);
                            InAppPurchaseProducts.products = products;
                            console.log(InAppPurchaseProducts.products);
                            defer.resolve(products);
                        })
                        .catch(function (err) {
                            console.log(err);
                            defer.reject(err);
                        });
                // }
                return defer.promise;
            }

            this.buyProduct = function (product) {
                var defer = $q.defer();
                if (window.cordova) {
                    inAppPurchase
                        .buy(product)
                        .then(function (data) {
                            if (data.receipt && data.transactionId) {
                              var paymentPromise = PaymentFactory.addPayment(data.transactionId, data.receipt, data.signature);
                              paymentPromise.then(function (data) {
                                console.log(data);
                                defer.resolve(true);
                              }, function (err) {
                                console.log(err);
                                defer.resolve(false);
                              })
                            } else {
                                defer.reject("Subscription Failed");
                            }
                          })
                        .catch(function (err) {
                            console.log(err);
                            defer.reject(err);
                        });
                }
                return defer.promise;
            }

        }])


})();