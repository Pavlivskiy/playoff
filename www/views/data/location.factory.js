(function() {
    'use strict';

    angular
        .module('app')
        .factory('LocationFactory', LocationFactory);

    LocationFactory.$inject = ['$q', '$ionicLoading', '$rootScope', 'AuthFactory', '$state'];

    function LocationFactory($q, $ionicLoading, $rootScope, AuthFactory, $state) {

        var lastLocationCheckTimeMoment = moment();
        var lagTimeMinutes = 10; //the amount of time until we force refresh location

        //for local factory storage
        var parseGeopointLocation;
        var localLocationObject;

        var service = {
            getCurrentLocation: getCurrentLocation,
            getCurrentLocationAsParseGeoPoint: getCurrentLocationAsParseGeoPoint,
            checkLocationPermission: checkLocationPermission
            // getCurrentLocationLatLng: getCurrentLocationLatLng
        };

        /**
         * @return {Parse.GeoPoint}
         * This also saves to Parse where the user is 
         */
        function getCurrentLocationAsParseGeoPoint(currUser) {
            // var currUser = Parse.User.current();
            if(currUser.get('premium') && currUser.get('matchByLocation')) {
                parseGeopointLocation = null;
                return $q.resolve(currUser.get('locationGeoPoint'));
            }
            //so we don't have to check too often
            if (parseGeopointLocation && lastLocationCheckTimeMoment.diff(moment(), 'minutes') < lagTimeMinutes) {
                //make sure we're only on the playoff screen
                if ($state.current.name == "playoff") {
                    return $q.resolve(parseGeopointLocation)
                }
            }
            return getCurrentLocation()
                .then(
                    function(result) {
                        var point = new Parse.GeoPoint(result.lat, result.lng)
                        parseGeopointLocation = point;

                        //save the point into parse
                        // var user = Parse.User.current()
                        AuthFactory.getCurrentUserDbEntry().then((user) => {
                            user.set('locationGeoPoint', point)

                            return user.save(null, {
                                success: function(user) {
                                    return $q.resolve(point)
                                },
                                error: function(error) {
                                    // An error occurred while saving one of the objects.
                                    return $q.reject(error)
                                }
                            });
                        });
                      
                    })
                .catch(function(error) {
                    console.error(error);
                    if (error.code && error.code == 209) {
                        //need to go to login
                        return AuthFactory.logOutAndReturnToWelcomeScreen()
                            .then(
                                function(result) {
                                    return $q.reject(error.code)
                                })
                            .catch(function(error) {
                                console.error(error);
                                return $q.reject(error);
                            })
                    } else {
                        return $q.reject(error);
                    }
                })
        }

        /**
         * @return {Promise<object>} result
         * @property {string} result.locationString
         * @property {number} result.lat
         * @property {number} result.lng
         *
         */
        function getCurrentLocation() {
            var deferred = $q.defer();

            //so we don't have to check too often
            if (localLocationObject && lastLocationCheckTimeMoment.diff(moment(), 'minutes') < lagTimeMinutes) {
                if ($state.current.name == "playoff") {
                    return $q.resolve(localLocationObject)
                }
            }

            lastLocationCheckTimeMoment = moment();

            var onSuccess = function(position) { // eslint-disable-line
                // var position = {
                //     coords: {
                //         latitude: 35.7714,
                //         longitude: -83.1988,
                //         altitude: 0,
                //         accuracy: 0,
                //         altitudeAccuracy: 0,
                //         heading: 0,
                //         speed: 0
                //     },
                //     timestamp: 1531510794000
                // }
                console.log('Latitude: ' + position.coords.latitude + '\n' +
                    'Longitude: ' + position.coords.longitude + '\n' +
                    'Altitude: ' + position.coords.altitude + '\n' +
                    'Accuracy: ' + position.coords.accuracy + '\n' +
                    'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                    'Heading: ' + position.coords.heading + '\n' +
                    'Speed: ' + position.coords.speed + '\n' +
                    'Timestamp: ' + position.timestamp + '\n');


                var geocoder = new google.maps.Geocoder;
                var lat = parseFloat(position.coords.latitude);
                var lng = parseFloat(position.coords.longitude);
                var latlng = {
                    lat: lat,
                    lng: lng
                };
               
                geocoder.geocode({
                    'location': latlng
                }, function(results, status) {
                    if (status === 'OK') {
                        console.log('Success reverse geocode: ', results, status)

                        //now need to get the first of the "neighborhood" results
                        var locationString;
                        _.each(results, function(element, index, list) {
                            if (element.types && element.types.length >= 1) {
                                var type = element.types[0];
                                if (type == "neighborhood") {
                                    locationString = element.formatted_address;
                                }
                            }
                        });

                        //if still undefined, use the next level up (not neighborhood, but locality)
                        if (!locationString) {
                            _.each(results, function(element, index, list) {
                                if (element.types && element.types.length >= 1) {
                                    var type = element.types[0];
                                    if (type == "locality") {
                                        locationString = element.formatted_address;
                                    }
                                }
                            });
                        }
                        var returnObject = {
                            locationString: locationString,
                            // locationString: 'locationString (test example)',
                            lat: lat,
                            lng: lng
                        };
                        localLocationObject = returnObject;
                        deferred.resolve(returnObject)
                    } else {
                        console.error(status)
                        deferred.reject(status)
                    }
                });

            };

            // onError Callback receives a PositionError object
            //
            function onError(error) {
                console.error('code: ' + error.code + '\n' +
                    'message: ' + error.message + '\n');
                deferred.reject(error.message)
            }

            if (window.cordova) {
                cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                    console.log("Location setting is " + (enabled ? "enabled" : "disabled"));
                    if (enabled) {
                        // onSuccess({});
                        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
                            enableHighAccuracy: true
                        });
                    } else {
                        openPhoneSettingPage();
                    }

                }, function(error) {
                    deferred.reject(error);
                    console.error("The following error occurred: " + error);
                });
            } else {
                navigator.geolocation.getCurrentPosition(onSuccess, onError, {
                    enableHighAccuracy: true
                });
            }

            return deferred.promise;

        }

        function checkLocationPermission() {
            var deferred = $q.defer();
            $ionicLoading.show();
            if (window.cordova) {
                if (!$rootScope.android) {
                    cordova.plugins.diagnostic.getLocationAuthorizationStatus(function(status) {
                        switch (status) {
                            case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                                // console.log("Permission not requested");
                                requestLocationPermission();
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED:
                                // console.log("Permission denied");
                                openPhoneSettingPage();
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                                // console.log("Permission granted always");
                                deferred.resolve(true);
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
                                // console.log("Permission granted only when in use");
                                deferred.resolve(true);
                                break;
                        }
                    }, function(error) {
                        deferred.reject(error);
                        console.error("The following error occurred: " + error);
                    });


                } else {

                    cordova.plugins.diagnostic.requestLocationAuthorization(function(status) {
                        switch (status) {
                            case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                                console.log("Permission not requested");
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                                console.log("Permission granted");
                                deferred.resolve(true);
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED:
                                console.log("Permission denied");
                                deferred.reject('Permission denied');
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                                console.log("Permission permanently denied");
                                openPhoneSettingPage();
                                break;
                        }
                    }, function(error) {
                        deferred.reject(error);
                        console.error(error);
                    })
                }
            } else {
                //for serve purpose only.

                $rootScope.currentLat = 26.854573199999997;
                $rootScope.currentLng = 75.8132289;
                $rootScope.myCurrentLat = 26.854573199999997;
                $rootScope.myCurrentLng = 75.8132289;

                deferred.resolve(true);

            }

            function requestLocationPermission() {

                cordova.plugins.diagnostic.requestLocationAuthorization(function(status) {
                    switch (status) {
                        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                            console.log("Permission not requested");
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.DENIED:
                            console.log("Permission denied");
                            openPhoneSettingPage();
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                            console.log("Permission granted always");
                            deferred.resolve(true);
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
                            console.log("Permission granted only when in use");
                            deferred.resolve(true);
                            break;
                    }
                }, function(error) {
                    deferred.reject(error);
                    console.error(error);
                }, cordova.plugins.diagnostic.locationAuthorizationMode.WHEN_IN_USE);
            }



            return deferred.promise;
        }

        function openPhoneSettingPage() {

            function onConfirm(buttonIndex) {

                if (buttonIndex == 0) {
                    cordova.plugins.diagnostic.switchToLocationSettings(function() {
                        console.log("Successfully switched to Settings app");
                    }, function(error) {
                        console.error("The following error occurred: " + error);
                    });
                }
            }

            navigator.notification.alert(
                'We need your location in order to find matches', // message
                onConfirm, // callback to invoke with index of button pressed
                'Location required', // title
                'Open Settings' // buttonLabels
            );
        }

        return service;

    }
})();