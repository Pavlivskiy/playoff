(function () {
    'use strict';

    angular
        .module('app')
        .factory('MatchFactory', MatchFactory);

    MatchFactory.$inject = ['$q', 'AuthFactory'];

    /**
     * @desc we're calling this MatchFactory because it deals with multiple data points:
     * Swipes, users, etc.
     */
    function MatchFactory($q, AuthFactory) {
        var excludeIdsArray = []

        var service = {
            getNext10InStackForUser: getNext10InStackForUser,
            get50MostRecentMatchesForUser: get50MostRecentMatchesForUser,
            getUserProfileInfo: getUserProfileInfo,
            rightSwipe: rightSwipe,
            leftSwipe: leftSwipe,
            upSwipe: upSwipe,
            unmatchUser: unmatchUser,
            undoSwipe: undoSwipe,
            reportAndUnmatchUser: reportAndUnmatchUser,
            reset: reset,
            reportUser: reportUser
        };

        return service;

        function makeNewCardData() {
            return {
                name: '',
                age: 18,
                image: '',
                description: '',
                primarySport: '',
                distanceFrom: 0.0
            }
        }
        FCMPlugin.onNotification(function(data){
            if(data.wasTapped){
              //Notification was received on device tray and tapped by the user.
              alert( JSON.stringify(data) );
            }else{
              //Notification was received in foreground. Maybe the user needs to be notified.
              alert( JSON.stringify(data) );
            }
        });
        function undoSwipe() {
            var user = Parse.User.current();
            var deferred = $q.defer();

            var lastReject = new Parse.Query("Swipes")
            lastReject.equalTo('sender', user)
            lastReject.first({
                success: function (results) {
                    results.destroy();
                    // results.save();
                    console.log(results);
                    deferred.resolve(results);
                },
                error: function (error) {
                    console.log(error);
                    deferred.reject(error)
                }
            })

            return deferred.promise;
        }

        /**
         * @param {string} options premium options ('00' to '11')
         * @param {boolean} [optionalSportMatch]
         * @param {boolean} [optionalSchoolMatch]
         *  //distance is already accounted for
         *
         * @return {Array} array of below objects
         * @return {object} card
         * @return {object.name}
         * @return {object.image}
         * @return {object.description}
         * @return {object.primarySport}
         * @return {object.distanceFrom}
         * @return {object.isMatch}
         * @return {object.heightFeet}
         * @return {object.heightInches}
         * @return {object.graduationYear}
         * @return {object.profileDescription}
         */
        function getNext10InStackForUser(options, optionalSportMatch, optionalSchoolMatch, matchDistance) {
            var deferred = $q.defer();

            if (!optionalSportMatch) {
                optionalSportMatch = false;
            }
            if (!optionalSchoolMatch) {
                optionalSchoolMatch = false;
            }
            getCurrentUserDbEntry().then((user) => {
                var userId = user.get("localId");
                // var test = user.attributes;
                // Parse.Cloud.run('getNext10InStackForUser', {
                //     userId: user.id,
                //     options: options,
                //     optionalSportMatch: optionalSportMatch,
                //     optionalSchoolMatch: optionalSchoolMatch,
                //     matchDistance: matchDistance,
                // })
                //     .then(
                //         function (results) {
                var json = {
                    excludeIds: excludeIdsArray
                }
                var client = new HttpClient();
                var fullUrl = 'https://us-central1-playoff-dating-app.cloudfunctions.net/getNext10InStackForUser?userId=' + userId + '&options=' + options;
                if (matchDistance) {
                    fullUrl = fullUrl + '&matchDistance=' + matchDistance;
                }
                console.log("additional ids to exlude: " + JSON.stringify(json))
                fetch(fullUrl, {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(json)
                }).then(function (res) {
                    return res.json();
                })
                    .then(function (results) {

                        // client.get(fullUrl, function (response) {
                        // do something with response
                        // var results = JSON.parse(response);
                        console.log('next 10 user response: ' + JSON.stringify(results))
                        //need to set up live query for potential matches
                        var cardArray = [];
                        var counter = 0;
                        excludeIdsArray = []
                        _.each(results, function (dataObject, index, list) {
                            var newCard = makeNewCardData()
                            var element = dataObject.user;
                            newCard.name = element.firstName;
                            newCard.image = element.publicPhotoLink0;
                            newCard.description = element.school;
                            newCard.primarySport = element.primarySport;
                            newCard.distanceFrom = dataObject.distanceFrom;
                            newCard.user = element; //adding this here for when we swipe - need data to submit to parse
                            newCard.isMatch = dataObject.isMatch; //adding this here for when we swipe - need data to submit to parse
                            newCard.heightFeet = dataObject.heightFeet;
                            newCard.heightInches = dataObject.heightInches;
                            newCard.graduationYear = dataObject.graduationYear;
                            newCard.profileDescription = dataObject.profileDescription;
                            newCard.age = dataObject.age;
                            newCard.question1 = dataObject.question1;
                            newCard.question2 = dataObject.question2;
                            newCard.question3 = dataObject.question3;
                            newCard.question4 = dataObject.question4;
                            cardArray.push(newCard)
                            excludeIdsArray.push(element.localId)
                            console.log(counter + " " + element.publicPhotoLink0)
                            console.log("localId:" + element.localId)
                            counter++;
                        });
                        deferred.resolve(cardArray);
                    })
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {string} userId
         * @return {object}
         */
        function getUserProfileInfo(requestedUserId, myUserId) {
            var deferred = $q.defer();
            var client = new HttpClient();
            var fullUrl = 'https://us-central1-playoff-dating-app.cloudfunctions.net/getUserProfileInfo?userId=' + requestedUserId + '&myUserId=' + myUserId;

            client.get(fullUrl, function (response) {
                // do something with response
                var results = JSON.parse(response);
                deferred.resolve(results);
            });
            // // var test = user.attributes;
            // Parse.Cloud.run('getUserProfileInfo', {
            //     userId: requestedUserId,
            //     myUserId: myUserId
            // })
            //     .then(
            //         function (result) {
            //             deferred.resolve(result);
            //         })
            //     .catch(function (error) {
            //         console.error(error);
            //         deferred.reject(error);
            //     })
            return deferred.promise;
        }

        /**
         * 
         * @param {ParseUserObject} parseUserSwipedRightOn 
         */
        function rightSwipe(parseUserSwipedRightOn, isMatch) {
            var deferred = $q.defer();
            AuthFactory.getCurrentUserDbEntry().then(function (user) {
                console.log('going to right swipe');
                var ref = firebase.database().ref("swipes");
                var currentDate = moment();
                var json = {
                    objectId: guid(),
                    recipient: parseUserSwipedRightOn.localId,
                    sender: user.get("localId"),
                    direction: 'right',
                    swipeDateX: parseFloat(currentDate.format('x'))

                }
                console.log('swipe object: ' + JSON.stringify(json));
                var dbResult = ref.push(json).then((snapshot) => {
                console.log('pushed, key: ' + snapshot.key);
                    deferred.resolve();
                }).catch((error) => {
                    console.error(error);
                    deferred.reject(error);
                });
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {ParseUserObject} parseUserSwipedLeftOn 
         */
        function leftSwipe(parseUserSwipedLeftOn) {
            var deferred = $q.defer();
            AuthFactory.getCurrentUserDbEntry().then(function (user) {
                console.log('going to left swipe');
                var ref = firebase.database().ref("swipes");
                var currentDate = moment();
                var json = {
                    objectId: guid(),
                    recipient: parseUserSwipedLeftOn.localId,
                    sender: user.get("localId"),
                    direction: 'left',
                    swipeDateX: parseFloat(currentDate.format('x'))

                }
                console.log('swipe object: ' + JSON.stringify(json));
                var dbResult = ref.push(json).then((snapshot) => {
                console.log('pushed, key: ' + snapshot.key);
                    deferred.resolve();
                }).catch((error) => {
                    console.error(error);
                    deferred.reject(error);
                });
            });
            return deferred.promise;
        }

        /**
         * 
         * @param {ParseUserObject} parseUserSwipedUpOn
         */
        function upSwipe(parseUserSwipedUpOn) {
            var deferred = $q.defer();
            return new Promise((resolve, reject) => {
                var ref = firebase.database().ref("_JoinUpSwipes_User");
                AuthFactory.getCurrentUserDbEntry().then(function (user) {
                    var json = {
                        objectId: guid(),
                        owningId: user.get("localId"),
                        relatedId: parseUserSwipedUpOn.localId,
                    }
                    console.log('swipe object: ' + JSON.stringify(json));
                    var dbResult = ref.push(json).then((snapshot) => {
                        console.log('pushed, key: ' + snapshot.key);
                        deferred.resolve();
                    }).catch((error) => {
                        console.error(error);
                        deferred.reject(error);
                    });
                });
            });

        }

        /**
         * @return {Array<object>}
         * @return {object} matchObject
         * @return {string} matchObject.firstName
         * @return {string} matchObject.id
         * @return {string} matchObject.publicPhotoLink0
         */
        function get50MostRecentMatchesForUser(showUnmatchedUsers) {
            // var user = Parse.User.current();
            var deferred = $q.defer();
            var unmatchIdArray = []

            // //default value to not show unmatched users
            if (!showUnmatchedUsers) {
                showUnmatchedUsers = false;
            }
            getCurrentUserDbEntry().then((user) => {
                var userLocalId = user.get("localId");
                var ref = firebase.database().ref("_JoinUnmatchList_User");
                ref.orderByChild("owningId").equalTo(userLocalId).once('value', function (snapshot) {
                    _.each(snapshot.val(), function (element, index, list) {
                        unmatchIdArray.push(element.relatedId)
                    });
                    query2(user)
                });
            });
            // var unmatchList = user.relation('unmatchList')
            // unmatchList.query().find({
            //     success: function(resultArray) {
            //         console.log(resultArray);
            //         _.each(resultArray, function(element, index, list) {
            //             unmatchIdArray.push(element.id)
            //         });
            //         query2()
            //     },
            //     error: function(error) {
            //         // request.log.error(JSON.stringify(error, null, 2))
            //         // response.error(error)
            //         // console.error(error)
            //         //return q.reject(error)
            //     },
            //      useMasterKey: true
            // })


            function query2(user) {
                var userLocalId = user.get("localId");
                console.log("localId:" + userLocalId)
                var client = new HttpClient();
                client.get('https://us-central1-playoff-dating-app.cloudfunctions.net/get50MostRecentMatchesForUser?userId=' + userLocalId, function (response, statusCode) {
                    // do something with response
                    // if (statusCode != 200) {
                    //     console.error(statusCode);
                    //     deferred.reject(statusCode);
                    // }
                    var results = JSON.parse(response);
                    var matchesArray = [];
                    //   var user = Parse.User.current();

                    var unmatchArray = user.get('unmatchList');

                    var matchObject, personData;
                    _.each(results, function (element, index, list) {
                        matchObject = element;
                        console.log(matchObject);
                        //add the data to our array if it's not the current user
                        if (matchObject.user1Data.id != user.get("localId")) {
                            personData = matchObject.user1Data
                        }
                        if (matchObject.user2Data.id != user.get("localId")) {
                            personData = matchObject.user2Data
                        }
                        personData.lastMessageText = matchObject.lastMessageText;
                        personData.lastMessageDateX = matchObject.lastMessageDateX;
                        personData.matchDate = matchObject.matchDate;
                        personData.key = matchObject.key;
                        if (showUnmatchedUsers == false) {
                            //remove an unmatched user
                            //by default, showUnmatchedUsers is false
                            if (_.contains(unmatchIdArray, matchObject.user1Data.id) || _.contains(unmatchIdArray, matchObject.user2Data.id)) {
                                return; //don't add
                            }
                        }
                        matchesArray.push(personData)
                    });
                    deferred.resolve(matchesArray);
                })
            }
            return deferred.promise;
        }

       

        function unmatchUser(userId) {
            var deferred = $q.defer();
            var userToReport = Parse.User.createWithoutData(userId)

            var currentUser = Parse.User.current()
            var relation = currentUser.relation('unmatchList')
            relation.add(userToReport)
            currentUser.save({
                success: function (results) {
                    deferred.resolve()
                },
                error: function (error) {
                    // request.log.error(JSON.stringify(error, null, 2))
                    // response.error(error)
                    console.error(error)
                    deferred.reject(error)
                    //return q.reject(error)    
                }
            })
            console.log('others unmatched');
            var otherRelation = userToReport.relation('unmatchList');
            otherRelation.add(currentUser)
            userToReport.save({}, { useMasterKey: true }).then(function (other) {
                console.log(other);
            }, function (err) {
                console.log(err);
            });
            return deferred.promise;
        }

        function reportAndUnmatchUser(userId, messages) {
            var deferred = $q.defer();
            var userToReport = Parse.User.createWithoutData(userId)

            var currentUser = Parse.User.current()

            //report the user
            var Reports = Parse.Object.extend("Reports");
            var report = new Reports();
            report.set('reportedUser', userToReport)
            report.set('reporter', currentUser)
            report.set('timeX', moment().format('x'))
            report.set('messagesArray', messages)

            report.save({
                success: function (results) {
                    unmatchUser(userId)
                        .then(
                            function (result) {
                                //need to add both the users and the chat history to the report block. maybe create a class?
                                deferred.resolve()
                            })
                        .catch(function (error) {
                            console.error(error);
                            deferred.reject(error);
                        })
                },
                error: function (error) {
                    // request.log.error(JSON.stringify(error, null, 2))
                    // response.error(error)
                    console.error(error)
                    //return q.reject(error)
                    deferred.reject(error);

                }
            })

            return deferred.promise;
        }

        function reportUser(userId) {
            var deferred = $q.defer();
            var userToReport = Parse.User.createWithoutData(userId)

            var currentUser = Parse.User.current()

            //report the user
            var Reports = Parse.Object.extend("Reports");
            var report = new Reports();
            report.set('reportedUser', userToReport)
            report.set('reporter', currentUser)
            report.set('timeX', moment().format('x'))

            report.save({
                success: function (results) {
                    deferred.resolve();
                },
                error: function (error) {
                    // request.log.error(JSON.stringify(error, null, 2))
                    // response.error(error)
                    console.error(error)
                    //return q.reject(error)
                    deferred.reject(error);

                }
            })

            return deferred.promise;
        }

        function reset() {
            var deferred = $q.defer();
            Parse.Cloud.run('reset')
                .then(
                    function (results) {
                        deferred.resolve(true);
                    })
                .catch(function (error) {
                    console.error(error.message);
                    deferred.reject(error.message);
                })
            return deferred.promise;
        }
        function getCurrentUserDbEntry() {
            var user = firebase.auth().currentUser;
            var ref = firebase.database().ref("_users");
            var deferred = $q.defer();
            ref.orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                var entryId = Object.keys(snapshot.val())[0];
                console.log(entryId);
                var user = snapshot.val();
                if (user) {
                    user.waitForASave = {};
                    user.get = function (propertyName) {
                        console.log(user[entryId][propertyName]);
                        return user[entryId][propertyName];
                    }
                    user.getAttributes = function () {
                        return user[entryId];
                    }
                    user.set = function (propertyName, propertyValue) {
                        user.waitForASave[propertyName] = propertyValue;
                    }
                    user.save = function (firstArgument, result) {
                        //create json from an array
                        firebase.database().ref("_users/" + entryId).update(user.waitForASave).then((snapshot) => {
                            user.waitForASave = {};
                            result.success(user);
                        }
                        ).catch((e) => {
                            user.waitForASave = {};
                            result.error(e);
                        });
                    }
                    deferred.resolve(user)
                }
                else {
                    deferred.reject();
                }
            });
            return deferred.promise;
        }
    }
    var HttpClient = function () {
        this.get = function (aUrl, aCallback) {
            var anHttpRequest = new XMLHttpRequest();
            anHttpRequest.onreadystatechange = function () {
                if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200) {
                    aCallback(anHttpRequest.responseText);
                } else if(anHttpRequest.readyState == 4 && anHttpRequest.status != 200){
                    aCallback(null, anHttpRequest.status);
                }
            }

            anHttpRequest.open("GET", aUrl, true);
            anHttpRequest.send(null);
        }
    }
    function guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      }
})();