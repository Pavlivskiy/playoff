(function () {
    'use strict';

    angular
        .module('app.messages')
        .factory('MessagesFactory', MessagesFactory);

    MessagesFactory.$inject = ['$q', '$rootScope', 'chatList', '$localForage', 'AuthFactory', 'MatchFactory', 'messagesLimit'];

    function MessagesFactory($q, $rootScope, chatList, $localForage, AuthFactory, MatchFactory, messagesLimit) {

        var service = {
            getMessagesBetweenUsers: getMessagesBetweenUsers,
            sendMessage: sendMessage,
            messageReceived: messageReceived,
            getMatches: getMatches
        };
        return service;

        function getMessagesBetweenUsers(otherUserId, skip) {
            console.log('called getMessagesBetweenUsers with parameters: ' + otherUserId + ", skip=" + skip);

            var deferred = $q.defer();
            var skipResult = skip ? skip : 0;
            AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                var userLocalId = currentUser.get("localId");
                console.log("localId:" + userLocalId)
                var client = new HttpClient();
                client.get('https://us-central1-playoff-dating-app.cloudfunctions.net/getMessagesBetweenUsers?userId=' + userLocalId + '&otherUserId=' + otherUserId + '&skip=' + skipResult, function (response, statusCode) {
                    // do something with response
                    console.log(response);
                    console.log(statusCode);
                    // if (statusCode != 200) {
                    //     console.error(statusCode);
                    //     deferred.reject(statusCode);
                    // }
                    var results = JSON.parse(response);
                    console.log(results);
                    var output = [];
                    _.each(results, function (message, index) {
                        // console.log(message.get('sender'));
                        output[index] = { sender: '', recipient: '', time: '', text: '' };
                        output[index].sender = message['sender'];
                        output[index].recipient = message['recipient'];
                        output[index].time = new Date(message['messageDateX']).getTime();
                        output[index].text = message['messageText'];
                    })
                    // console.log(output);
                    deferred.resolve(output)
                })
            });
            return deferred.promise;
        }

        function sendMessage(otherUserId, messageText, matchId) {
             var deferred = $q.defer();
            AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                var userLocalId = currentUser.get("localId");
                var url = "https://us-central1-playoff-dating-app.cloudfunctions.net/sendMessage"
                var messageDateX = new Date().getTime();
                var json = {
                    messageDateX: messageDateX,
                    userId: userLocalId,
                    otherUserId: otherUserId,
                    messageText: messageText,
                    matchId: matchId
                }
                fetch(url, {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(json)
                }).then(function (res) {
                    return res.json();
                })
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                // var user = Parse.User.current()
                // var Messages = Parse.Object.extend('Messages');
                // var message = new Messages()
                // var messageDateX = new Date().getTime();
                // console.log(messageDateX);
                // message.set('messageDateX', messageDateX)
                // message.set('sender', user)
                // message.set('senderId', user.id)

                // var userPointer = Parse.User.createWithoutData(otherUserId)
                // console.log(userPointer);
                // message.set('recipient', userPointer)
                // message.set('recipientId', userPointer.id)

                // message.set('messageText', messageText)
                // console.log('called')
                // var deferred = $q.defer();
                // message.save()
                //     .then(
                //     function (result) {
                //         var data = {
                //             'recipientId': otherUserId,
                //             'lastMessageText': messageText,
                //             'messageDateX': new Date(result.get('createdAt')).getTime()
                //         };
                //         service.messageReceived(data);

                //         deferred.resolve(result);
                //     })
                //     .catch(function (error) {
                //         console.error(error);
                //         deferred.reject(error);
                //     })
            });
            return deferred.promise;
        }

        function messageReceived(data) {
            console.log('messageReceived');
            //modify which one in matchesarray has new data
            var recipientId = data.recipientId;
            var matchObject = _.findWhere(chatList.data, {
                id: recipientId
            });
            if (matchObject) {
                matchObject.lastMessageText = data.lastMessageText;
                matchObject.lastMessageDateX = data.messageDateX;
                matchObject.unseen = data.unseen || false;
            }

            $localForage.setItem('chatList', chatList.data).then(function () {
                console.log('chatlist updated in local')
            })
        }

        function getMatches() {
            var deferred = $q.defer();
            // vm.matchesArray = 0;

            MatchFactory.get50MostRecentMatchesForUser()
                .then(
                    function (results) {
                        chatList.data = [];
                        // console.log(results);
                        _.each(results, function (item) {
                            if (!checkIfIdExistInArray(item, chatList.data)) {
                                chatList.data.push(item)
                            }
                        })
                        deferred.resolve(results);
                    })
                .catch(function (error) {
                    console.error(error);
                    deferred.reject(error);
                })
            return deferred.promise;
        }
        function checkIfIdExistInArray(current, array){
            var returnFlag = false;
            angular.forEach(array, item => {
                if([current.id].indexOf(item.id)!=-1){
                    returnFlag = true;
                    return returnFlag;
                }
            })
            return returnFlag;
        }

    }
    var HttpClient = function () {
        this.get = function (aUrl, aCallback) {
            var anHttpRequest = new XMLHttpRequest();
            anHttpRequest.onreadystatechange = function () {
                if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200) {
                    aCallback(anHttpRequest.responseText);
                } else if(anHttpRequest.readyState == 4 && anHttpRequest.status != 200){
                    aCallback(null, anHttpRequest.status);
                }
            }

            anHttpRequest.open("GET", aUrl, true);
            anHttpRequest.send(null);
        }
    }
})();