(function () {
    'use strict';

    angular
        .module('app')

        .factory("InAppPurchaseProducts2", function () {
            return {
                products: []
            };
        })

        .service("InAppPurchaseService2", ["InAppPurchaseProducts2", "StoreProductId2", "$q", "PaymentFactory", function (InAppPurchaseProducts2, StoreProductId2, $q, PaymentFactory) {
            this.getProducts = function () {
                var defer = $q.defer();
                var currentUser1 = firebase.auth().currentUser;
                console.log('get product called');
                // if (window.cordova) {
                    inAppPurchase
                        .getProducts([StoreProductId2])
                        .then(function (products) {
                            console.log('products');
                            console.log(StoreProductId2);

                            
                            console.log(InAppPurchaseProducts2.products);

                            defer.resolve(products);
                        })
                        .catch(function (err) {
                            console.log(err);
                            defer.reject(err);
                        });
                // }
                return defer.promise;
            }

            this.buyProduct = function (product) {
                console.log('my product');
                console.log(product);
                var defer = $q.defer();
                if (window.cordova) {
                    inAppPurchase
                        .buy(product)
                        .then(function (data) {
                            if (data.receipt && data.transactionId) {
                              var paymentPromise = PaymentFactory.addPayment(data.transactionId, data.receipt, data.signature);
                              paymentPromise.then(function (data) {
                                console.log(data);
                                var currentUser1 = firebase.auth().currentUser;
                                var d = new Date();
                                var a = d.toString() 
                                firebase.database().ref('users/' + currentUser1.uid).set({premiumTwo: true, premiumOnTwo: a});
                                InAppPurchaseProducts2.products = products;
                                defer.resolve(true);
                              }, function (err) {
                                console.log(err);
                                defer.resolve(false);
                              })
                            } else {
                                defer.reject("Subscription Failed");
                            }
                          })
                        .catch(function (err) {
                            if(err.message === "Item already owned") {
                                var currentUser1 = firebase.auth().currentUser;
                                var d = new Date();
                                var a = d.toString() 
                                firebase.database().ref('users/' + currentUser1.uid).set({premiumTwo: true, premiumOnTwo: a});
                            }
                            console.log(err);
                            defer.reject(err.message);
                        });
                }
                return defer.promise;
            }

        }])


})();