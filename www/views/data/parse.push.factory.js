(function () {
    'use strict';
    angular.module('app')
    .factory('ParsePush', ParsePushFactory);

    ParsePushFactory.$inject = ['$q'];
    function ParsePushFactory($q) {
  
      return {
  
        init: init,
        on: on,
        subscribe: subscribe,
        getSubscriptions: getSubscriptions,
        unsubscribe: unsubscribe,
        subscribeUser: subscribeUser,
        resetBadge: resetBadge,
        sendPush: sendPush
      };
  
  
      function init() {
        
        var defer = $q.defer();
        defer.resolve(1);

        // if (window.ParsePushPlugin) {
  
        //   ParsePushPlugin.getInstallationId(function (id) {
        //     subscribeUser().then(function(data)
        //     {
        //       console.log('parse push init in factory');              
        //       defer.resolve(id);
        //     });
        //   }, function (e) {
        //     console.log('error', e);
        //     defer.reject(e);
        //   });
        // } else {
        //   defer.reject('Not Parse Push');
        // }
        return defer.promise;
      }
  
      function getSubscriptions() {
        var defer = $q.defer();
        // ParsePushPlugin.getSubscriptions(function (subscriptions) {
        //   defer.resolve(subscriptions);
        // }, function (e) {
        //   defer.reject(e);
        // });
        return defer.promise;
      }
  
  
      function subscribeUser() {
        var defer = $q.defer();
        // var user = Parse.User.current();
  
        // if (window.ParsePushPlugin && user) {
     
        //   subscribe(user.id).then(function(data)
        //   {
        //     defer.resolve('subscribed');
        //   });
  
  
        // } else {
        //   defer.reject('Not device');
        // }
        return defer.promise;
      }
  
      function on(event, callback) {
        // if (window.ParsePushPlugin) {
        //   ParsePushPlugin.on(event, callback);
        // }
      }
  
      function resetBadge(success, error) {
        // if (window.ParsePushPlugin) {
        //   ParsePushPlugin.resetBadge(success, error);
        // }
      }
  
      function subscribe(channel) {
        var defer = $q.defer();
        // if (window.ParsePushPlugin) {
        //   ParsePushPlugin.subscribe(channel, function (resp) {
        //     defer.resolve(resp);
        //   }, function (err) {
        //     defer.reject(err);
        //   });
        // }
        return defer.promise;
      }
  
      function unsubscribe(channel) {
        var defer = $q.defer();
        // if (window.ParsePushPlugin) {
        //   ParsePushPlugin.unsubscribe(channel, function (resp) {
        //     defer.resolve(resp);
        //   }, function (err) {
        //     defer.reject(err);
        //   });
        // }
        return defer.promise;
      }
  
      function sendPush(channel,message,payload) {
    
        // Parse.Cloud.run('sendPush', {channel: channel, message: message,payload:JSON.stringify(payload)}, {
        //   success: function (data) {
  
        //   },
        //   error: function (error) {
        //     console.log(error);
        //   }
        // });
      }
    }
  
  
  })();
  