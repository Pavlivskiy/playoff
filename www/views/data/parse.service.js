(function () {
    'use strict';

    angular
        .module('app')
        .service('ParseAsService', ParseAsService);

    ParseAsService.$inject = ['ENV', '$rootScope', '$state', '$cordovaNetwork', '$timeout', '$ionicPopup'];

    function ParseAsService(ENV, $rootScope, $state, $cordovaNetwork, $timeout, $ionicPopup) {
        this.User = Parse.User;
        this.Query = Parse.Query;
        this.GeoPoint = Parse.GeoPoint;
        this.Object = Parse.Object;

        init()

        function init() {

            //parse fb
            //load fb parse
            //Dev 467174560306595
            var fbTestAppId = "237170033459835"
            var fbLiveAppId = "237170033459835"

            console.log('configuring ENV. ENV is', ENV)

            //init parse
            if (ENV == 'production') {
                console.log('Loading prod app')

                Parse.initialize("eSzbKSy1sf14JD8gkeuTmZyrRl4UROfgNz0O4L9U", 'DVBjdRNHg4LI8SxK0ybg7KVqsqNa10eRRIhSuxCi');
                // Parse.initialize("eSzbKSy1sf14JD8gkeuTmZyrRl4UROfgNz0O4L9U");
                Parse.serverURL = "https://parseapi.back4app.com/"
                Parse.masterKey = "muZQoD76DbERgvDXfJYd61HPHNaQai1B6v8cFNjy";

                //load the facebook sdk
                window.fbAsyncInit = function () {
                    Parse.FacebookUtils.init({
                        appId: fbLiveAppId,
                        autoLogAppEvents: true,
                        xfbml: true,
                        status: true,
                        cookie: true,
                        version: 'v2.10'
                    });
                    FB.AppEvents.logPageView();

                    // loadParse()
                    console.log('Parse FB loaded');
                    $rootScope.$broadcast('parse-init');

                    checkLogin()
                };
                document.addEventListener("deviceready", onDeviceReady, false);

                function onDeviceReady() {
                    var type = $cordovaNetwork.getNetwork();
                    console.log('in parse push service info');
                    var isOnline = $cordovaNetwork.isOnline();
                    console.log(isOnline);
                    if(!isOnline) {
                        navigator.splashscreen.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Playoff',
                            template: 'Internet connection is not available.'
                          });
                       
                          alertPopup.then(function(res) {
                            console.log('network connection failed');
                          });
                    }
                }
            } else {
                console.log('Loading dev app')
                //development app
                Parse.initialize("ent5YIa1kuvkJlQKI3scJyNjfgEW0VFtKGP2TrTr", "yjdAU5UXEUjyiJN1wpDxCugTq08PiFLEHOt8AjSa");
                // Parse.initialize("ent5YIa1kuvkJlQKI3scJyNjfgEW0VFtKGP2TrTr");
                Parse.serverURL = "https://parseapi.back4app.com/"
                Parse.masterKey = "vQUWsdtBoWhaEDhc4JLpnhhDfIaPaTaGS7UHjaKp";

                //load the facebook sdk
                window.fbAsyncInit = function () {
                    Parse.FacebookUtils.init({
                        appId: fbTestAppId,
                        autoLogAppEvents: true,
                        xfbml: true,
                        status: true,
                        cookie: true,
                        version: 'v2.10'
                    });
                    FB.AppEvents.logPageView(); // eslint-disable-line
                    console.log('Parse FB loaded')
                    $rootScope.$broadcast('parse-init');

                    checkLogin()
                    // loadParse()
                };
            }

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0]; // eslint-disable-line
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }

        function checkLogin() {
            var currentUser = Parse.User.current();
            if (currentUser) {
                // do stuff with the user
                // console.log('User is logged in');
            } else {
                // show the signup or login page
                $state.go('signup-1')

            }
        }

        ////////////////

        function exposedFn() { }
    }
})();