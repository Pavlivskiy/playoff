(function () {
  'use strict';
  angular.module('app.playoff').factory('PaymentFactory', PaymentFactory);

  function PaymentFactory($q) {

    var ParseObject = Parse.Object.extend('Payments',
      {},

      {
        addPayment: function (transactionId, receipt, signature) {
          var defer = $q.defer();
          var query = new ParseObject();
          var currentUser = Parse.User.current();
          query.set('user', currentUser)
            .set('transactionId', transactionId)
            .set('signature', signature)
            .set('receipt', receipt)
            .save()
            .then(function (result) {
              currentUser.set('premium', true);
              currentUser.set('premiumOn', new Date());
              currentUser.save().then(function (result) {
                console.log('save successfuly');
                console.log(result);
                defer.resolve(result);
              }, function (err) {
                console.log(err);
                defer.reject(err);
              });
            }, function (err) {
              console.log(err);
              defer.reject(err);
            })
          return defer.promise;
        }
      });
    return ParseObject;

  }

})();
