(function () {
    'use strict';

    angular
        .module('app')
        
        .service("CheckInternet", ["$cordovaNetwork", "$ionicLoading", "$q", function ($cordovaNetwork, $ionicLoading, $q) {

            this.internetStatus = function () {

                var defer = $q.defer();
                if (window.cordova && $cordovaNetwork.isOnline()) {
                    defer.resolve({ internet: true });
                }

                else if (!window.cordova) {
                    defer.resolve({ internet: true });
                }
                else {


                    $ionicLoading.hide();
                    function alertDismissed(buttonIndex) {

                    }

                    navigator.notification.alert(
                        'Internet appears to be offline.',  // message
                        alertDismissed,         // callback
                        'Offline',            // title
                        'Ok'                  // buttonName
                    );
                    defer.reject({ internet: false });
                }
                return defer.promise;
            }

        }])
        .factory("swipedOut", function () {
            return {
                data: []
            };
        })
        .factory("chatList", function () {
            return {
                data: []
            };
        })
        .factory("cameFromSearchPage", function () {
            return {
                isTrue: false
            };
        })
        // googleAutocompleteService
        .factory('googleAutocompleteService', function ($q) {

            var autocompleteService = new google.maps.places.AutocompleteService();
            var detailsService = new google.maps.places.PlacesService(document.createElement("input"));
        
            return {
              /**
               * Search an address from an input and and option country restriction
               * @param required input string
               * @param optional countryCode two letters code
               */
              searchAddress: function (input, countryCode) {
        
                var dfd = $q.defer();
        
                autocompleteService.getPlacePredictions({
                  input: input,
                  componentRestrictions: countryCode ? { country: countryCode } : undefined
                }, function (result, status) {
        
                  if (status == google.maps.places.PlacesServiceStatus.OK) {
        
                    console.log(status);
                    dfd.resolve(result);
                  }
                  else
                    dfd.reject(status);
                });
        
                return dfd.promise;
              },
              /**
               * Gets the details of a placeId
               * @param required placeId
               */
              getDetails: function (placeId) {
        
                var dfd = $q.defer();
        
                detailsService.getDetails({ placeId: placeId }, function (result) {
        
                  dfd.resolve(result);
                });
        
                return dfd.promise;
              }
            };
          })


})();