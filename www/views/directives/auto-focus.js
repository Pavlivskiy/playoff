angular.module('app.setup')
.directive('autoFocus', ['$rootScope',function($rootScope) {
  return {
    restrict: 'AEC',
    link: function(scope, element, attrs)
    {
      element.bind('blur', function()
      {
        if($rootScope.sendButtonClicked) {
          element[0].focus();
        }
      });
    }
  }
}]);