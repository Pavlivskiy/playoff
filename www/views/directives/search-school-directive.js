angular.module('app.setup')
.directive('searchSchoolDirective', ['$ionicModal','$timeout', function ($ionicModal, $timeout) {
    return {
      restrict: 'A',
      template: '',
      scope: {
      },
      link: function (scope, elem) {
        elem.bind('click', showModal);
        $scope = scope;
        
        $scope.close = function () {
          $timeout(function () {
            $scope.showAutocomplete = false;
            $scope.showModalObj.hide();
            $scope.showModalObj.remove();
          }, 10);
        };
        $scope.filterSchools = function(item) {
            $scope.showingSchoolResults = true;
            console.log(item);
            if (item.length > 2) {
                $scope.showFilteredResults = true;
            } else {
                $scope.showFilteredResults = false;
            }
        }
        $scope.setSchool = function(school) {
            $scope.showingSchoolResults = false;
            $scope.showFilteredResults = false;
            $scope.schoolSearch = false;
            $scope.selectedSchool = school;
        }
        function showModal() {
            $scope.selectedSchool = '';
            $scope.schoolArray = all_college_list;
          // console.log('search location')
          $ionicModal.fromTemplateUrl('views/directives/search-school-view.html', {
            scope: $scope,
            animation: 'slide-in-right'
          })
            .then(function (modal) {
              $scope.showModalObj = modal;
              $scope.showModalObj.show();
            })
        };

      }
    };
  }]);