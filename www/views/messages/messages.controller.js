(function () {
    'use strict';

    angular
        .module('app.messages', [])
        .controller('MessagesCtrl', MessagesCtrl);

    MessagesCtrl.$inject = ['$localForage', '$cordovaToast', 'chatList', 'cameFromSearchPage', '$scope', 'MatchFactory', '$timeout', '$state', '$rootScope', '$stateParams', '$ionicLoading', 'MessagesFactory', 'AuthFactory'];

    function MessagesCtrl($localForage, $cordovaToast, chatList, cameFromSearchPage, $scope, MatchFactory, $timeout, $state, $rootScope, $stateParams, $ionicLoading, MessagesFactory, AuthFactory) {
        var vm = this;
        vm.goToChat = goToChat;
        vm.goBack = goBack;
        vm.doRefresh = doRefresh;

        vm.queryFinished = false; //used for tracking status of pulling in matches

        function init() {
            // if (window.ParsePushPlugin) {
            //     ParsePush.resetBadge();
            // }
            doRefresh();
        }
        init();
        AuthFactory.subscribe(function (data) {
            console.log('New message has beed received');
            console.log(data);
            if (!data.tap) {
                var currentMessageIndex = getIndexCountWithCurrentPerson(data.sender);
                if (currentMessageIndex != null) {
                    vm.matchesArray[currentMessageIndex].lastMessageText = data.messageText;
                    vm.matchesArray[currentMessageIndex].lastMessageDateX = new Date(data.createdAt).getTime();
                    vm.matchesArray[currentMessageIndex].unseen = true;
                    $scope.safeApply();
                    $localForage.setItem('chatList', vm.matchesArray).then(function () {
                        console.log('chatlist saved in message page')
                    })
                }
            }
            else {
                var matchData = {
                    id: data.sender
                }
                goToChat(matchData);
            }
        });
        function getIndexCountWithCurrentPerson(id){
            var result = null;
            _.each(vm.matchesArray, function (current, index) {
                if(current.id == id){
                    result = index;
                }
            });
            return result;
        }
        $rootScope.$on('refreshMessagesList', function (event, data) {
            $timeout(function () {
                vm.queryFinished = false;
            }, 0);
            MessagesFactory.getMatches()
        })


        function goToChat(matchData) {
            return $state.go('chat', {
                matchData: matchData
            })
        }

        function checkListOrder() {
            if (vm.matchesArray.length) {

            }
        }

        $scope.$on('$ionicView.afterEnter', function () {
            compareWithLocalList();
            $ionicLoading.hide();
        });

        // function for compare local and global list.
        function compareWithLocalList() {
            
            $localForage.getItem('chatList').then(function (results) {
                // retrieve locally saved chat list
                console.log('local data');
                console.log(results);
                if (results) {
                    vm.localChatList = results;
                }

                vm.matchesArray = chatList.data;

                if (vm.localChatList) {
                    refreshListOrder();
                }
                else {
                    $localForage.setItem('chatList', vm.matchesArray).then(function () {
                        console.log('chatlist saved in message page')
                    })
                }
                // }
            })
        }

        function refreshListOrder() {
            // If the chatlist is retrieved from DB 
            console.log(vm.matchesArray);
            if (cameFromSearchPage.isTrue) {
                _.each(vm.matchesArray, function (chat, index) {
                    // match item in local chat list
                    var localIndex = _.findIndex(vm.localChatList, function (item) {
                        return item.id == chat.id;
                    })

                    if (localIndex != -1) {
                        // assign the status to the retrieved list item, whether seen/unseen
                        if (vm.matchesArray[index].lastMessageDateX < vm.localChatList[localIndex].lastMessageDateX) {
                            vm.matchesArray[index].unseen = vm.localChatList[localIndex].unseen;

                        }
                        else if (vm.matchesArray[index].lastMessageDateX > vm.localChatList[localIndex].lastMessageDateX) {
                            vm.matchesArray[index].unseen = true;
                            console.log('refreshListOrder True')
                            console.log('refreshListOrder True')
                            console.log('refreshListOrder True')

                        }
                        else if (vm.matchesArray[index].lastMessageDateX == vm.localChatList[localIndex].lastMessageDateX) {
                            // vm.matchesArray[index].unseen = vm.localChatList[localIndex].unseen;
                            vm.matchesArray[index].lastMessageDateX = vm.localChatList[localIndex].lastMessageDateX;
                        }
                    }
                })
                // $localForage.setItem('chatList', vm.matchesArray).then(function () {
                //     console.log('chatlist saved in message page')
                // });
            }
            else {
                _.each(vm.matchesArray, function (chat, index) {
                    // match item in local chat list
                    var localIndex = _.findIndex(vm.localChatList, function (item) {
                        return item.id == chat.id;
                    })

                    if (localIndex != -1) {
                        // assign the status to the retrieved list item, whether seen/unseen
                        if (vm.matchesArray[index].lastMessageDateX < vm.localChatList[localIndex].lastMessageDateX) {
                            // vm.matchesArray[index].unseen = vm.localChatList[localIndex].unseen;

                        }
                        else if (vm.matchesArray[index].lastMessageDateX > vm.localChatList[localIndex].lastMessageDateX) {
                            // vm.matchesArray[index].unseen = true;
                            console.log('refreshListOrder True')
                            console.log('refreshListOrder True')
                            console.log('refreshListOrder True')

                        }
                        else if (vm.matchesArray[index].lastMessageDateX == vm.localChatList[localIndex].lastMessageDateX) {
                            // vm.matchesArray[index].unseen = vm.localChatList[localIndex].unseen;
                            vm.matchesArray[index].lastMessageDateX = vm.localChatList[localIndex].lastMessageDateX;
                        }
                    }
                })
                $localForage.setItem('chatList', vm.matchesArray).then(function () {
                    console.log('chatlist saved in message page')
                });
            }

        }
        function doRefresh() {
            console.log('refresh list');
            vm.matchesArray = [];
            $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
            MessagesFactory.getMatches().then(function (results) {
                console.log(results);
                compareWithLocalList();
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            }, function (err) {
                console.log(err);
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            });
        }

        function goBack() {
            // chatList.data.length = 0;
            $state.go('playoff');
        }

        document.addEventListener("resume", function () {
            if ($state.current.name === 'messages') {
                if (window.cordova) {
                    $cordovaToast.showShortBottom('Refreshing your chats');
                }
                doRefresh();
            }
        });

        $scope.safeApply = function (fn) {
            this.$apply(fn);
        };
    }
})();