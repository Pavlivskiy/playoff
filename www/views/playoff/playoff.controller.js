(function () {
    'use strict';

    angular
        .module('app.playoff', [])
        .controller('PlayoffCtrl', PlayoffCtrl);

    PlayoffCtrl.$inject = ['$stateParams', 'swipedOut', '$window', 'StoreProductId', 'StoreProductId2', '$rootScope', 'cameFromSearchPage', '$scope', '$timeout', 'TDCardDelegate', 'MatchFactory', 'LocationFactory', '$ionicLoading', '$ionicPopup', '$state', '$ionicModal', '$ionicViewSwitcher', 'ParseAsService', 'InAppPurchaseService2', 'InAppPurchaseProducts2','InAppPurchaseService', 'InAppPurchaseProducts', '$q', 'MessagesFactory', 'AuthFactory', '$localForage', '$ionicHistory', '$cordovaNetwork', '$ionicPlatform', '$cordovaToast', 'appVersionNumber', '$cordovaLocalNotification'];

    function PlayoffCtrl($stateParams, swipedOut, $window, StoreProductId, StoreProductId2, $rootScope, cameFromSearchPage, $scope, $timeout, TDCardDelegate, MatchFactory, LocationFactory, $ionicLoading, $ionicPopup, $state, $ionicModal, $ionicViewSwitcher, ParseAsService, InAppPurchaseService2, InAppPurchaseProducts2, InAppPurchaseService, InAppPurchaseProducts, $q, MessagesFactory, AuthFactory, $localForage, $ionicHistory, $cordovaNetwork, $ionicPlatform, $cordovaToast, appVersionNumber, $cordovaLocalNotification) {
        var vm = this;
        var currentGeoPoint;
        var noUsersText = "Hmm, no athletes are near you. Want to try again?";
        var noLocationText = "Hmm, we couldn't find your location. Want to try again?";
        var serverTimeoutText = "We couldn't communicate with the server. Try again?";
        var noUsersExpendSearch = "Hmm, no athletes are near you. Expanding Search Radius..";
        var premiumTwo;
        var pricePayment;
        var periodPayment;
        vm.errorStatus; //undefined if good, "noUsers", "noLocation"

        vm.swipeUp = swipeUp;
        vm.swipeLeft = swipeLeft;
        vm.swipeRight = swipeRight;
        vm.openProfile = openProfile;
        vm.goToMessages = goToMessages;
        vm.reject = reject;
        vm.like = like;
        vm.remove = remove;
        vm.helpText = "";
        vm.getCards = getCards;
        vm.backtrack = backtrack;
        vm.swipedOutCards = swipedOut.data;

        vm.matchCard; //this is used for when we pop a profile up
        vm.goToMessageForNewMatch = goToMessageForNewMatch;
        vm.dismissMatchPopup = dismissMatchPopup;
        vm.hideLoading = hideLoading;
        vm.cards = [];
        vm.appSettings = {};
        vm.skipUpdate = skipUpdate;
        vm.updateApp = updateApp;
        vm.daysToUpdate;
        vm.device;
        $scope.showMatchModalFlag = false;
        $scope.loadingDataFlag = false;

        $scope.$watch('vm.errorStatus', function (newVal, oldVal) {
            switch (newVal) {
                case 'noUsers':
                    vm.showHelpText = true;
                    vm.helpText = noUsersText;
                    break;
                case 'noLocation':
                    vm.showHelpText = true;
                    vm.helpText = noLocationText;
                    break;
                case 'serverTimeout':
                    vm.showHelpText = true;
                    vm.helpText = serverTimeoutText;
                    break;
                case 'expendSearch':
                    vm.showHelpText = true;
                    vm.helpText = noUsersExpendSearch;
                    break;
                case 'nextStackError':
                    vm.showHelpText = true;
                    // vm.helpText = serverTimeoutText; //handled in function
                    break;
                default:
                    vm.showHelpText = false;
                    vm.helpText = "";
                    break;
            }
        });
      
        // FCMPlugin.onNotification(function (data) {
        //     console.log( JSON.stringify(data))
        //     var alarmTime = new Date();
        //     alarmTime.setMinutes(alarmTime.getMinutes() + 1);
        //     $cordovaLocalNotification.add({
        //         id: getRandomInt(1,1000),
        //         date: alarmTime,
        //         message: data.message_text,
        //         title: data.title,
        //         autoCancel: true,
        //         sound: null
        //     }).then(function () {
        //         console.log("The notification has been set");
        //     });
        //     // if(data.wasTapped){
        //     //   //Notification was received on device tray and tapped by the user.
        //     //   alert( JSON.stringify(data) );
        //     // }else{
        //     //   //Notification was received in foreground. Maybe the user needs to be notified.
        //     //   alert( JSON.stringify(data) );
        //     // }
        // });
        // Hide splash screen
        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            navigator.splashscreen.hide();
        }
        //back button disable
        $ionicPlatform.registerBackButtonAction(function () {
            navigator.app.exitApp();
        }, 100);
        // console.log('playoff controller');

        /* Check if we need to open the premium popup
            coming back from settings page 
         */
        if ($stateParams.action && $stateParams.action == 'openPremiumPopup') {
            showPurchaseModal();
        } else {
            if (window.cordova) {
                console.log('outside device ready');
                document.addEventListener('deviceready', function () {
                    console.log('inside device ready');
                    fetchAppSettings();
                    //init();
                }, false);
            } else {
                console.log('call browser');
                fetchAppSettings();
                //init();
            }

        }


        function makeNewCardData() {
            return {
                name: '',
                age: 18,
                image: '',
                description: '',
                primarySport: '',
                distanceFrom: 0.0
            }
        }

        function openPhoneSettingPage() {
            // Direct go to next setup.
            function onConfirm(buttonIndex) {
                if (buttonIndex == 1) {
                    console.log('No location is found');
                } else {
                    cordova.plugins.diagnostic.switchToSettings(function () {
                        console.log("Successfully switched to Settings app");
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }
            }

            navigator.notification.confirm(
                'You have denied location permission to Playoff. We need to access your phone\'s location in order to find matches for you.', // message
                onConfirm, // callback to invoke with index of button pressed
                'Location Access Required', // title
                'Cancel, Open Settings' // buttonLabels
            );

        }
        function updateApp() {
            console.log('Update app');
            if ($ionicPlatform.is('ios')) {
                window.location.href = 'https://itunes.apple.com/us/app/playoff-athlete-dating/id1292212774?mt=8';
            } else if($ionicPlatform.is('android')) {
                window.location.href = 'https://play.google.com/store/apps/details?id=com.playoffdatingapp.playoff';
            }
        }
        function skipUpdate() {
            $scope.appUpdatemodal.hide();
            $scope.appUpdatemodal.remove();
            init();
        }
        function showAppUpdateModal() {
            $ionicModal.fromTemplateUrl('./views/playoff/app.update.modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.appUpdatemodal = modal;
               // $scope.appUpdatemodal.show();
            });
        }
        function fetchAppSettings() {
            var query = new Parse.Query("Settings");
            query.limit(999)
            query.find({
                success: function (data) {
                    vm.appSettings = data[0].toJSON();
                    console.log(vm.appSettings);
                    console.log($ionicPlatform.is('ios'));
                    vm.device = $ionicPlatform.is('ios') ? 'ios' : 'android';
                    var newVersion = ($ionicPlatform.is('ios')) ? vm.appSettings.versionIOS : vm.appSettings.versionAndroid;
                    if (newVersion && appVersionNumber < newVersion) {
                        console.log('new version is available');
                        if (vm.appSettings.versionDate && new Date() > new Date(vm.appSettings.versionDate.iso)) {
                            // console.log('block user for update');
                            // vm.daysToUpdate = 0;
                            // showAppUpdateModal();
                            init();
                        } else if (vm.appSettings.versionDate) {
                            vm.daysToUpdate = parseInt((new Date(vm.appSettings.versionDate.iso) - new Date()) / (24 * 60 * 60 * 1000));
                            $localForage.getItem('appUpdateAlertDate').then(function (results) {
                                console.log(results);
                                if (!results) {
                                    $localForage.setItem('appUpdateAlertDate', new Date());
                                    showAppUpdateModal();
                                } else {
                                    if (parseInt((new Date() - new Date(results)) / (24 * 60 * 60 * 1000)) > 0) {
                                        $localForage.setItem('appUpdateAlertDate', new Date());
                                        showAppUpdateModal();
                                    } else {
                                        init();
                                    }
                                }
                            }, function(err) {
                                console.log(err);
                                init();
                            });

                        } else {
                            init();
                        }
                    } else {
                        init();
                    }

                },
                error: function (err) {
                    console.log(err)
                    init();
                }
            });
        }

        function init() { // eslint-disable-line
            var PremiunTime = 0;
            if($ionicPlatform.is('ios')) {
                PremiunTime = 1440;
            } else {
                PremiunTime = 10080;
            }
            cameFromSearchPage.isTrue = true;
            console.log('init called');
            if (window.cordova) {
                var type = $cordovaNetwork.getNetwork()
                console.log('playoff network info')
                console.log(type);
                var isOnline = $cordovaNetwork.isOnline()
                console.log(isOnline);
            }
            // var currentUser = Parse.User.current();
            var currentUser = firebase.auth().currentUser;
            console.log(currentUser);
            firebase.database().ref("_users").orderByChild("username").equalTo(currentUser.uid).once('value', function (snapshot) {
                console.log(snapshot);
            });
            firebase.database().ref('/users/' + currentUser.uid).once('value').then(function(snapshot) {
                return snapshot.val();
            }).then(function(data) {
                if(data) {
                    if(data.premiumTwo !== false) {
                        if(data.premiumOnTwo !== undefined) {
                            var dataNewDate = new Date(data.premiumOnTwo);
                            var dat = new Date();
                            var reduce = dat - dataNewDate;
                            var results = Math.floor(reduce/60000);
                            console.log(results);
                            if(results <= PremiunTime) {
                                vm.premiumTwo = true;
                            } else {
                                vm.premiumTwo = false;
                            }
                        } else {vm.premiumTwo = false;}
                    } else {
                        firebase.database().ref('users/' + currentUser.uid).set({premiumTwo: false, premiumOnTwo: 'undefined'});
                        vm.premiumTwo = false;
                    }
                } else {
                    firebase.database().ref('users/' + currentUser.uid).set({premiumTwo: false, premiumOnTwo: 'undefined'});
                    vm.premiumTwo = false;
                }
            });
            $localForage.getItem('welcomeMsgSeen').then(function (results) {
                console.log(results);
                if (!results) {
                    var welcomeAlert = $ionicPopup.alert({
                        title: 'Welcome to Playoff',
                        template: "Hello " + currentUser.displayName + "! Welcome to Playoff! Please provide accurate information for your athlete profile verification. Meanwhile, enjoy the app!"
                    });

                    welcomeAlert.then(function (res) {
                        $localForage.setItem('welcomeMsgSeen', true);
                        console.log('watch set to true')
                    });
                }
            });
            
            AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                console.log('fetch user');
                console.log(vm);
                //console.log(user.get('premium'));
                if (currentUser.get("manuallyVerified") == false || !currentUser.get("manuallyVerified")) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Manual Verification Fail',
                        template: "We're sorry, but your profile could not be verified. To get your account back up and running, please email marketing@playoffdatingapp.com."
                    });

                    alertPopup.then(function (res) {
                        $ionicHistory.clearCache().then(function () {
                            AuthFactory.logOutAndReturnToWelcomeScreen();
                        })
                    });
                } else {
                    if (currentUser.get("manuallyVerified") == true) {
                        $localForage.getItem('manuallyVerified').then(function (results) {
                            console.log(results);
                            if (!results) {
                                var welcomeAlert = $ionicPopup.alert({
                                    title: 'Congratulations!',
                                    template: "Hello " + currentUser.get("firstName") + ", your profile has been verified by our admin team. Enjoy the app!"
                                });

                                welcomeAlert.then(function (res) {
                                    $localForage.setItem('manuallyVerified', true);
                                });
                            }
                        });
                    }

                    // var user = ParseAsService.User.current();
                    var user = currentUser;

                    vm.distanceMilesMax = user.get('distanceMilesMax');

                    var currentUser1 = firebase.auth().currentUser;
                    


                    // firebase.database().ref('/users/' + currentUser1.uid).once('value').then(function(snapshot) {
                    //     return snapshot.val();
                    // }).then(function(data) {
                    //     if (data) {
                    //         vm.premiumTwo = true;

                    //     } else {
                    //         vm.optionsTwo = '00';
                    //         vm.premiumTwo = false;
                    //     }
                    // });

                    if (user.get('premium')) {
                        vm.isPremium = true;
                        vm.options = user.get('premiumOptions');

                    } else {
                        vm.options = '00';
                    }
                   
                    if (!user) {
                        return $state.go('signup-1')
                    }
                    if ($state.current.name === 'playoff') {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="ripple"></ion-spinner>'
                        })
                    }
                    // Get the chat list first, then find cards
                    MessagesFactory.getMatches().then(function (results) {
                        if (vm.cards && vm.cards.length <= 0) {
                            if ($state.current.name === 'playoff') {
                                getCards();
                            }
                        }
                    })
                }
            }, function (err) {
                console.log('err');
            });

        }

        function checkMatchModalCondition() {
            $scope.loadingDataFlag = false;
            if ($scope.showMatchModalFlag) {
                showMatch($scope.pushCard);
            }
        }

        /**
         * @return {array}
         */
        function getCards() {
            //temp for tests
            // showMatch({
            //     name: 'TestName',
            //     age: 18,
            //     image: 'http://maxpixel.freegreatpicture.com/static/photo/1x/Girl-Black-Tight-Dress-Beautiful-Woman-1293985.jpg',
            //     description: '',
            //     primarySport: '',
            //     distanceFrom: 0.0
            // })
            // return;

            // getStack()
            $scope.loadingDataFlag = true;
            var options;
            // var user = ParseAsService.User.current();
            AuthFactory.getCurrentUserDbEntry().then(function (user) {

                if (user.get('premium')) {
                    vm.isPremium = true;
                    vm.options = user.get('premiumOptions');
                    options = user.get('premiumOptions');

                } else {
                    vm.options = '00';
                    options = '00';
                }
                $ionicLoading.show({
                    template: '<ion-spinner icon="ripple"></ion-spinner>'
                })
                $timeout(function () {
                    vm.errorStatus = undefined;
                }, 0);
                $timeout(function () {
                    // debugger;
                }, 1500);

                LocationFactory.getCurrentLocationAsParseGeoPoint(user)
                    .then(

                        function (currentGeoPointResult) {
                            currentGeoPoint = currentGeoPointResult;
                            return getStack(options, true, false)
                        },
                        function (error) {
                            console.log(error);
                            $timeout(function () {
                                vm.errorStatus = "noLocation";
                            }, 0);
                            $ionicLoading.hide();
                            openPhoneSettingPage();
                        })
                    .catch(function (error) {
                        console.error(error);
                        //ADD ERROR HANDLER HERE
                    })
                    .then(
                        function (results) {
                            if (results && results.length == 0) {
                                return getStack(options, false, true)
                            } else {
                                checkMatchModalCondition();
                                return $q.resolve(results)
                            }
                        })
                    .catch(function (error) {
                        console.error(error);
                        checkMatchModalCondition();
                        return $q.reject(error);
                    })
                    .then(
                        function (results) {
                            if (results && results.length == 0) {
                                return getStack(options, false, false)
                            } else {
                                checkMatchModalCondition();
                                return $q.resolve(results)
                            }
                        })
                    .catch(function (error) {
                        console.error(error);
                        checkMatchModalCondition();
                        return $q.reject(error);
                    })
                    .then(
                        function (results) {
                            //done?
                            //handle no results
                            if (results && results.length == 0) {
                                if ($state.current.name === 'playoff') {
                                    console.log($state.current.name);
                                    var matchDistance = vm.distanceMilesMax < 50 ? 50 : vm.distanceMilesMax < 100 ? 100 : vm.distanceMilesMax < 150 ? 150 : 200;
                                    $timeout(function () {
                                        vm.errorStatus = "expendSearch";
                                    }, 0);
                                    $ionicLoading.show({
                                        template: '<ion-spinner icon="ripple"></ion-spinner>'
                                    })
                                    extendSearchDistance(options, false, false, matchDistance, results);
                                    // $timeout(function() {
                                    //     vm.errorStatus = "noUsers";
                                    //     return $q.resolve()
                                    // }, 0);
                                }
                            } else {
                                checkMatchModalCondition();
                                return $q.resolve(results)
                            }
                        })
                    .catch(function (error) {
                        console.error(error);
                        checkMatchModalCondition();
                        return $q.reject(error);
                    });
            })

            function searchPremiunTwoUsers(users) {
                    vm.cards = [];
                    //firebase.database().ref('users/' + 'yDB51kllm6').set({premiumTwo: false});
                    console.log(users)
                    users.map(function(item, index) {
                        firebase.database().ref('/users/' + item.user.localId).once('value').then(function(snapshot) {
                            return snapshot.val();
                        }).then(function(data) {
                            //console.log(data.premiumTwo);
                            if(data !== null) {
                                if(data.premiumTwo === true) {
                                    vm.cards.unshift(item);
                                } else {
                                    vm.cards.push(item);
                                }
                            } else {
                                vm.cards.push(item);
                            }
                            
                        });

                    })
            }

            function extendSearchDistance(options, optionalSportMatch, optionalSchoolMatch, matchDistance, results) {
                if (matchDistance <= 3000) {
                    MatchFactory.getNext10InStackForUser(options, optionalSportMatch, optionalSchoolMatch, matchDistance).then(function (data) {
                        console.log(data);
                        if (data.length == 0) {
                            matchDistance = matchDistance + 500;
                            extendSearchDistance(options, optionalSportMatch, optionalSchoolMatch, matchDistance, data);
                        } else {
                            checkMatchModalCondition();
                            vm.cards = data; // ---------------------------------------Add new users(cards)
                            if(vm.cards !== null) {
                                searchPremiunTwoUsers(vm.cards);
                            }
                            $ionicLoading.hide();
                        }
                    }, function (error) {
                        console.log(error);
                        matchDistance = matchDistance + 50;
                        extendSearchDistance(options, optionalSportMatch, optionalSchoolMatch, matchDistance, []);
                    })
                } else if (results.length == 0) {
                    checkMatchModalCondition();
                    $timeout(function () {
                        vm.errorStatus = "noUsers";
                    }, 0);
                    $ionicLoading.hide();
                }
            }

            /**
             * 
             * @param {string} options 
             * @param {boolean} optionalSportMatch 
             * @param {boolean} optionalSchoolMatch 
             */
            function getStack(options, optionalSportMatch, optionalSchoolMatch) {
                console.log(options);
                var hasResponse = false;
                var deferred = $q.defer();

                MatchFactory.getNext10InStackForUser(options, optionalSportMatch, optionalSchoolMatch, null)
                    .then(
                    function (results) {
                        hasResponse = true;
                        $ionicLoading.hide()

                        //handle no results. NO? This is done elsewhere? 10-11-17
                        // if (results.length == 0) {
                        //     $timeout(function () {
                        //         vm.errorStatus = "noUsers";
                        //     }, 0);
                        // }

                        vm.cards = results;
                        console.log('HHHHHHHHHHHHHHHHHHHHHHH');
                        console.log(results);
                        $timeout(function () {
                            deferred.resolve(results)
                        }, 0);

                        // $timeout(function() {
                        //     console.warn('TEMP')
                        //     vm.openProfile(vm.cards[0])
                        // }, 0);
                    },
                    function (err) {
                        console.log(err);
                    })
                    .catch(function (error) {
                        console.error(error);
                        $ionicLoading.hide()
                        hasResponse = true;
                        //ADD ERROR HANDLER HERE
                        vm.errorStatus = 'nextStackError'
                        $timeout(function () {
                            vm.helpText = error
                        }, 0);
                        deferred.reject('timeout')
                    })

                //handle no resposne from server
                $timeout(function () {
                    if (!hasResponse) {
                        $timeout(function () {
                            $ionicLoading.hide()
                            vm.errorStatus = "serverTimeout"
                            deferred.reject('timeout')
                        }, 0);
                    }
                }, 10000);

                return deferred.promise;

            }
        }

        function remove(index) {
            vm.cards.splice(index, 1);
            if (!vm.isMatched) {
                if (vm.cards.length <= 0) {
                    $timeout(function () {
                        getCards();
                    }, 0);
                }
            }
        }


        function reject(card) {
            // console.log('LEFT SWIPE');
            return MatchFactory.leftSwipe(card.user)

        }


        function like(card) {
            // console.log('RIGHT SWIPE');
            console.log(card)
            vm.isMatched = false;
            if (card.isMatch) {
                //match!
                console.log('match!')
                vm.isMatched = true;
                // showMatch(card)
            }
            return MatchFactory.rightSwipe(card.user, card.isMatch)
        }

        /**
         * 
         * @param {object} card the card object we've constructed
         */
        function showMatch(card) {
            vm.matchCard = card;
            vm.alertPopup = $ionicPopup.alert({
                cssClass: 'match-popup', // to remove title
                templateUrl: './views/playoff/match.popup.html',
                scope: $scope,
                buttons: []
            });

            vm.alertPopup.then(function (res) {
                // console.log('Thank you for not eating my delicious ice cream cone');
            });
            $scope.showMatchModalFlag = false;

        }

        function openProfile(card) {
            console.log(card);
            $scope.sliderOptions = {
                loop: false,
                effect: 'slide',
                speed: 500
            }
            $scope.modalCard = card;
            var aa = $scope.modalCard;
            var a = $scope.modalCard.user;

            $scope.modalCard.profileExpDescription = {};
            $scope.modalCard.profileExpDescription.question1 = $scope.modalCard.user.question1;
            $scope.modalCard.profileExpDescription.question2 = $scope.modalCard.user.question2;
            $scope.modalCard.profileExpDescription.question3 = $scope.modalCard.user.question3;
            $scope.modalCard.profileExpDescription.question4 = $scope.modalCard.user.question4;
            $scope.modalCard.reportUser = reportUser
            $scope.modalCard.userId = card.user.id;
            $scope.allPhotos = [];
            $scope.allPhotos[0] = $scope.modalCard.image;

            var user = $scope.modalCard.user;

            if (user.publicPhotoLink1) {
                $scope.allPhotos.push(user.publicPhotoLink1)
            }
            if (user.publicPhotoLink2) {
                $scope.allPhotos.push(user.publicPhotoLink2)
            }
            if (user.publicPhotoLink3) {
                $scope.allPhotos.push(user.publicPhotoLink3)
            }
            if (user.publicPhotoLink4) {
                $scope.allPhotos.push(user.publicPhotoLink4)
            }

            $scope.showUnmatchButton = false;
            vm.userImage = 4 * window.innerWidth / 3;
            console.log($scope.allPhotos);
            $ionicModal.fromTemplateUrl('./views/view-profile/view-profile.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
            // $scope.openModal = function() {
            // };
            $scope.closeModal = function () {
                $scope.modal.hide();
            };
            // Cleanup the modal when we're done with it!
            // $scope.$on('$destroy', function () {
            //     $scope.modal.remove();
            // });



            $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                // data.slider is the instance of Swiper
                $scope.slider = data.slider;
            });

            $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                // console.log('Slide change is beginning');
            });

            $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                // note: the indexes are 0-based
                $scope.activeIndex = data.slider.activeIndex;
                $scope.previousIndex = data.slider.previousIndex;
            });

            // return $state.go('view-profile', {
            //     user: {
            //         id: card.user.id,
            //         firstName: card.name,
            //         publicPhotoLink0: card.image
            //     }
            // })
        }

        function swipeUp(card) {
            // console.log('UP SWIPE ' + card.name);
            return MatchFactory.upSwipe(card.user)

        }


        function swipeLeft() {
            // console.log(angular.copy(vm.cards));            
            swipedOut.data.push(vm.cards[0]);
            console.log(TDCardDelegate.$getByHandle('vm.cards'));
            console.log(TDCardDelegate.$getByHandle('vm.cards').getFirstCard())
            TDCardDelegate.$getByHandle('vm.cards').getFirstCard().swipe('left');
            // $timeout(function(){
            //     console.log(angular.copy(vm.cards));
            // },1000)

        }


        function swipeRight() {
            // console.log(angular.copy(vm.cards));
            // swipedOut.data.push(vm.cards[0]);
            TDCardDelegate.$getByHandle('vm.cards').getFirstCard().swipe('right');
            // console.log(angular.copy(vm.cards));
        }

        /**
         * 
         * @param {object} card 
         */
        function goToMessageForNewMatch(card) {
            if (vm.alertPopup) {
                vm.alertPopup.close()
            }
            $state.go('chat', {
                matchData: {
                    id: card.user.id,
                    firstName: card.name,
                    publicPhotoLink0: card.image
                }
            })
        }

        function dismissMatchPopup() {
            if (vm.alertPopup) {
                vm.alertPopup.close()
                // if (vm.cards.length <= 0) {
                //     $timeout(function () {
                //         getCards();
                //     }, 0);
                MessagesFactory.getMatches().then(function (results) {
                    console.log(results);
                    if (vm.cards && vm.cards.length <= 0) {
                        getCards();
                    }
                })
                // }
            }
        }

        function goToMessages() {
            $ionicViewSwitcher.nextDirection("forward");
        }

        function revertOneCard() {
            console.log(swipedOut.data);
            console.log(angular.copy(vm.cards));

            // remove swipe from database
            MatchFactory.undoSwipe().then(function (data) {
                console.log(data);
                // if DB request is successful
                //Update the cards array adding last swiped card
                var revertedCard = swipedOut.data[swipedOut.data.length - 1];
                revertedCard.reverted = true;
                vm.cards.unshift(revertedCard);
                $scope.safeApply();

                //remove the reverted card from factory array
                swipedOut.data.splice(swipedOut.data.length - 1, 1);

                //variable for swipe back effect
                vm.swipedBackCard = true;
                // re-arrange the cards in order
                TDCardDelegate.$getByHandle('vm.cards').sort();

                // remove variable 
                $timeout(function () {
                    vm.swipedBackCard = false;
                }, 1000);
            }, function (error) {
                console.log(error);
                if (window.cordova) {
                    $cordovaToast.showShortBottom('Error reverting swipe. Please try again')
                } else {
                    console.log('Error reverting swipe. Please try again');
                }

            })

        }

        $scope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && (typeof (fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };

        /**
         * Undoes the previous swipe
         * 
         * 
         */
        function backtrack() {

             
            if (vm.isPremium) {
                //revertOneCard();
                if(!vm.premiumTwo) {// don't forget put !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    showPurchaseModal2();
                } else {
                    console.log('premiun true');
                }
            } else {
                // Show the user an option to choose subscription
                showPurchaseModal();
            //     //showPurchaseModal2();
            }

           
        }

        function showPurchaseModal() {
            var devHeight = $window.innerHeight;
            var devwidth = $window.innerWidth;

            $scope.card = {
                'height': 0.75 * devHeight + 'px'
            };
            $scope.fullHeight = {
                'height': devHeight + 'px'
            };
            $scope.sliderBox = {
                'width': devwidth - 40 - 20 + 'px'
            };
            $scope.slider = {
                'height': 0.6 * (0.75 * devHeight - 57 - 40) + 'px'
            };
            $scope.pricing = {
                'height': 0.35 * (0.75 * devHeight - 57 - 40) + 'px'
            };
            $scope.logo = {
                'height': 0.6 * (0.75 * devHeight - 57 - 40) - 70 - 15 + 'px'
            };
            $scope.conditions = {
                'height': 0.25 * (devHeight - 20 - 10) + 'px'
            };

            $ionicModal.fromTemplateUrl('./views/purchase/purchaseModal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });

            $scope.closePurchaseModal = function () {
                $scope.modal.hide();
                $timeout(function () {
                    //init();
                    if(!vm.premiumTwo) {// don't forget put !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        showPurchaseModal2();
                    } else {
                        console.log('premiun true');
                    }
                    $scope.modal.remove();
                }, 300);
            };
        }

        function showPurchaseModal2() {
            var devHeight = $window.innerHeight;
            var devwidth = $window.innerWidth;

            $scope.card = {
                'height': 0.75 * devHeight + 'px'
            };
            $scope.fullHeight = {
                'height': devHeight + 'px'
            };
            $scope.sliderBox = {
                'width': devwidth - 40 - 20 + 'px'
            };
            $scope.slider = {
                'height': 0.6 * (0.75 * devHeight - 57 - 40) + 'px'
            };
            $scope.pricing = {
                'height': 0.35 * (0.75 * devHeight - 57 - 40) + 'px'
            };
            $scope.logo = {
                'height': 0.6 * (0.75 * devHeight - 57 - 40) - 70 - 15 + 'px'
            };
            $scope.conditions = {
                'height': 0.25 * (devHeight - 20 - 10) + 'px'
            };

            if($ionicPlatform.is('android')) {
                pricePayment = 1.99;
                periodPayment = 'week';
            } else {
                pricePayment = 0.99;
                periodPayment = '24 hours';
            }

            $ionicModal.fromTemplateUrl('./views/purchase/purchaseModalNew.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
                $scope.pricePayment = pricePayment;
                $scope.periodPayment = periodPayment;
            });
            $scope.closePurchaseModal2 = function () {
                $scope.modal.hide();
                $timeout(function () {
                    vm.cards = [];
                    $localForage.setItem('welcomeMsgSeen', true);
                    vm.hideLoading();
                    init();
                    
                    
                    $scope.modal.remove();
                }, 300)

            };
        }

        $scope.purchase = function () {

            if (!window.cordova) {
                AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                    currentUser.set('premium', true);
                    currentUser.save();
                    vm.isPremium = true;
                    console.log("successfully subscribed");
                    $scope.closePurchaseModal();
                });
            }
            console.warn('Not implemented');
            InAppPurchaseService.getProducts().then(function (data) {
                InAppPurchaseService.buyProduct(StoreProductId).then(function (data) {
                    console.log('data');
                    console.log(data);
                    if (data) {
                        vm.isPremium = true;
                        console.log("successfully subscribed");
                        console.log(data);
                        $scope.closePurchaseModal();
                    } else {
                        if (window.cordova) {
                            $cordovaToast.showShortBottom('Subscription Failed');
                        } else {
                            alert('Subscription Failed');
                        }
                    }
                }, function (err) {
                    if (window.cordova) {
                        $cordovaToast.showShortBottom('Subscription Failed');
                    } else {
                        alert('Subscription Failed');
                    }
                });
            }, function (err) {

            });
        }

        $scope.purchaseTwo = function () {
            var currentUser1 = firebase.auth().currentUser;
            if (!window.cordova) {
                AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                    currentUser.save();
                    vm.premiumTwo = true;
                    console.log("successfully subscribed");
                    $scope.closePurchaseModal2();
                });
            }
            console.warn('Not implemented');
            InAppPurchaseService2.getProducts().then(function (data) {
                InAppPurchaseService2.buyProduct(StoreProductId2).then(function (data) {
                    console.log('data');
                    console.log(data);
                    if (data) {
                        vm.premiumTwo = true;
                        console.log("successfully subscribed");
                        console.log(data);
                        $scope.closePurchaseModal2();
                    } else {
                        if (window.cordova) {
                            $cordovaToast.showShortBottom('Subscription Failed');
                        } else {
                            alert('Subscription Failed');
                        }
                    }
                }, function (err) {
                    
                    if (window.cordova) {
                        $scope.closePurchaseModal2();
                        //$cordovaToast.showShortBottom('Subscription Failed');
                    } else {
                        alert('Subscription Failed');
                    }
                });
            }, function (err) {

            });
        }

        $scope.$on('push-match', function (e, data) {

            data = JSON.parse(data);
            $scope.pushCard = {
                image: data.payload.userPhoto,
                name: data.payload.userName,
                user: {
                    id: data.payload.userId
                }
            }
            $scope.showMatchModalFlag = true;
            console.log($scope.pushCard);
            if (!$scope.loadingDataFlag) {
                showMatch($scope.pushCard);
            }
        })
        function hideLoading() {
            console.log('hide loading');
            $ionicLoading.hide();
        }
        // Report User
        function reportUser(userIdToReport) {
            console.log('report user called');
            var confirmPopup = $ionicPopup.confirm({
                title: 'Report',
                template: 'Are you sure you want to report this user?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    return MatchFactory.reportUser(userIdToReport)
                        .then(
                        function (result) {
                            $scope.closeModal();
                            $timeout(function () {
                                swipeLeft();
                            }, 50);
                        })
                        .catch(function (error) {
                            console.error(error);
                        })
                }
            });
        }
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
})();