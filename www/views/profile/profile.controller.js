(function() {
    'use strict';

    angular
        .module('app.profile', [])
        .controller('ProfileCtrl', ProfileCtrl);

    ProfileCtrl.$inject = ['$state', 'parseUser', 'ionicDatePicker', 'AuthFactory', '$timeout'];

    function ProfileCtrl($state, parseUser, ionicDatePicker, AuthFactory, $timeout) {
        var vm = this;
        vm.saveButtonClick = saveButtonClick;
        vm.openBirthdayPicker = openBirthdayPicker;
        vm.primarySportsArray = ['Football', 'Soccer', 'Basketball', 'Baseball', 'Softball', 'Lacrosse', 'Track & Field', 'Volleyball', 'Hockey', 'Equestrian', 'Water Polo', 'Polo', 'Rugby', 'Swimming/Diving', 'Gymnastics', 'Winter Sports', 'Weight Lifting', 'Golf', 'Tennis', 'Rowing', 'Field Hockey', 'Cross Country'];
        vm.graduationYears = []

        //vars
        vm.graduationYear;
        vm.primarySport;
        vm.birthdayMoment;
        vm.firstName;
        vm.company;
        vm.jobTitle;
        vm.school;

        activate();

        ////////////////

        function activate() {
            generateGradYears()

            if (parseUser) {

            }
        }

        function generateGradYears() {
            for (var index = 1950; index < 2026; index++) {
                vm.graduationYears.push(index)
            }
        }

        function saveButtonClick() {
            if (parseUser) {
                //is an already logged in user, so go back to settings
                return $state.go('settings')
            } else {
                //we're still in setup
                return $state.go('setup-2')
            }
        }

        function openBirthdayPicker() {

            var ipObj1 = {
                callback: function(val) { //Mandatory
                    // console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    vm.birthdayMoment = moment(val)
                },
                from: new Date(1950, 0, 0), //Optional
                to: moment().subtract(18, 'years').toDate(), //Optional
                inputDate: new Date(1990, 0, 0), //Optional
                mondayFirst: false, //Optional
                closeOnSelect: true, //Optional
                // templateType: 'popup' //Optional or modal
            };

            ionicDatePicker.openDatePicker(ipObj1);
        }
    }
})();