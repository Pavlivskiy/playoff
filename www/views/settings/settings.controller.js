(function () {
    'use strict';

  angular
    .module('app.settings')  
    .controller('SettingsCtrl', ['$stateParams', 'swipedOut', '$window', '$rootScope', '$scope', '$timeout', 'TDCardDelegate', 'MatchFactory', 'LocationFactory', '$ionicLoading', '$ionicPopup', '$state', '$ionicModal', '$ionicViewSwitcher', 'ParseAsService', 'InAppPurchaseService', 'InAppPurchaseProducts', '$q', 'MessagesFactory', 'AuthFactory', '$localForage', '$ionicHistory', '$cordovaNetwork', '$ionicPlatform', '$cordovaToast', 'appVersionNumber', 'googleAutocompleteService',
    
    function SettingsCtrl($stateParams, swipedOut, $window, $rootScope, $scope, $timeout, TDCardDelegate, MatchFactory, LocationFactory, $ionicLoading, $ionicPopup, $state, $ionicModal, $ionicViewSwitcher, ParseAsService, InAppPurchaseService, InAppPurchaseProducts, $q, MessagesFactory, AuthFactory, $localForage, $ionicHistory, $cordovaNetwork, $ionicPlatform, $cordovaToast, appVersionNumber, googleAutocompleteService) {
        var vm = this;
        vm.selectedSchool = [];
        vm.removeFromSelectedSchool = removeFromSelectedSchool;
        vm.saveProfile = saveProfile;
        vm.openSchoolSelectModal = openSchoolSelectModal;
        vm.openLocationSelectModal = openLocationSelectModal;
        vm.setPremiumOptions = setPremiumOptions;
        vm.setMatchByLocation = setMatchByLocation;
        vm.logOut = logOut;
        vm.openProfile = openProfile;
        vm.resetCards = resetCards;
        vm.addTestSwipes = addTestSwipes;
        vm.upgrade = upgrade;
        vm.schoolArray = all_college_list;
        vm.primarySportsArray = [];
        vm.setSchool = setSchool;
        vm.setSport = setSport;
        vm.filterSchools = filterSchools;
        vm.showSchoolSearch = showSchoolSearch;
        vm.showingSchoolResults = false;
        vm.submitFeedback = submitFeedback;
        vm.deleteProfile = deleteProfile;
        vm.isAdmin = isAdmin;
        vm.friendsList = [];
        vm.role = '';
        vm.showingSchoolResults = false;
        vm.submitFeedback = submitFeedback;
        vm.deleteProfile = deleteProfile;
        vm.isAdmin = isAdmin;
        vm.friendsList = [];
        vm.role = '';
        vm.primarySportsList = [{
            id: 1,
            name: "Football",
            selected: false
        },
        {
            id: 2,
            name: "Soccer",
            selected: false
        },
        {
            id: 3,
            name: "Basketball",
            selected: false
        },
        {
            id: 4,
            name: "Baseball",
            selected: false
        },
        {
            id: 5,
            name: "Softball",
            selected: false
        },
        {
            id: 6,
            name: "Lacrosse",
            selected: false
        },
        {
            id: 7,
            name: "Track & Field",
            selected: false
        },
        {
            id: 8,
            name: "Volleyball",
            selected: false
        },
        {
            id: 9,
            name: "Hockey",
            selected: false
        },
        {
            id: 10,
            name: "Equestrian",
            selected: false
        },
        {
            id: 11,
            name: "Water Polo",
            selected: false
        },
        {
            id: 12,
            name: "Polo",
            selected: false
        },
        {
            id: 13,
            name: "Rugby",
            selected: false
        },
        {
            id: 14,
            name: "Swimming/Diving",
            selected: false
        },
        {
            id: 15,
            name: "Gymnastics",
            selected: false
        },
        {
            id: 16,
            name: "Winter Sports",
            selected: false
        },
        {
            id: 17,
            name: "Weight Lifting",
            selected: false
        },
        {
            id: 18,
            name: "Golf",
            selected: false
        },
        {
            id: 19,
            name: "Tennis",
            selected: false
        },
        {
            id: 20,
            name: "Rowing",
            selected: false
        },
        {
            id: 21,
            name: "Field Hockey",
            selected: false
        },
        {
            id: 22,
            name: "Cross Country",
            selected: false
        },
        ];
        vm.selectedSport = [];

        activate();

        $scope.$watch('vm.distanceMilesMax', function () {
            vm.colorLimit1 = ((100 * (vm.distanceMilesMax - 20)) / 130) - 1;
            vm.colorLimit2 = ((100 * (vm.distanceMilesMax - 20)) / 130);
        });

        ////////////////
        function setPremiumOptions() {
            AuthFactory.getCurrentUserDbEntry().then(function (user) {
                var options = '00';
                if (vm.matchBySport && vm.matchBySchool) {
                    options = '11';
                } else if (!vm.matchBySport && vm.matchBySchool) {
                    options = '01';
                } else if (vm.matchBySport && !vm.matchBySchool) {
                    options = '10';
                }
                $timeout(function () {

                }, 0);
                user.set('premiumOptions', options);
                user.save("", {
                    success: function (result) {

                    },
                    error: function (error) {

                    }
                });
            });
        }

        function setMatchByLocation() {
            AuthFactory.getCurrentUserDbEntry().then(function (user) {
                if (!vm.matchByLocation) {
                    vm.formatted_address = '';
                    user.set('searchLocationString', '')
                }
                user.set('matchByLocation', vm.matchByLocation)
                user.save(null, {
                    success: function (user) {
                        
                    },
                    error: function (error) {
                     
                    }
                });
            });
        }

        function upgrade() {
            $state.go('playoff', {
                action: 'openPremiumPopup'
            })
        }

        function resetCards() {
            MatchFactory.reset()
                .then(
                function (results) {
                    if (window.cordova) {
                        $cordovaToast.showShortBottom('Reset successful');
                    } else {
                        console.log('Reset successful')
                    }
                })
                .catch(function (error) {
                    if (window.cordova) {
                        $cordovaToast.showShortBottom('Error resetting data');
                    } else {
                        console.log('Error resetting data')
                    }
                })
        }

        function addTestSwipes() {
            Parse.Cloud.run('add15TestUsersFunc', {
                userId: Parse.User.current().id
            })
        }

        function activate() {

            fetchSportList();

            $timeout(function () {
                $ionicLoading.hide();
            }, 500)


            AuthFactory.getAllUserAttributes()
                .then(
                function (result) {
                    console.log(result);
                    vm.userEmail = result.userEmail;
                    vm.firstName = result.firstName;
                    vm.school = result.school;
                    vm.profilePicture = result.publicPhotoLink0;
                    vm.distanceMilesMax = result.distanceMilesMax;
                    vm.range = {};
                    vm.range.from = result.ageMin;
                    vm.range.to = result.ageMax;
                    vm.genderString = result.genderString;
                    vm.interestedInString = result.interestedInString;
                    vm.isPremium = result.premium;
                    vm.role = result.role;
                    vm.matchByLocation = result.matchByLocation;
                    vm.formatted_address = result.searchLocationString;
                    vm.matchBySchool = result.matchBySchool;
                    if (vm.matchBySchool) {
                        if (result.selectedSchool) {
                            vm.selectedSchool = result.selectedSchool;
                        }
                    }
                    vm.matchBySport = result.matchBySport;
                    if (vm.matchBySport) {
                        vm.selectedSport = result.selectedSport;
                        _.each(vm.primarySportsList, function (item, key) {
                            if (_.indexOf(vm.selectedSport, item.name) !== -1) {
                                item.selected = true;
                            }
                        });
                        console.log(vm.primarySportsList);
                    }

                    setPremiumOptions()

                    switch (result.premiumOptions) {
                        case '00':
                            vm.matchBySports = false;
                            vm.matchBySchool = false;
                            break;
                        case '01':
                            vm.matchBySports = false;
                            vm.matchBySchool = true;
                            break;
                        case '10':
                            vm.matchBySports = true;
                            vm.matchBySchool = false;
                            break;
                        case '11':
                            vm.matchBySports = true;
                            vm.matchBySchool = true;
                            break;
                    }
                })
                .catch(function (error) {
                    console.error(error);
                })

            LocationFactory.getCurrentLocation()
                .then(
                function (result) {
                    vm.locationString = result.locationString;
                })
                .catch(function (error) {
                    console.error(error);
                })

            MatchFactory.get50MostRecentMatchesForUser()
                .then(
                function (result) {
                    vm.matchesArray = result;
                })
                .catch(function (error) {
                    console.error(error);
                })

            AuthFactory.getFacebookFriends().then(function (data) {
                vm.friendsList = data.data;
                if (vm.friendsList.length) {
                    vm.friendsListText = vm.friendsList.length == 1 ? (vm.friendsList[0].name + ' is using this app') : (vm.friendsList.length == 2) ? (vm.friendsList[0].name + ' and ' + vm.friendsList[1].name + ' are using this app') : (vm.friendsList[0].name + ', ' + vm.friendsList[1].name + ' and ' + (vm.friendsList.length - 2) + ' more friends are using this app');
                }
            }, function (err) {
                console.log(err);
            })
        }

        function setSchool(school) {
            vm.showingSchoolResults = false;
            vm.showFilteredResults = false;
            vm.searchedSchool = '';
            vm.selectedSchool.push(school);
        }

        function removeFromSelectedSchool(school) {
            vm.selectedSchool = _.reject(vm.selectedSchool, function (item) {
                return item == school;
            })
        }

        function setSport(sport) {
            vm.showingSportResults = false;
            vm.showFilteredResultsSport = false;
            vm.sportSearch = false;
            vm.selectedSport = sport;
        }

        function filterSchools() {
            vm.showingSchoolResults = true;

            if (vm.searchedSchool.length > 2) {
                vm.showFilteredResults = true;
            } else {
                vm.showFilteredResults = false;
            }
        }

        function showSchoolSearch(status) {
            vm.schoolSearch = status;
        }

        function openSchoolSelectModal() {
            $ionicModal.fromTemplateUrl('./views/settings/school-select.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });

            $scope.closeSchoolModal = function () {
                $scope.modal.hide();
                $scope.modal.remove();
            };
        }
        //Open location modal
        function openLocationSelectModal() {
            $scope.search = {};
            $scope.search.suggestions = [];
            $ionicModal.fromTemplateUrl('./views/settings/select-location.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.locationModal = modal;
                $scope.locationModal.show();
            });

            $scope.closeLocationModal = function () {
                $timeout(function () {
                    vm.searchedLocation = '';
                    $scope.search.suggestions = [];
                }, 10);
                $scope.locationModal.hide();
                $scope.locationModal.remove();
            };

            $scope.$watch('vm.searchedLocation', function (newValue) {

                if (newValue) {

                    googleAutocompleteService.searchAddress(newValue, ['US', 'CA']).then(function (result) {
                        console.log(result);
                        $scope.search.error = null;
                        $scope.search.suggestions = result;
                    }, function (status) {
                        $scope.search.error = "There was an error :( " + status;
                    });
                }
            });
            function convertAddress(address) {
                var convertedArray = address.adr_address.split(', ');
                var city, state, country;
                _.each(convertedArray, function (item) {

                    if (item.includes('<span class="locality">')) {
                        // console.log('city')
                        city = item.substring('<span class="locality">'.length, item.indexOf('</span>'));
                    }

                    if (item.includes('<span class="region">')) {

                        state = item.substring('<span class="region">'.length, item.indexOf('</span>'));
                    }

                    if (item.includes('<span class="country-name">')) {

                        country = item.substring('<span class="country-name">'.length, item.indexOf('</span>'));
                    }
                })

                if (address.geometry.viewport) {
                    var longitude = (address.geometry.viewport.b.f + address.geometry.viewport.b.b) / 2;
                    var latitude = (address.geometry.viewport.f.b + address.geometry.viewport.f.f) / 2;
                }

                var addressObject = {
                    city: city,
                    state: state,
                    country: country,
                    longitude: longitude,
                    latitude: latitude
                }
                return addressObject;
            }
            $scope.choosePlace = function (place) {
                googleAutocompleteService.getDetails(place.place_id).then(function (location) {
                    if (location) {
                        console.log(location);
                        $scope.location = convertAddress(location);
                        vm.formatted_address = location.formatted_address;
                        var point = new Parse.GeoPoint($scope.location.latitude, $scope.location.longitude)
                        //save the point into parse
                        AuthFactory.getCurrentUserDbEntry().then(function (user) {
                            user.set('locationGeoPoint', point);
                            user.set('searchLocationString', location.formatted_address);

                            user.save(null, {
                                success: function (user) {
                                    console.log('location save successfully');
                                },
                                error: function (error) {
                                    console.log('error in location save', error);
                                }
                            });
                        });

                        $scope.closeLocationModal();
                    }
                });
            };
        }

        function saveProfile(goBack) {
            console.log(vm.primarySportsArray);
            var matchSportlist = [];
            _.each(vm.primarySportsArray, function (item, key) {
                if (item.selected) {
                    matchSportlist.push(item.name);
                }
            });
            vm.selectedSport = angular.copy(matchSportlist);
            console.log(vm.selectedSport);
            $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'
            });
            AuthFactory.getCurrentUserDbEntry().then(function (user) {
                user.set('distanceMilesMax', parseFloat(vm.distanceMilesMax))
                user.set('ageMin', parseFloat(vm.range.from))
                user.set('ageMax', parseFloat(vm.range.to))
                user.set('genderString', vm.genderString)
                user.set('interestedInString', vm.interestedInString)

                user.set('matchBySchool', vm.matchBySchool);

                if (vm.matchBySchool && vm.selectedSchool) {
                    user.set('selectedSchool', vm.selectedSchool)
                } else {
                    user.set('selectedSchool', null)
                }

                user.set('matchBySport', vm.matchBySport);

                if (vm.matchBySport && vm.selectedSport) {
                    user.set('selectedSport', vm.selectedSport)

                } else {
                    user.set('selectedSport', null)
                }

                //pasted from premium options function
                var options = '00';
                if (vm.matchBySport && vm.matchBySchool) {
                    options = '11';
                } else if (!vm.matchBySport && vm.matchBySchool) {
                    options = '01';
                } else if (vm.matchBySport && !vm.matchBySchool) {
                    options = '10';
                }
                user.set('premiumOptions', options);

                user.save(null, {
                    success: function (user) {
                        $ionicLoading.hide();
                        if (goBack) {
                            $state.go('playoff');
                        }
                    },
                    error: function (error) {
                        // An error occurred while saving one of the objects.
                        console.error(error);
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Hmmm',
                            template: 'We were unable to save your profile. Please try again later.',
                            okType: "button-balanced button-green"
                        });
                    }
                });
            });
        }

        function submitFeedback() {
            $scope.showEmailDisabled = vm.userEmail ? true : false;
            $scope.showEmailErr = false;
            var myPopup = $ionicPopup.show({
                template: `<input type="email" style="text-align: center" ng-model="vm.userEmail" placeholder="enter your email" ng-disabled="showEmailDisabled">
                    <div ng-if="showEmailErr" style="text-align: center; color:red">* Valid email is required</div>
                <input type="text" style="text-align: center" ng-model="vm.feedback" placeholder="Let us know what you think!">`,
                title: 'Feedback',
                scope: $scope,
                buttons: [{
                    text: 'Cancel'
                },
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (vm.userEmail) {
                            if (!vm.feedback) {
                                //don't allow the user to close unless he enters wifi password
                                // e.preventDefault();
                            } else {
                                return vm.feedback;
                            }
                        } else {
                            console.log('enter email');
                            $scope.showEmailErr = true;
                            e.preventDefault();
                        }
                    }
                }
                ]
            });

            myPopup.then(function (res) {
                // console.log('Tapped!', res);
                if (res) {
                    AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                        currentUser.set('userEmail', vm.userEmail);
                        currentUser.set('feedback', res);
                        currentUser.save().then(function (data) {

                                vm.feedback = '';
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Thanks!',
                                    template: 'We appreciate the feedback!'
                                });

                            }, function (err) {
                                console.log(err);
                            })
                    });
                }
            });
        }

        function logOut() {
            return AuthFactory.logOut()
                .then(
                function (result) {
                    return $state.go('signup-1')                        
                })
                .catch(function (error) {
                    console.error(error);
                })
        }

        function toggleProfanityFilter() {
            $rootScope.profanityFilterBool = vm.profanityFilter;
        }

        function openProfile(targetUserId) {

            $scope.sliderOptions = {
                loop: false,
                effect: 'slide',
                speed: 500
            }
            AuthFactory.getCurrentUserDbEntry().then(function (user) {

                //to make sure we have messages in case of a report
                MatchFactory.getUserProfileInfo(targetUserId, user.get("localId"))
                    .then(
                        function (result) {
                            console.log(result);
                            $scope.modalCard = result;
                            $scope.modalCard.name = result.firstName;
                            $scope.modalCard.description = result.school;
                            $scope.modalCard.image = result.publicPhotoLink0;
                            $scope.modalCard.primarySport = result.primarySport;
                            $scope.modalCard.distanceFrom = result.distanceFrom;
                            $scope.modalCard.age = result.age;
                            $scope.modalCard.reportUser = reportUser
                            $scope.modalCard.unmatchUser = unmatchUser
                            $scope.modalCard.userId = targetUserId;

                            $scope.modalCard.messages = vm.messages;

                            $scope.modalCard.settingsClick = true;

                            $scope.modalCard.openChat = openChat;

                            $scope.allPhotos = [];
                            $scope.allPhotos[0] = $scope.modalCard.image;

                            if (result.publicPhotoLink1) {
                                $scope.allPhotos.push(result.publicPhotoLink1)
                            }
                            if (result.publicPhotoLink2) {
                                $scope.allPhotos.push(result.publicPhotoLink2)
                            }
                            if (result.publicPhotoLink3) {
                                $scope.allPhotos.push(result.publicPhotoLink3)
                            }
                            if (result.publicPhotoLink4) {
                                $scope.allPhotos.push(result.publicPhotoLink4)
                            }

                            openModal()
                        })
                    .catch(function (error) {
                        console.error(error);
                    })

                function openChat(userId) {
                    var matchData = {
                        id: userId
                    }
                    $scope.closeModal()
                    $timeout(function () {
                        return $state.go('chat', {
                            matchData: matchData
                        })
                    }, 100);
                }

                function openModal() {
                    vm.userImage = 4 * window.innerWidth / 3;
                    $ionicModal.fromTemplateUrl('./views/view-profile/view-profile.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function (modal) {
                        $scope.modal = modal;
                        $scope.modal.show();
                    });
                    // $scope.openModal = function() {
                    // };
                    $scope.closeModal = function () {
                        $scope.modal.hide();
                    };
                    // Cleanup the modal when we're done with it!
                    $scope.$on('$destroy', function () {
                        $scope.modal.remove();
                    });
                    // Execute action on hide modal
                    $scope.$on('modal.hidden', function () {
                        // Execute action
                    });
                    // Execute action on remove modal
                    $scope.$on('modal.removed', function () {
                        // Execute action
                    });



                    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                        // data.slider is the instance of Swiper
                        $scope.slider = data.slider;
                    });

                    $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                        // console.log('Slide change is beginning');
                    });

                    $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                        // note: the indexes are 0-based
                        $scope.activeIndex = data.slider.activeIndex;
                        $scope.previousIndex = data.slider.previousIndex;
                    });

                    // return $state.go('view-profile', {
                    //     user: {
                    //         id: card.user.id,
                    //         firstName: card.name,
                    //         publicPhotoLink0: card.image
                    //     }
                    // })    
                }
            });
        }

        /**
         * 
         * @param {string} userIdToUnmatch
         */
        function unmatchUser(userIdToUnmatch) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Unmatch',
                template: 'Are you sure you want to unmatch this user?'
            });

            confirmPopup.then(function (res) { // eslint-disable-line
                if (res) {
                    return MatchFactory.unmatchUser(userIdToUnmatch)
                        .then(
                        function (result) {
                            $rootScope.$emit('refreshMessagesList')

                            return $state.go('messages')
                        })
                        .catch(function (error) {
                            console.error(error);
                        })
                }
            });
        }

        /**
         * 
         * @param {string} userIdToReport
         * @param {array} messages comes from vm.messages actually indirectly through the scope
         * NOTE: This also unmatches the users
         */
        function reportUser(userIdToReport, messages) {

            var confirmPopup = $ionicPopup.confirm({
                title: 'Report',
                template: 'Are you sure you want to report this user? This will unmatch you.'
            });

            confirmPopup.then(function (res) { // eslint-disable-line
                if (res) {
                    return MatchFactory.reportAndUnmatchUser(userIdToReport, messages)
                        .then(
                        function (result) {
                            $rootScope.$emit('refreshMessagesList')
                            return $state.go('messages')
                        })
                        .catch(function (error) {
                            console.error(error);
                        })
                }
            });
        }

        // Delete Profile 
        function deleteProfile() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Delete Profile',
                template: 'Are you sure you want to delete profile? This cannot be undone.'
            });

            confirmPopup.then(function (res) { // eslint-disable-line
                if (res) {
                    $ionicLoading.show('Deleting profile');
                    console.log('profile deleted');
                    AuthFactory.deleteProfile().then(function (success) {
                        AuthFactory.logOutAndReturnToWelcomeScreen();
                        $localForage.clear();
                        $ionicLoading.hide();
                    }, function (err) {
                        console.log('error in profile deleting');
                        $ionicLoading.hide();
                    });

                }
            });
        }

        function isAdmin() {
            var user = Parse.User.current()
            if (user.attributes.authData.facebook.id == '10102709346594644' || user.attributes.authData.facebook.id == '10209813334996284' || user.attributes.authData.facebook.id == '10154616206980776') {
                return true;
            }
            return false;
        }

        function fetchSportList() {
            vm.primarySportsArray = [];
            var query = new Parse.Query("SportList");
            query.limit(999)
            query.find({
                success: function (data) {
                    console.log(data);
                    _.each(data, function (sport, index) {
                        console.log(sport.toJSON().name);

                        vm.primarySportsArray.push(
                            {
                                id: index,
                                name: sport.toJSON().name,
                                selected: false
                            }
                        );
                    })
                },
                error: function (err) {
                    console.log(err)
                    $cordovaToast.showShortBottom('Error in getting sportlist. Please check your internet connection!')
                }
            });
        }
    }
]);

      
})();

