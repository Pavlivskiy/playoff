(function () {
    'use strict';

    angular
        .module('app.setup', [])
        .controller('SetupProfile1Ctrl', SetupProfile1Ctrl);

    SetupProfile1Ctrl.$inject = ['AuthFactory', 'ionicDatePicker', 'moment', '$timeout', '$state', 'parseUser', 'parseUserAttributes', '$ionicViewSwitcher', 'isFromEditScreen', '$ionicLoading', 'focus', '$scope', '$ionicModal', '$cordovaToast'];

    function SetupProfile1Ctrl( AuthFactory, ionicDatePicker, moment, $timeout, $state, parseUser, parseUserAttributes, $ionicViewSwitcher, isFromEditScreen, $ionicLoading, focus, $scope, $ionicModal, $cordovaToast) {

        var vm = this;
        vm.primarySportsArray = [];
        vm.schoolArray = all_college_list;
        vm.graduationYears = [];
        vm.openBirthdayPicker = openBirthdayPicker;
        vm.continueSetup = continueSetup;
        vm.invalidProfile = invalidProfile;
        vm.moveToSecondHeight = moveToSecondHeight;
        vm.errorText = undefined;

        // console.log( JSON.stringify(vm.schoolArray));
        //profile vars
        vm.graduationYear;
        vm.primarySport;
        vm.birthdayMoment;
        vm.firstName;
        vm.lastName;
        vm.userEmail;
        vm.company;
        vm.jobTitle;
        vm.school;
        vm.heightFeet;
        vm.heightInches

        vm.parseUser = parseUser;
        vm.showSchoolSearch = showSchoolSearch;
        vm.isFromEditScreen = isFromEditScreen;
        vm.filterSchools = filterSchools;
        vm.setSchool = setSchool;
        vm.previewProfile = previewProfile;
        vm.fromEdit = false;

        // hide splash screen
        document.addEventListener("deviceready", onDeviceReady, false);
        
            function onDeviceReady() {
                navigator.splashscreen.hide();
            }

        activate()

        function setSchool(school) {
            vm.showFilteredResults = false;
            vm.schoolSearch = false;
            vm.school = school
        }

        function filterSchools() {
            if (vm.school.length > 2) {
                vm.showFilteredResults = true;
            } else {
                vm.showFilteredResults = false;
            }
        }

        function showSchoolSearch(status) {
            vm.schoolSearch = status;
        }
        function fetchSportList() {
            vm.primarySportsArray = [];
            // var query = new Parse.Query("SportList");
            // query.limit(999)
            // query.find({
            //     success: function(data) {
            //         console.log(data)
            //         _.each(data, function(sport) {
            //             vm.primarySportsArray.push(sport.toJSON().name);
            //         })
            //     },
            //     error: function(err) {
            //         console.log(err)
            //         $cordovaToast.showShortBottom('Error in getting sportlist. Please check your internet connection!')
            //     }
            // });
                       
            var ref = firebase.database().ref("sportlist");
            ref.on('value', function (snapshot) {
                var arrayResults = snapshot.val();
                if(arrayResults){
                    _.each(arrayResults, function (sport) {
                        vm.primarySportsArray.push(sport.name);
                    })
                }
                else{
                    console.log(err)
                    $cordovaToast.showShortBottom('Error in getting sportlist. Please check your internet connection!')
                }
            });
        }

        function activate() {
            // $ionicLoading.show();
            generateGradYears();
            fetchSportList();

            console.log("$state", $state.current.name)
            if ($state.current.name !== 'profile-edit') {
                setupName();
            }

            // setupBirthday();
            // setupWork();
            // setupEducation();
            else {
                $timeout(function(){
                    vm.fromEdit = true;
                },20);
                AuthFactory.getAllUserAttributes()
                    .then(
                    function (result) {
                        if (result.firstName) {
                            vm.firstName = result.firstName
                        }
                        if (result.lastName) {
                            vm.lastName = result.lastName
                        }
                        if (result.userEmail) {
                            vm.userEmail = result.userEmail
                        }
                        if (result.heightInches) {
                            vm.heightInches = result.heightInches
                        }
                        if (result.heightFeet) {
                            vm.heightFeet = result.heightFeet
                        }
                        if (result.graduationYear) {
                            vm.graduationYear = result.graduationYear
                        }
                        if (result.primarySport) {
                            vm.primarySport = result.primarySport
                        }
                        if (result.school) {
                            vm.school = result.school
                        }
                        if (result.jobTitle) {
                            vm.jobTitle = result.jobTitle
                        }
                        if (result.company) {
                            vm.company = result.company
                        }

                        if (result.birthdayMMDDYYYY && moment(result.birthdayMMDDYYYY, 'MM/DD/YYYY').isValid()) {
                            var bdayMoment = moment(result.birthdayMMDDYYYY, 'MM/DD/YYYY');
                            vm.birthdayMoment = bdayMoment;

                        } else {
                            vm.birthdayMoment = moment().subtract(18, 'years')

                        }
                        $ionicLoading.hide();

                        //for the first load, set up details from facebook
                        if (!isFromEditScreen) {
                            setupName()
                        }
                    })
                    .catch(function (error) {

                        $ionicLoading.hide();
                        console.error(error);
                    })
            }
            //additional setup if we came from settings menu
            if (parseUser) {
                //came from settings
            }


        }

        function generateGradYears() {
            for (var index = 1950; index < 2026; index++) {
                vm.graduationYears.push(index)
            }
        }

        function openBirthdayPicker() {

            var ipObj1 = {
                callback: function (val) { //Mandatory
                    // console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    vm.birthdayMoment = moment(val)
                },
                from: new Date(1950, 0, 0), //Optional
                to: moment().subtract(18, 'years').toDate(), //Optional
                inputDate: new Date(1990, 0, 0), //Optional
                mondayFirst: false, //Optional
                closeOnSelect: false //Optional
                // templateType: 'popup' //Optional or modal
            };

            ionicDatePicker.openDatePicker(ipObj1);
        }

        function setupName() {

            //first name setup
            $timeout(function () {
                AuthFactory.getFacebookFirstName()
                    .then(
                    function (result) {
                        console.log(result);
                        if (!vm.firstName && result.first_name) {
                            vm.firstName = result.first_name;
                        }
                        if (!vm.birthdayMoment && result.birthday) {
                            vm.birthdayMoment = result.birthday;
                        }
                        if (result.company) {
                            vm.company = result.company;
                        }
                        if (result.jobTitle) {
                            vm.jobTitle = result.jobTitle;
                        }
                        // if (result.school) {
                        //     vm.school = result.school;
                        // }
                        // if(result.email) {
                        //     vm.userEmail = result.email;
                        // }
                        else{
                            //this is phone number login case
                            vm.userEmail = "";
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                        $timeout(function () {
                            // setupName()
                        }, 1000);
                    })
            }, 1000);
        }

        // function setupBirthday() {
        //     //birthday setup
        //     $timeout(function() {
        //         AuthFactory.getBirthdayMoment()
        //             .then(
        //                 function(result) {

        //                     vm.birthdayMoment = result;
        //                 })
        //             .catch(function(error) {
        //                 console.error(error);
        //                 $timeout(function() {
        //                     // setupName()
        //                 }, 1000);
        //             })
        //     }, 1000);
        // }

        // function setupWork() {
        //     //work setup
        //     $timeout(function() {
        //         AuthFactory.getFacebookMostRecentCompanyAndJobTitle()
        //             .then(
        //                 function(resultObject) {

        //                     vm.company = resultObject.company;
        //                     vm.jobTitle = resultObject.jobTitle;
        //                 })
        //             .catch(function(error) {
        //                 console.error(error);
        //                 $timeout(function() {
        //                     setupName()
        //                 }, 1000);
        //             })
        //     }, 1000);
        // }

        // function setupEducation() {
        //     //work setup
        //     $timeout(function() {
        //         AuthFactory.getFacebookMostRecentEducation()
        //             .then(
        //                 function(resultObject) {

        //                     vm.school = resultObject.school;
        //                     // vm.jobTitle = resultObject.jobTitle;
        //                 })
        //             .catch(function(error) {
        //                 console.error(error);
        //                 $timeout(function() {
        //                     setupName()
        //                 }, 1000);
        //             })
        //     }, 1000);
        // }

        function continueSetup() {
            $ionicLoading.show();
            $timeout(function () {
                vm.submitted = true;
            }, 0);

            if (invalidProfile()) {
                $ionicLoading.hide();
                return;
            }
            //save the profile info
            // wip,
            // continue setup here
            return AuthFactory.saveProfilePage1(vm.firstName, vm.lastName, vm.birthdayMoment, vm.primarySport, vm.school, vm.graduationYear, vm.heightFeet, vm.heightInches, vm.company, vm.jobTitle, vm.userEmail)
                .then(
                function (result) {
                    if (vm.isFromEditScreen) {
                        $ionicViewSwitcher.nextDirection('back');
                        $ionicLoading.hide();
                        return $state.go('settings')
                    }
                    $ionicLoading.hide();
                    return $state.go('setup-2')
                })
                .catch(function (error) {
                    $ionicLoading.hide();
                    console.error(error);
                })

        }

        function moveToSecondHeight() {
            // $timeout(function() {
            //     if (vm.heightFeet && String(vm.heightFeet).length >= 1) {
            //         // .trigger('focus');
            //         var elem = angular.element('#focus2')
            //         elem.trigger('focus');
            //     }
            // }, 50);
            focus('focusMe');
        }

        //custon validator for this "form"
        function invalidProfile() {
            if (!vm.firstName) {
                $timeout(function () {
                    vm.errorText = "Please ensure that the first name is filled out!";
                }, 0);
                return true;
            }
            if (vm.firstName.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure that the first name is filled out!";
                }, 0);
                return true;
            }
            if (!vm.lastName || vm.lastName.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure that the last name is filled out!";
                }, 0);
                return true;
            }
            if (!vm.userEmail || vm.userEmail.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure that the last email is filled out!";
                }, 0);
                return true;
            }
            if (!vm.birthdayMoment) {
                $timeout(function () {
                    vm.errorText = "Please ensure you selected your birthday!";
                }, 0);
                return true;
            }
            if (!vm.primarySport || vm.primarySport.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure you selected your primary sport!";
                }, 0);
                return true;
            }
            if (!vm.school || vm.school.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure you wrote in your school!";
                }, 0);
                return true;
            }
            // if (!vm.work || vm.work.length == 0) {
            //     $timeout(function() {
            //         vm.errorText = "Please ensure you wrote in your work!";
            //     }, 0);
            //     return true;
            // }
            if (!vm.graduationYear || vm.graduationYear.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure you chose a graduation year!";
                }, 0);
                return true;
            }
            if (vm.heightFeet == undefined || vm.heightFeet.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure you filled in your height (feet)!";
                }, 0);
                return true;
            }
            if (vm.heightInches === undefined || vm.heightInches.length == 0) {
                $timeout(function () {
                    vm.errorText = "Please ensure you filled in your height (inches)!";
                }, 0);
                return true;
            }
            return false;
        }
        // previewProfile function to show self profile card.
        function previewProfile() {

            console.log('previewProfile');
            vm.userImage = 4 * window.innerWidth / 3;
            // var userDetails = Parse.User.current().toJSON();
            AuthFactory.getCurrentUserDbEntry().then(function (userDetails) {
                $scope.modalCard = userDetails;
                console.log($scope.modalCard);
                $scope.modalCard.currentUser = true;
                $scope.modalCard.name = userDetails.get('firstName');
                $scope.modalCard.description = userDetails.get('school');
                $scope.modalCard.image = userDetails.get('publicPhotoLink0');
                $scope.modalCard.primarySport = userDetails.get('primarySport');
                // $scope.modalCard.distanceFrom = userDetails.distanceFrom;
                $scope.modalCard.age = userDetails.get('age');
                // $scope.modalCard.reportUser = reportUser
                // $scope.modalCard.unmatchUser = unmatchUser
                //$scope.modalCard.userId = targetUserId;

                // $scope.modalCard.messages = vm.messages;

                $scope.modalCard.settingsClick = true;

                //$scope.modalCard.openChat = openChat;

                $scope.allPhotos = [];
                $scope.allPhotos[0] = $scope.modalCard.image;

                if (userDetails.get('publicPhotoLink1')) {
                    $scope.allPhotos.push(userDetails.get('publicPhotoLink1'))
                }
                if (userDetails.get('publicPhotoLink2')) {
                    $scope.allPhotos.push(userDetails.get('publicPhotoLink2'))
                }
                if (userDetails.get('publicPhotoLink3')) {
                    $scope.allPhotos.push(userDetails.get('publicPhotoLink3'))
                }
                if (userDetails.get('publicPhotoLink4')) {
                    $scope.allPhotos.push(userDetails.get('publicPhotoLink4'))
                }
                $ionicModal.fromTemplateUrl('./views/view-profile/view-profile.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
                // $scope.openModal = function() {
                // };
                $scope.closeModal = function () {
                    $scope.modal.hide();
                };
                $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                    // data.slider is the instance of Swiper
                    $scope.slider = data.slider;
                });

                $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                    // console.log('Slide change is beginning');
                });

                $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                    // note: the indexes are 0-based
                    $scope.activeIndex = data.slider.activeIndex;
                    $scope.previousIndex = data.slider.previousIndex;
                });
            });

        }

    }
})();