(function () {
    'use strict';

    angular
        .module('app.setup')
        .controller('SetupProfile2Ctrl', SetupProfile2Ctrl);

    SetupProfile2Ctrl.$inject = ['$window', '$rootScope', 'AuthFactory', '$q', '$timeout', '$state', '$ionicActionSheet', 'CameraService', '$ionicLoading', '$cordovaToast', '$scope', '$ionicModal', 'isFromEditScreen'];

    function SetupProfile2Ctrl($window, $rootScope, AuthFactory, $q, $timeout, $state, $ionicActionSheet, CameraService, $ionicLoading, $cordovaToast, $scope, $ionicModal, isFromEditScreen) {
        var vm = this;
        // $ionicLoading.show();
        $scope.parentPhotoHeight = {
            'height': (window.innerHeight - 88) + 'px'
        };
        vm.photoWidth = ($window.innerWidth - 30) / 2;
        vm.photoHeight = 1.33 * vm.photoWidth;
        $scope.gridsterOpts = {
            columns: 2, // the width of the grid, in columns
            pushing: false, // whether to push other items out of the way on move or resize
            floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: vm.photoHeight + 10, // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 10], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            sparse: false, // "true" can increase performance of dragging and resizing for big grid (e.g. 20x50)
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 100, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: false, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 2, // the minimum height of the grid, in rows
            maxRows: 2,
            defaultSizeX: 1, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: 1, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: 1, // maximum row height of an item
            resizable: {
                enabled: false,
                handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
                start: function (event, $element, widget) { }, // optional callback fired when resize is started,
                resize: function (event, $element, widget) { }, // optional callback fired when item is resized,
                stop: function (event, $element, widget) { } // optional callback fired when item is finished resizing
            },
            draggable: {
                enabled: false,
                stop: function (event, $element, widget) {
                    console.log($scope.standardItems)
                } // optional callback fired when item is finished dragging
            }
        };
        $scope.standardItems = [{
            sizeX: 1,
            sizeY: 1,
            row: 0,
            col: 0,
            index: 0
        },
        {
            sizeX: 1,
            sizeY: 1,
            row: 0,
            col: 1,
            index: 1
        },
        {
            sizeX: 1,
            sizeY: 1,
            row: 1,
            col: 0,
            index: 2
        },
        {
            sizeX: 1,
            sizeY: 1,
            row: 1,
            col: 1,
            index: 3
        }
        ];
        // Hide Splash screen.
        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            navigator.splashscreen.hide();
        }
        $scope.$on('gridster-item-resized', function (item) {
            console.log(item);
            // item.$element
            // item.gridster
            // item.row
            // item.col
            // item.sizeX
            // item.sizeY
            // item.minSizeX
            // item.minSizeY
            // item.maxSizeX
            // item.maxSizeY
        })

        vm.continueSetup = continueSetup;
        vm.lockDrag = lockDrag;
        vm.cropProfileImage = cropProfileImage;
        vm.openActionSheet = openActionSheet;
        vm.pickFromCamera = pickFromCamera;        
        vm.dragLocked = true;

        vm.profileImg = undefined; //purposely undefined at first for placeholder
        vm.publicImagesArray = ['img/camera2.png', 'img/camera2.png', 'img/camera2.png', 'img/camera2.png'];


        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            if (!$rootScope.android) {
                cordova.plugins.diagnostic.requestCameraRollAuthorization(function () { }, function () {
                    $cordovaToast.showShortBottom('Gallery permission denied');
                });
            }
        }

        activate();

        ////////////////

        function activate() {
            setupProfilePicture();
        }

        function lockDrag(status) {
            if (status) {
                $scope.gridsterOpts.draggable.enabled = false;
                vm.dragLocked = true;

            } else {

                $scope.gridsterOpts.draggable.enabled = true;
                vm.dragLocked = false;
            }
        }

        function processImageArray(array) {
            var finalArray = ['', '', '', ''];
            _.each($scope.standardItems, function (item, index) {
                if (item.row == 0 && item.col == 0) {
                    finalArray[index] = array[0];
                } else if (item.row == 0 && item.col == 1) {
                    finalArray[index] = array[1];
                } else if (item.row == 1 && item.col == 0) {
                    finalArray[index] = array[2];
                } else if (item.row == 1 && item.col == 1) {
                    finalArray[index] = array[3];
                }
            })

            return finalArray;
        }

        function continueSetup() {

            //todo: add checker if profile pics exist... (issue #8)

            //it's okay if images 2-4 are undefined here. The function checks for it

            if (window.cordova) {
                if ((vm.publicImagesArray[0] != 'img/camera2.png' && vm.publicImagesArray[1] != 'img/camera2.png') || (vm.publicImagesArray[0] != 'img/camera2.png' && vm.publicImagesArray[2] != 'img/camera2.png') || (vm.publicImagesArray[0] != 'img/camera2.png' && vm.publicImagesArray[3] != 'img/camera2.png') || (vm.publicImagesArray[1] != 'img/camera2.png' && vm.publicImagesArray[2] != 'img/camera2.png') || (vm.publicImagesArray[1] != 'img/camera2.png' && vm.publicImagesArray[3] != 'img/camera2.png') || (vm.publicImagesArray[2] != 'img/camera2.png' && vm.publicImagesArray[3] != 'img/camera2.png')) {
                    var imageArray = processImageArray(vm.publicImagesArray);
                    console.log(imageArray);
                    return AuthFactory.saveProfilePage2Images(vm.profileImg, imageArray[0], imageArray[1] || "", imageArray[2] || "", imageArray[3] || "")

                        .then(
                        function (result) {
                            $ionicLoading.hide();
                            if (isFromEditScreen) {
                                return $state.go('profile-edit')
                            } else {
                                return $state.go('setup-3');
                            }

                        })
                        .catch(function (error) {
                            $ionicLoading.hide();
                            console.error(error);
                            return $q.reject(error);
                        })
                } else {
                    $ionicLoading.hide();
                    if (window.cordova) {
                        $cordovaToast.showShortBottom('Select at least 2 images');
                    } else {
                        console.log('Select at least 2 images');
                    }
                }
            } else {
                console.log('Skipping image count check');
                var imageArray = processImageArray(vm.publicImagesArray);
                console.log(imageArray);
                return AuthFactory.saveProfilePage2Images(vm.profileImg, imageArray[0], imageArray[1] || "", imageArray[2] || "", imageArray[3] || "")
                    .then(
                    function (result) {
                        $ionicLoading.hide();
                        if (isFromEditScreen) {
                            return $state.go('profile-edit')
                        } else {
                            return $state.go('setup-3');
                        }

                    })
                    .catch(function (error) {
                        $ionicLoading.hide();
                        console.error(error);
                        return $q.reject(error);
                    })
            }
        }

        function setupProfilePicture() {
            return $timeout(function () {
                return AuthFactory.getFacebookProfilePicture()
                    .then(
                    function (result) {
                        setupFirstFourPhotos();
                        return $timeout(function () {
                            vm.profileImg = result;
                        }, 0);
                    })
                    .catch(function (error) {
                        console.error(error);
                        $ionicLoading.hide();
                        return $q.reject(error);
                    })
            }, 10);
        }

        function cropImageFun(base64Image, photoIndex, url) {
            if (url) {
                $scope.parentImage = base64Image;
            } else {
                $scope.parentImage = 'data:image/jpg;base64,' + base64Image;
            }
            vm.croppedImage = '';
            $scope.imageArray = [];
            // open a modal to crop image
            $ionicModal.fromTemplateUrl('./views/setup-profile/setup-2/crop-image.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });

            $scope.closeModal = function () {
                $scope.modal.hide();
                $scope.modal.remove();
            };

            $scope.cropPicture = function () {
                // console.log(vm.croppedImage);
                $scope.modal.hide();
                CameraService.processCroppedPicture(vm.croppedImage, photoIndex).then(function (resultUrl) {
                    $scope.modal.remove();

                    if (photoIndex >= 0) {
                        vm.publicImagesArray[photoIndex] = resultUrl;
                    } else {
                        vm.profileImg = resultUrl;
                    }
                    $timeout(function () {
                        console.log(vm.publicImagesArray[photoIndex]);
                    }, 100)

                })
            };
        }

        function openPhotoEditorSDK(base64Image, photoIndex, byUrl) {
            var ReactUI = PhotoEditorSDK.UI.ReactUI;
            var _PhotoEditorSDK = PhotoEditorSDK;
            var Promise = _PhotoEditorSDK.Promise;
            var editor;
            var myImage;
            var editorPromise;

            function onLoad() {
                var defer = $q.defer();
                // maxWindow();
                var w = window.innerWidth;
                var h = window.innerHeight;

                var div = document.createElement("div");
                div.id = "editor";

                document.getElementById("main").appendChild(div);

                document.getElementById("editor").style.height = h + "px";
                document.getElementById("editor").style.width = w + "px";
                document.getElementById("editor").style.zIndex = 500;
                var container = document.getElementById('editor');
                editor = new ReactUI({
                    container: container,
                    title: '',
                    showCloseButton: true,
                    // logLevel: 'info',
                    editor: {
                        image: myImage,
                        //preferredRenderer: 'canvas' works in ios, preferredRenderer: 'webgl' works in android,
                        preferredRenderer: 'canvas',
                        // responsive: true,
                        tools: ['crop', 'filter'],
                        controlsOrder: ['crop', 'filter'],
                        export: {
                            showButton: true,
                            download: false,
                            type: PhotoEditorSDK.RenderType.DATAURL
                            // type: PhotoEditorSDK.ImageFormat.IMAGE
                        }
                    },
                    assets: {
                        // baseUrl: 'file:///android_asset/www/sdkassets' // <-- This should be the absolute path to your `cassets` directory
                        baseUrl: 'sdkassets'
                    }
                });
                editorPromise = defer.promise;

                editor.on('export', function (dataURL) {
                    console.log(dataURL);
                    var url = "/my-upload-handler.php";
                    var data = {
                        image: dataURL
                    };
                    defer.resolve(dataURL);

                    var element = document.getElementById("editor");
                    element.parentNode.removeChild(element);


                    // urlData.link=dataURL;
                });
                editor.on('close', function (close) {
                    console.log(close);
                    var element = document.getElementById("editor");
                    element.parentNode.removeChild(element);
                })
                editorPromise
                    .then(function (success) {
                        // console.log(success);
                        CameraService.processCroppedPicture(success, photoIndex).then(function (resultUrl) {
                            if (photoIndex >= 0) {
                                vm.publicImagesArray[photoIndex] = resultUrl;
                            } else {
                                vm.profileImg = resultUrl;
                            }
                            $timeout(function () {
                                console.log(vm.publicImagesArray[photoIndex]);
                            }, 100)

                        })
                    })
            }
            myImage = new Image();
            myImage.addEventListener('load', function () {
                // $scope.openModal();
                // $state.go('tab.account');
                onLoad();
            });
            if (byUrl) {
                myImage.src = base64Image;
            } else {
                base64Image = "data:image/jpeg;base64," + base64Image;
                myImage.src = base64Image;
            }
        }

        function pickFromGallery(photoIndex) {
            if (!$rootScope.android) {
                cordova.plugins.diagnostic.isCameraRollAuthorized(function (authorized) {
                    if (authorized) {
                        //camera roll authorized
                        CameraService.pickPicture().then(function (result) {
                            //console.log(result);

                            openPhotoEditorSDK(result, photoIndex);
                            //cropImageFun(result, photoIndex, false);
                            // $timeout(function () {
                            //     vm.publicImagesArray[photoIndex] = result;
                            // }, 10)
                        })
                    } else {
                        //camera roll permission not authorized
                        openPhoneSettingPage();
                    }
                }, function (error) {
                    console.error("The following error occurred: " + error);
                });
            } else {
                CameraService.checkCameraPermission().then(function (data) {
                    if (data) {
                        //camera/camera roll authorized
                        CameraService.pickPicture().then(function (result) {
                            // console.log(result);
                            openPhotoEditorSDK(result, photoIndex);
                            //cropImageFun(result, photoIndex, false);
                            // $timeout(function () {
                            //     vm.publicImagesArray[photoIndex] = result;
                            // }, 10)
                        })
                    }
                }, function (err) {
                    console.log(err);
                })
            }
        }

        function pickFromCamera(photoIndex) {
            CameraService.checkCameraPermission().then(function (data) {
                if (data) {
                    //camera authorized
                    CameraService.takePicture().then(function (base64Image) {
                        // console.log(base64Image);
                        openPhotoEditorSDK(base64Image, photoIndex);
                        //cropImageFun(base64Image, photoIndex, false);
                    })
                }
            }, function (err) {
                console.log(err);

            })
        }

        function openPhoneSettingPage() {

            function onConfirm(buttonIndex) {
                if (buttonIndex == 1) {

                    cordova.plugins.diagnostic.switchToSettings(function () {
                        console.log("Successfully switched to Settings app");
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }
            }

            navigator.notification.alert(
                'We need to access phone\'s camera roll in order to upload pictures', // message
                onConfirm, // callback to invoke with index of button pressed
                'Camera Roll required', // title
                'Open Settings' // buttonLabels
            );

        }

        function cropProfileImage() {
            openPhotoEditorSDK(vm.profileImg, -1, true)
            //cropImageFun(vm.profileImg, -1, true);
        }

        function deletePhotosAsPerIndex(photoIndex) {
            vm.publicImagesArray[photoIndex] = 'img/camera2.png';

            //Save all images first to back-end before deletion, so the order changed by dragging etc remains intact
            console.log('Skipping image count check');
            var imageArray = processImageArray(vm.publicImagesArray);
            console.log(imageArray);
            return AuthFactory.saveProfilePage2Images(vm.profileImg, imageArray[0], imageArray[1], imageArray[2], imageArray[3])
                .then(
                function (result) {
                    // delete the photo from back-end -> Send empty string
                    AuthFactory.deletePicture(photoIndex).then(function (result) {
                        console.log(result);
                        $ionicLoading.hide();
                    })
                })
                .catch(function (error) {
                    $ionicLoading.hide();
                    console.error(error);
                })

            // for (var i = photoIndex; i < 4; i++) {
            //     console.log(i);
            //     if (vm.publicImagesArray[i + 1]) {
            //         vm.publicImagesArray[i] = angular.copy(vm.publicImagesArray[i + 1])
            //     }
            //     else{
            //         vm.publicImagesArray[i] = "";
            //     }
            // }

        }

        function deletePicture(photoIndex) {
            $ionicLoading.show();
            if (window.cordova) {
                function onConfirm(buttonIndex) {
                    if (buttonIndex == 1) {
                        deletePhotosAsPerIndex(photoIndex);
                    }
                    else {
                        $ionicLoading.hide();
                    }
                }

                navigator.notification.confirm(
                    'Are you sure you want to delete this image ?', // message
                    onConfirm, // callback to invoke with index of button pressed
                    'Wait .. ', // title
                    ['Yes', 'Cancel'] // buttonLabels
                );
            } else {
                deletePhotosAsPerIndex(photoIndex);
            }
        }

        function makeProfilePicture(photoIndex) {
            var temp = angular.copy(vm.profileImg);

            vm.profileImg = angular.copy(vm.publicImagesArray[photoIndex]);

            vm.publicImagesArray[photoIndex] = temp;
        }

        function openActionSheet(photoIndex) {
            console.log("photoIndex", photoIndex)
            var buttons = [{
                text: '<span ><span class="icon ion-images"></span>From Gallery</span>'
            },
            {
                text: '<span ><span class="icon ion-ios-camera"></span>Camera</span>'
            }
            ];
            // Show the action sheet
            if (vm.publicImagesArray[photoIndex] != 'img/camera2.png') {
                buttons.push({
                    text: '<span ><span class="icon ion-person"></span>Set as Profile Image</span>'
                });
                buttons.push({
                    text: '<span><span class="icon ion-trash-a"></span>Delete Image</span>'
                });
            }
            var hideSheet = $ionicActionSheet.show({

                buttons: buttons,

                cancelText: 'Cancel',
                cssClass: 'photo-action-sheet',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    // console.log(index);
                    if (index == 0) {
                        console.log('0');
                        pickFromGallery(photoIndex);
                    }
                    if (index == 1) {
                        console.log('1');
                        pickFromCamera(photoIndex);
                    }
                    if (index == 2) {
                        console.log('2');
                        makeProfilePicture(photoIndex);
                    }
                    if (index == 3) {
                        console.log('3');
                        deletePicture(photoIndex);
                    }
                    return true;
                }
            });

        }

        function setupFirstFourPhotos() {
            return $timeout(function () {
                return AuthFactory.getFacebookLastFourProfilePictures()
                    .then(

                    function (resultArray) {
                        console.log("resultArray", resultArray)
                        return $timeout(function () {
                            vm.publicImagesArray = resultArray;
                            $ionicLoading.hide();
                        }, 0);
                    })
                    .catch(function (error) {

                        $ionicLoading.hide();
                        console.error(error);
                        return $q.reject(error);
                    })
            }, 10);
        }

        $scope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && (typeof (fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };

    }

})();