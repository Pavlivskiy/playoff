(function() {
    'use strict';

    angular
        .module('app.setup')
        .controller('SetupProfile3Ctrl', SetupProfile3Ctrl);

    SetupProfile3Ctrl.$inject = ['AuthFactory', '$q', '$state', 'isFromEditScreen', '$ionicViewSwitcher', 'profileDescription', '$timeout', '$ionicLoading', '$ionicHistory'];

    function SetupProfile3Ctrl(AuthFactory, $q, $state, isFromEditScreen, $ionicViewSwitcher, profileDescription, $timeout, $ionicLoading, $ionicHistory) {

        var vm = this;
        vm.continueSetup = continueSetup
        vm.isFromEditScreen = isFromEditScreen

        activate()

        function activate() {
            if (isFromEditScreen) {
                //pull the profile
                $timeout(function() {
                    vm.profileDescription = profileDescription;
                }, 0);
            }
        }

        function continueSetup() {
            $ionicLoading.show();
            return AuthFactory.saveProfileDescription(vm.profileDescription)
                .then(function (result) {
                        if (isFromEditScreen) {
                            // $ionicViewSwitcher.nextDirection('back');
                            $ionicLoading.hide();
                            return $state.go('profile-edit')
                            // $ionicHistory.goBack(1)
                            // return $q.resolve();
                        }
                        $ionicLoading.hide();
                        return $state.go('setup-35')
                    })
                .catch(function (error) {
                    $ionicLoading.hide();
                    console.error(error);
                    return $q.reject(error);
                })
        }

    }
})();