(function() {
    'use strict';

    angular
        .module('app.setup')
        .controller('SetupProfile35Ctrl', SetupProfile35Ctrl);

    SetupProfile35Ctrl.$inject = ['AuthFactory', '$q', '$state', 'isFromEditScreen', '$ionicViewSwitcher', 'profileDescription', '$timeout', '$ionicLoading', '$ionicHistory'];

    function SetupProfile35Ctrl(AuthFactory, $q, $state, isFromEditScreen, $ionicViewSwitcher, profileDescription, $timeout, $ionicLoading, $ionicHistory) {

        var vm = this;
        vm.continueSetup = continueSetup
        vm.isFromEditScreen = isFromEditScreen

        activate()

        function activate() {
            if (isFromEditScreen) {
                //pull the profile
                $timeout(function () {
                    AuthFactory.getCurrentUserDbEntry().then(function (currentUser) {
                        vm.profileDescription = currentUser;
                        vm.question1 = vm.profileDescription.get('question1');
                        vm.question2 = vm.profileDescription.get('question2');
                        vm.question3 = vm.profileDescription.get('question3');
                        vm.question4 = vm.profileDescription.get('question4');
                    });
                }, 0);
            }
        }

        function continueSetup() {
            $ionicLoading.show();
            return AuthFactory.saveProfileQuestions({
                    question1: vm.question1,
                    question2: vm.question2,
                    question3: vm.question3,
                    question4: vm.question4
                })
                .then(
                    function(result) {
                        if (isFromEditScreen) {
                            $ionicViewSwitcher.nextDirection('back');
                            $ionicLoading.hide();
                            $ionicHistory.nextViewOptions({
                                disableBack: true
                            });
                            return $state.go('profile-edit')
                        }
                        $ionicLoading.hide();
                        return $state.go('setup-4')
                    })
                .catch(function(error) {
                    $ionicLoading.hide();
                    console.error(error);
                    return $q.reject(error);
                })
        }

    }
})();