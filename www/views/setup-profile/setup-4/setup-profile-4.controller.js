(function() {
    'use strict';

    angular
        .module('app.setup')
        .controller('SetupProfile4Ctrl', SetupProfile4Ctrl);

    SetupProfile4Ctrl.$inject = ['AuthFactory', '$q', '$state', 'isFromEditScreen', '$ionicViewSwitcher', 'profileDescription', '$timeout', '$ionicLoading', 'LocationFactory', '$scope', '$ionicPopup'];


    function SetupProfile4Ctrl(AuthFactory, $q, $state, isFromEditScreen, $ionicViewSwitcher, profileDescription, $timeout, $ionicLoading, LocationFactory, $scope, $ionicPopup) {
        var vm = this;

        vm.genderString = 'man';
        vm.interestedInString = 'women'
        vm.continueSetup = continueSetup
        vm.isFromEditScreen = isFromEditScreen;
        vm.setLocation = setLocation;
        vm.locationString = '';

        // Hide splash screen
        document.addEventListener("deviceready", onDeviceReady, false);
        
                function onDeviceReady() {
                    navigator.splashscreen.hide();
                }
        //defaults

        activate()

        $scope.$watch('vm.distanceMilesMax', function() {
            vm.colorLimit1 = ((100 * (vm.distanceMilesMax - 20)) / 130) - 1;
            vm.colorLimit2 = ((100 * (vm.distanceMilesMax - 20)) / 130);
        });


        function activate() {

            if (isFromEditScreen) {
                //pull the profile
                $timeout(function() {
                    vm.profileDescription = profileDescription;
                }, 0);
            }

            AuthFactory.getAllUserAttributes()
                .then(
                    function(result) {
                        vm.range = {};

                        if (result.distanceMilesMax) {
                            vm.distanceMilesMax = result.distanceMilesMax;
                        } else {
                            vm.distanceMilesMax = 50;
                        }
                        vm.range = {};
                        if (result.ageMin) {
                            vm.range.from = result.ageMin;
                        } else {
                            vm.range.from = 18;
                        }
                        if (result.ageMax) {
                            vm.range.to = result.ageMax;
                        } else {
                            vm.range.to = 35;
                        }
                        vm.genderString = result.genderString;
                        vm.interestedInString = result.interestedInString;
                        vm.locationString = result.locationString;

                        $timeout(function() {}, 0);
                    })
                .catch(function(error) {
                    console.error(error);
                })

        }

        function continueSetup() {
            $ionicLoading.show();

            //make sure we have all the data
            if (!vm.range.from || !vm.range.to || !vm.distanceMilesMax || !vm.genderString || !vm.interestedInString) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Hmmm',
                    template: 'It doesn\'t look like all the fields are filled in. Please double check!',
                    buttons: [{
                        text: '<b>Ok</b>',
                        type: 'button-positive',
                        onTap: function(e) {

                        }
                    }]
                });
                return $q.reject('Not all fields are filled in')
            }
            return AuthFactory.saveProfilePage4(vm.range.from, vm.range.to, vm.distanceMilesMax, vm.genderString, vm.interestedInString)
                .then(

                    function(result) {
                        //2017-10-11 21:07:48 not sure why this is here. We don't return this info from authfactory
                        // if (result.genderString) {
                        //     vm.genderString = result.genderString || 'man'
                        // }
                        // if (result.interestedInString) {
                        //     vm.interestedInString = result.interestedInString || 'women'
                        // }
                        $ionicLoading.hide();
                        if (isFromEditScreen) {
                            $ionicViewSwitcher.nextDirection('back');
                            $ionicLoading.hide();
                            return $state.go('profile-edit')
                        }
                        $ionicLoading.hide();
                        return $state.go('playoff')
                    })
                .catch(function(error) {

                    $ionicLoading.hide();
                    console.error(error);
                    return $q.reject(error);
                })
        }
        function setLocation(){
            LocationFactory.getCurrentLocation()
            .then(
                function(result) {
                    vm.locationString = result.locationString;
                    saveLocation();
                })
            .catch(function(error) {
                console.error(error);
                openPhoneSettingPage();
            })
        }
        function saveLocation() {
            if (vm.locationString) {
                $ionicLoading.show();
                return AuthFactory.saveLocationString(vm.locationString)
                    .then(

                        function(result) {
                            $ionicLoading.hide();
                        })
                    .catch(function(error) {

                        $ionicLoading.hide();
                        console.error(error);
                        return $q.reject(error);
                    })
            }
        }
        function openPhoneSettingPage() {
            // Direct go to next setup.
            function onConfirm(buttonIndex) {
                if(buttonIndex == 1) {
                } else {
                    cordova.plugins.diagnostic.switchToSettings(function() {
                    console.log("Successfully switched to Settings app");
                }, function(error) {
                    console.error("The following error occurred: " + error);
                });
                }
            }

            navigator.notification.confirm(
                'You have denied location permission to Playoff. We need to access your phone\'s location in order to find matches for you.', // message
                onConfirm, // callback to invoke with index of button pressed
                'Location Access Required', // title
                'Cancel, Open Settings' // buttonLabels
            );
        }

    }
})();