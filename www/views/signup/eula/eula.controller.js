(function() {
    'use strict';

    angular
        .module('app.signup')
        .controller('EulaCtrl', EulaCtrl);

    EulaCtrl.$inject = ['$state', '$ionicPopup', 'isFromEditScreen'];

    function EulaCtrl($state, $ionicPopup, isFromEditScreen) {

        var vm = this;
        vm.continueSetup = continueSetup;
        vm.isFromEditScreen = isFromEditScreen;

        function continueSetup() {
            if (isFromEditScreen) {
                return $state.go('settings')
            }
            var confirmPopup = $ionicPopup.confirm({
                title: 'Agree to EULA',
                template: '<span style="text-align:center">By clicking \"Agree\", you confirm that you have read and agree to the End User License Agreement, the Privacy Policy, and the Terms of Use in its entirety.</span>',
                buttons: [{
                        text: 'Cancel'
                    },
                    {
                        text: '<b>I Agree</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            return $state.go('signup-2')

                        }
                    }
                ]
            });

            // confirmPopup.then(function(res) { // eslint-disable-line
            //     if (res) {
            //     }
            // });

        }

    }
})();