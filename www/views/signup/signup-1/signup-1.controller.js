(function () {
    'use strict';

    angular
        .module('app.signup', [])
        .controller('Signup1Ctrl', Signup1Ctrl);

    Signup1Ctrl.$inject = ['$ionicPopup', '$state', 'AuthFactory', '$timeout', '$ionicNavBarDelegate', 'ParseAsService', '$ionicLoading', '$localForage'];

    function Signup1Ctrl($ionicPopup, $state, AuthFactory, $timeout, $ionicNavBarDelegate, ParseAsService, $ionicLoading, $localForage) {
        var vm = this;
        vm.loginFacebook = loginFacebook;
        vm.openUrl = openUrl;

        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {

            $localForage.getItem('fbAuthData').then(function (fbAuthData) {
                if (fbAuthData) {
                    var currentTokenExpireDate = new Date(fbAuthData.expiration_date).getTime();
                    var today = new Date().getTime();
                    if (fbAuthData || today <= currentTokenExpireDate) {
                        loginFacebook(true);
                    } else {
                        navigator.splashscreen.hide();
                    }
                }else {
                    navigator.splashscreen.hide();
                }
            });
        }

        activate();

        ////////////////

        function activate() {
            $ionicNavBarDelegate.showBackButton(false);
        }

        function loginFacebook(autoLogin) {
            if(!autoLogin){
                $ionicLoading.show();
            }
            return AuthFactory.loginUserFacebook(autoLogin)
                .then(
                function (isThisOldAccount) {
                    $ionicNavBarDelegate.showBackButton(true);
                    // ParsePush.init();
                    if (!isThisOldAccount) {
                        //go to signup 2
                        $ionicLoading.hide();
                        // $state.go('setup-1')
                        goToEula();
                        navigator.splashscreen.hide();
                    } else {
                        //go straight home to playoff
                        $ionicLoading.hide();
                        // $state.go('setup-2')

                        if (window.cordova) {
                            //check if the app has notification permissions
                            checkNotificationPermsission();
                        } else {
                            goToPlayoffHome();
                            navigator.splashscreen.hide();
                        }
                    }
                })
                .catch(function (error) {
                    console.error(error);
                    $ionicLoading.hide();
                    return errorFacebookLogin(error)
                })

            //2017-07-31 12:53:01 may not use while we stick with fb browser login for parse. Parse FB cordova is TBD
            //check if we're on the app or on web
            //after success, ui-sref='signup-2'

            // if (window.cordova) {
            //     //we're on the app
            //     loginFacebookCordova()

            // } else {
            //     //we're on the browser
            //     loginFacebookWeb()
            // }
        }

        function checkNotificationPermsission() {
            goToPlayoffHome();
            // if (window.ParsePushPlugin) {
            //     ParsePushPlugin.register(function (response) {
            //         console.log(response);
            //         console.log('success push register')
            //         goToPlayoffHome();
            //     }, function (error) {
            //         console.log('fail push register')
            //         openPhoneSettingPage();
            //     })
            // }

            function openPhoneSettingPage() {

                function onConfirm(buttonIndex) {
                    cordova.plugins.diagnostic.switchToSettings(function () {
                        console.log("Successfully switched to Settings app");
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }

                navigator.notification.alert(
                    'Playoff needs permission to notify you of matches and chat messages', // message
                    onConfirm, // callback to invoke with index of button pressed
                    'Notification permission required', // title
                    'Open Settings' // buttonLabels
                );

            }
        }

        //2017-07-31 12:53:01 may not use while we stick with fb browser login for parse. Parse FB cordova is TBD
        function loginFacebookWeb() {
            FB.login(function (response) {
                // handle the response
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.

                    goToEula()
                } else {
                    errorFacebookLogin(response.status)
                }
            }, {
                    scope: 'public_profile,email,user_birthday'
                });
        }

        //2017-07-31 12:53:01 may not use while we stick with fb browser login for parse. Parse FB cordova is TBD
        function loginFacebookCordova() {
            facebookConnectPlugin.login(['public_profile', 'email', 'user_birthday'], function (successResult) {
                console.log('success login: ', successResult)
                goToEula()
            }, function (errorResult) {
                console.log('errorResult login: ', errorResult)

                errorFacebookLogin(errorResult)
            })
        }

        function goToEula() {
            return $state.go('eula') 
        }

        function goToPlayoffHome() {
            return $state.go('playoff')
        }

        function errorFacebookLogin(errorText) {
            console.log(errorText);
            // The person is not logged into this app or we are unable to tell. 
            var alertPopup = $ionicPopup.alert({
                title: 'Hmmm',
                template: 'We were unable to login via Facebook',
                okType: "button-balanced button-green"
            });
        }

        function openUrl(params) {
            if (params == 1) {
                // Terms
                var url = "https://goo.gl/mr2N3k";
            }
            else {
                // Privacy
                var url = "https://goo.gl/efkpnQ";
            }
            if (window.cordova) {
                cordova.InAppBrowser.open(url, '_blank', 'location=no');
            }
            else {
                window.open(url, '_blank')
            }
        }
    }
})();