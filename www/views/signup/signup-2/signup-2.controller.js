(function () {
    'use strict';

    angular
        .module('app.signup')
        .controller('Signup2Ctrl', Signup2Ctrl);

    Signup2Ctrl.$inject = ['$timeout', 'AuthFactory', '$ionicPopup', '$state'];

    function Signup2Ctrl($timeout, AuthFactory, $ionicPopup, $state) {
        var vm = this;
        vm.activate = activate;

        // activate()

        function activate() {
            if (window.cordova) {
                askPush();
                //we're on the app, check if notifications need to be requested

            } else {
                //we're on the browser. request notifications
                $state.go('signup-3')
            }
        }

        function askPush() {
            // ParsePushPlugin.register(successCB, errorCB)
            // if (window.ParsePushPlugin) {
            //     try {
            //         console.log(response);
            //         console.log('success push register')
            //         $state.go('signup-3');
            //     }
            //     catch (e) {
            //         console.log('fail push register')
            //         openPhoneSettingPage();
            //     };
            // }
            window.FirebasePlugin.hasPermission(function(data){
                console.log(data.isEnabled);
                if(data.isEnabled){
                    // console.log(response);
                    console.log('success push register')
                    $state.go('signup-3');
                }
                else {
                    console.log('fail push register')
                    openPhoneSettingPage();
                }
            });
        }

        function openPhoneSettingPage() {

            function onConfirm(buttonIndex) {
                cordova.plugins.diagnostic.switchToSettings(function () {
                    console.log("Successfully switched to Settings app");
                }, function (error) {
                    console.error("The following error occurred: " + error);
                });
            }

            navigator.notification.alert(
                'Playoff needs permission to notify you of matches and chat messages', // message
                onConfirm, // callback to invoke with index of button pressed
                'Notification permission required', // title
                'Open Settings' // buttonLabels
            );

        }

    }
})();