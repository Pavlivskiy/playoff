(function() {
    'use strict';

    angular
        .module('app.signup')
        .controller('Signup3Ctrl', Signup3Ctrl);

    Signup3Ctrl.$inject = ['LocationFactory', 'AuthFactory', '$state', '$ionicLoading'];

    function Signup3Ctrl(LocationFactory, AuthFactory, $state, $ionicLoading) {
        var vm = this;
        // vm.continueSetup = continueSetup;
        vm.enableLocation = enableLocation;

        function enableLocation() {
            LocationFactory.getCurrentLocation()
                .then(
                    function(result) {
                        vm.locationString = result.locationString;
                        continueSetup();
                    })
                .catch(function(error) {
                    console.error(error);
                    openPhoneSettingPage();
                })
        }


        function continueSetup() {
            if (vm.locationString) {
                $ionicLoading.show();
                return AuthFactory.saveLocationString(vm.locationString)
                    .then(

                        function(result) {
                            $ionicLoading.hide();
                            $state.go('signup-4');
                        })
                    .catch(function(error) {

                        $ionicLoading.hide();
                        console.error(error);
                        return $q.reject(error);
                    })
            }
        }
        //         else {
        //     cordova.plugins.diagnostic.isLocationAuthorized(function (authorized) {
        //         console.log("Location is " + (authorized ? "authorized" : "unauthorized"));
        //         if (authorized) {
        //             activate();
        //         }
        //     }, function (error) {
        //         console.error("The following error occurred: " + error);
        //     });
        // }
        // }
        // else {
        //     $state.go('signup-4');
        // }
        // }

        function openPhoneSettingPage() {
            // Direct go to next setup.
            function onConfirm(buttonIndex) {
                if(buttonIndex == 1) {
                    $state.go('signup-4');
                } else {
                    cordova.plugins.diagnostic.switchToSettings(function() {
                    console.log("Successfully switched to Settings app");
                }, function(error) {
                    console.error("The following error occurred: " + error);
                });
                }
            }

            navigator.notification.confirm(
                'You have denied location permission to Playoff. We need to access your phone\'s location in order to find matches for you.', // message
                onConfirm, // callback to invoke with index of button pressed
                'Location Access Required', // title
                'Cancel, Open Settings' // buttonLabels
            );

        }

    }



})();