(function () {
    'use strict';

    angular
        .module('app.signup')
        .controller('Signup4Ctrl', Signup4Ctrl);

    Signup4Ctrl.$inject = ['$ionicPopup', '$scope', '$timeout', '$ionicActionSheet', '$rootScope', 'CameraService', '$ionicModal', '$cordovaToast', '$state', '$ionicLoading', '$http', '$q'];

    function Signup4Ctrl($ionicPopup, $scope, $timeout, $ionicActionSheet, $rootScope, CameraService, $ionicModal, $cordovaToast, $state, $ionicLoading, $http, $q) {
        var vm = this;
        vm.verifyAthlete = {
            jerseyImage: '',
            submitBio: false
        };
        if ($rootScope.ios) {
            vm.fullDim = {
                'height': window.innerHeight - 64 + 'px',
                'width': window.innerWidth + 'px'
            };
        } else {
            vm.fullDim = {
                'height': window.innerHeight - 44 + 'px',
                'width': window.innerWidth + 'px'
            };
        }
        vm.holder = {
            'height': window.innerHeight - 64 - 100 + 'px'
        };
        $scope.parentPhotoHeight = {
            'height': (window.innerHeight - 88) + 'px'
        };
        vm.userBio = '';
        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            if ($rootScope.ios) {
                cordova.plugins.diagnostic.requestCameraRollAuthorization(function () { }, function () {
                    $cordovaToast.showShortBottom('Gallery permission denied');
                });
            }
            navigator.splashscreen.hide();
        }
        // submit bio function
        vm.submitBio = function () {
            // Show popup for submit bio.
            var myPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="vm.userBio" placeholder="For example www.mybiolink.com">',
                title: 'Athlete Verification',
                subTitle: 'Please submit a link to your athletic bio, team roster, or an article with your name in it',
                scope: $scope,
                buttons: [{
                    text: 'Cancel'
                },
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!vm.userBio) {
                            //don't allow the user to close unless he enters biolink
                            e.preventDefault();
                        } else {
                            return vm.userBio;
                        }
                    }
                }
                ]
            });

            myPopup.then(function (res) {
                if (!res) {
                    $cordovaToast.showShortBottom('Try one more time, unexcpeted input');
                    $ionicLoading.hide();
                }
                else {
                    $ionicLoading.show();
                    console.log('Tapped!', res);
                    if (res) {
                        var urlString = '';
                        var checkUrl = res.split('//');
                        if (checkUrl[0] && checkUrl[0].toLowerCase() == 'http:' || checkUrl[0].toLowerCase() == 'https:') {
                            urlString = res;
                        } else {
                            urlString = 'http://' + res;
                        }
                        $http.head(urlString).then(function (success) {
                            console.log(success);
                            var user = firebase.auth().currentUser;
                            //prepare string to firebase format

                            firebase.database().ref("_users").orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                                var entryId = Object.keys(snapshot.val())[0];
                                //snapshot.ref is not working, so I declaring a full entry url using entryId
                                var json = {
                                    bioLink: res,
                                };
                                firebase.database().ref("_users/" + entryId).update(json).then((snapshot) => {
                                    vm.verifyAthlete.submitBio = true;
                                    vm.userBio = '';
                                    $cordovaToast.showShortBottom('Bio submitted successfully!');
                                    $ionicLoading.hide();
                                }).catch((e) => {
                                    $ionicLoading.hide();
                                    console.log(e);
                                });

                            }).catch((e) => {
                                console.log(e);
                                $cordovaToast.showShortBottom('DB update issues');
                                $ionicLoading.hide();
                            });
                        }, function (err) {
                            console.log(err);
                            if ($state.current.name === 'signup-4') {
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Bio link verification failed',
                                    template: 'The link added by you is not valid'
                                });

                                alertPopup.then(function (res) {
                                    console.log('link invalid');
                                });
                            }
                        });

                    }
                }
            });

        }

        // Image Crop functions.
        vm.jerseyImage = '';

        function cropImageFun(base64Image, photoIndex, url) {
            if (url) {
                $scope.parentImage = base64Image;
            } else {
                $scope.parentImage = 'data:image/jpg;base64,' + base64Image;
            }
            vm.croppedImage = '';
            $scope.imageArray = [];
            // open a modal to crop image
            $ionicModal.fromTemplateUrl('./views/setup-profile/setup-2/crop-image.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });

            $scope.closeModal = function () {
                $scope.modal.hide();
                $scope.modal.remove();
            };

            $scope.cropPicture = function () {
                // console.log(vm.croppedImage);
                // $ionicLoading.show();
                // $scope.modal.hide();
                // var user = Parse.User.current();
                var username = firebase.auth().currentUser.uid;
                var parseFile = new Parse.File(username, {
                    base64: vm.croppedImage
                }, 'image/jpeg');

                parseFile.save().then(function (data) {
                    console.log(data);
                    Parse.User.current().set('jerseyImage', data._url)
                        .save().then(function (success) {
                            console.log(success);
                            vm.verifyAthlete.jerseyImage = data._url;
                            vm.jerseyImage = data._url;
                            $cordovaToast.showShortBottom('Image uploaded successfully!');
                            $ionicLoading.hide();
                        }, function (err) {
                            console.log(err);
                            $ionicLoading.hide();
                        })
                }, function (err) {
                    console.log(err);
                    $ionicLoading.hide();
                });
            };
        }

        function openPhotoEditorSDK(base64Image) {
            var ReactUI = PhotoEditorSDK.UI.ReactUI;
            var _PhotoEditorSDK = PhotoEditorSDK;
            var Promise = _PhotoEditorSDK.Promise;
            var editor;
            var myImage;
            var editorPromise;

            function onLoad() {
                var defer = $q.defer();
                // maxWindow();
                var w = window.innerWidth;
                var h = window.innerHeight;

                var div = document.createElement("div");
                div.id = "editor";

                document.getElementById("main").appendChild(div);

                document.getElementById("editor").style.height = h + "px";
                document.getElementById("editor").style.width = w + "px";
                document.getElementById("editor").style.zIndex = 500;
                var container = document.getElementById('editor');
                editor = new ReactUI({
                    container: container,
                    title: '',
                    showCloseButton: true,
                    // logLevel: 'info',
                    editor: {
                        image: myImage,
                        //preferredRenderer: 'canvas' works in ios, preferredRenderer: 'webgl' works in android,
                        preferredRenderer: 'canvas',
                        // responsive: true,
                        
                        tools:['crop','filter'],
                        controlsOrder:['crop','filter'],
                        export: {
                            showButton: true,
                            download: false,
                            type: PhotoEditorSDK.RenderType.DATAURL
                            // type: PhotoEditorSDK.ImageFormat.IMAGE
                        }
                    },
                    assets: {
                        // baseUrl: 'file:///android_asset/www/sdkassets' // <-- This should be the absolute path to your `cassets` directory
                        baseUrl: 'sdkassets'
                    }
                });
                editorPromise = defer.promise;

                editor.on('export', function (dataURL) {
                    console.log(dataURL);
                    var url = "/my-upload-handler.php";
                    var data = {
                        image: dataURL
                    };
                    defer.resolve(dataURL);

                    var element = document.getElementById("editor");
                    element.parentNode.removeChild(element);


                    // urlData.link=dataURL;
                });
                editor.on('close', function (close) {
                    console.log(close);
                    var element = document.getElementById("editor");
                    element.parentNode.removeChild(element);
                });

                editorPromise
                    .then(function (success) {
                        // console.log(success);
                        $ionicLoading.show();
                        // var user = Parse.User.current();
                        // var username = user.get("username");
                   
                        var user = firebase.auth().currentUser;

                        //prepare string to firebase format
                        var message = success.replace('data:image/png;base64,', '').replace(/\s/g, '');;
                        var fullPictureFirebasePath = '/playoff_photos/' + createUUID() + '.png';
                        firebase.storage().ref(fullPictureFirebasePath).putString(message, 'base64').then(function (snapshot) {
                            firebase.storage().ref(fullPictureFirebasePath).getDownloadURL().then(function (url) {
                                console.log(url);
                                console.log('Uploaded a base64 string!');
                                //update a field with url in the db
                                firebase.database().ref("_users").orderByChild("username").equalTo(user.uid).once('value', function (snapshot) {
                                    var entryId = Object.keys(snapshot.val())[0];
                                    //snapshot.ref is not working, so I declaring a full entry url using entryId
                                    var json = { 
                                        jerseyImage: url,
                                        jerseyImageFullUrl: fullPictureFirebasePath
                                    };
                                    firebase.database().ref("_users/" + entryId).update(json).then((snapshot) => {
                                        vm.verifyAthlete.jerseyImage = url;
                                        vm.jerseyImage = url;
                                        $cordovaToast.showShortBottom('Image uploaded successfully!');
                                        $ionicLoading.hide();
                                    }).catch((e) => {
                                        $ionicLoading.hide();
                                        $cordovaToast.showShortBottom("Can't upload image");
                                        console.log(e);
                                    });

                                }).catch((e) => {
                                    $ionicLoading.hide();
                                    $cordovaToast.showShortBottom("Can't upload image");
                                    console.log(e);
                                });
                            });
                        }).catch((e) => {
                            console.log(e);
                            $ionicLoading.hide();
                        });

                        // parseFile.save().then(function (data) {
                        //     console.log(data);
                        //     user.updateProfile({
                        //         jerseyImage: data._url
                        //     }).then(function () {
                        //         // Update successful.
                        //         console.log(success);
                        //         vm.verifyAthlete.jerseyImage = data._url;
                        //         vm.jerseyImage = data._url;
                        //         $cordovaToast.showShortBottom('Image uploaded successfully!');
                        //         $ionicLoading.hide();
                        //     }).catch(function (error) {
                        //         // An error happened.
                        //         console.log(err);
                        //         $ionicLoading.hide();
                        //     });
                            //     Parse.User.current().set('jerseyImage', data._url)
                            //         .save().then(function (success) {
                            //             console.log(success);
                            //             vm.verifyAthlete.jerseyImage = data._url;
                            //             vm.jerseyImage = data._url;
                            //             $cordovaToast.showShortBottom('Image uploaded successfully!');
                            //             $ionicLoading.hide();
                            //         }, function (err) {
                            //             console.log(err);
                            //             $ionicLoading.hide();
                            //         })
                        // }, function (err) {
                        //     console.log(err);
                        //     $ionicLoading.hide();
                        // });
                    })
            }
            myImage = new Image();
            myImage.addEventListener('load', function () {
                // $scope.openModal();
                // $state.go('tab.account');
                onLoad();
            });
            base64Image = "data:image/jpeg;base64," + base64Image;
            myImage.src = base64Image;
        }
        function createUUID() {
            // http://www.ietf.org/rfc/rfc4122.txt
            var s = [];
            var hexDigits = "0123456789ABCDEF";
            for (var i = 0; i < 32; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[12] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
            s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
         
            var uuid = s.join("");
            return uuid;
        }
        function pickFromGallery(photoIndex) {
            if (!$rootScope.android) {


                cordova.plugins.diagnostic.isCameraRollAuthorized(function (authorized) {
                    if (authorized) {
                        //camera roll authorized
                        CameraService.pickPicture().then(function (result) {
                            //console.log(result);
                            openPhotoEditorSDK(result);
                        })
                    } else {
                        //camera roll permission not authorized
                        openPhoneSettingPage();
                    }
                }, function (error) {
                    console.error("The following error occurred: " + error);
                });
            } else {
                CameraService.checkCameraPermission().then(function (data) {
                    if (data) {
                        //camera/camera roll authorized
                        CameraService.pickPicture().then(function (result) {
                            // console.log(result);
                            openPhotoEditorSDK(result);
                        })
                    }
                }, function (err) {
                    console.log(err);
                })
            }
        }

        function pickFromCamera(photoIndex) {
            CameraService.checkCameraPermission().then(function (data) {
                if (data) {
                    //camera authorized
                    CameraService.takePicture().then(function (base64Image) {
                        // console.log(base64Image);
                        openPhotoEditorSDK(base64Image);
                        //cropImageFun(base64Image, photoIndex, false);
                    })
                }
            }, function (err) {
                console.log(err);

            })
        }
        // Open setting app for permission.
        function openPhoneSettingPage() {

            function onConfirm(buttonIndex) {
                if (buttonIndex == 1) {

                    cordova.plugins.diagnostic.switchToSettings(function () {
                        console.log("Successfully switched to Settings app");
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }
            }

            navigator.notification.alert(
                'We need to access phone\'s camera roll in order to upload pictures', // message
                onConfirm, // callback to invoke with index of button pressed
                'Camera Roll required', // title
                'Open Settings' // buttonLabels
            );

        }

        // upload jersey photo
        vm.uploadJerseyPhoto = function () {

            //for tests
            try {
                if (cordova) {

                }
            } catch (error) {
                if (window.location.href.indexOf("localhost") > -1) {
                    return $state.go('setup-1')
                }
            }

            // Show the action sheet
            var hideSheet = $ionicActionSheet.show({
                buttons: [{
                    text: '<span ><span class="icon ion-images"></span>From Gallery</span>'
                },
                {
                    text: '<span ><span class="icon ion-ios-camera"></span>Camera</span>'
                }
                ],
                cancelText: 'Cancel',
                cssClass: 'photo-action-sheet',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    // console.log(index);
                    if (index == 0) {
                        console.log('0');
                        pickFromGallery();
                    }
                    if (index == 1) {
                        console.log('1');
                        pickFromCamera();
                    }
                    return true;
                }
            });
        }
        vm.continueSetup = function () {
            if (window.cordova) {
                if (vm.verifyAthlete.jerseyImage && vm.verifyAthlete.submitBio) {
                    $state.go('setup-1');
                } else {
                    function alertDismissed() {
                        // do something
                    }

                    navigator.notification.alert(
                        'Please upload jersey image and bio for verification',  // message
                        alertDismissed,         // callback
                        'Profile setup Error',            // title
                        'Done'                  // buttonName
                    );
                }
            }
            else {
                $state.go('setup-1');

            }
        }
    }
})();